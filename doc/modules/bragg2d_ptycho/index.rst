.. py:module:: pynx.ptycho

.. toctree::
   :maxdepth: 2

:mod:`pynx.ptycho.bragg2d`: 2D Bragg Ptychography
=================================================

WARNING: The 2D Bragg Ptychography code is highly unstable (development stage),
so should not be used except by developers.

API Reference
-------------
Note that the Python API is quickly evolving.

For regular data analysis, it is recommended to use the :doc:`scripts <../../scripts/index>` which are stable,
independently of the underlying Python API.

2D Ptychography (operator-based)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is the Ptychography class, which can be used with operators

.. automodule:: pynx.ptycho.bragg2d.bragg2d
   :members:

2D Ptychography Operators
^^^^^^^^^^^^^^^^^^^^^^^^^
This section lists the operators, which can be imported automatically using `from pynx.ptycho.bragg2d import *`
or `from pynx.ptycho.bragg2d.operator import *`.

.. automodule:: pynx.ptycho.bragg2d.cl_operator
   :members:


Examples
--------

..
  Operator-based API, far field
  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  .. literalinclude:: ../../../pynx/ptycho/examples/ptycho_operators.py
