# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr


import warnings

try:
    import pyopencl as cl
    import pyopencl.array
    import pyopencl.elementwise
    import gpyfft

    has_opencl = True

    gpyfft_version = gpyfft.version.__version__.split('.')
    if int(gpyfft_version[0]) == 0 and int(gpyfft_version[1]) < 7:
        warnings.warn("WARNING: gpyfft version earlier than 0.7.0. Upgrade may be required", gpyfft_version)
    # else:
    #     print("gpyfft version:", gpyfft.version.__version__)

    if cl.VERSION < (2016, 1):
        warnings.warn("WARNING: PyOpenCL version < (2016,1) ! Upgrade may be required (from_int_ptr error)\n")

except ImportError:
    has_opencl = False

from . import ProcessingUnit
from .cl_resources import cl_resources
from .opencl_device import cl_device_fft_speed

from pynx.utils.math import test_smaller_primes


class CLEvent(object):
    """
    Record an event, and optionally the number of floating-point operations and the number of bytes transferred
    during the event. This is only useful when profiling is used.
    """

    def __init__(self, event: cl.Event, nflop: int = 0, nbyte: int = 0):
        self.event = event
        self.nflop = nflop
        self.nbytes = nbyte

    def gflops(self):
        """
        Gflop/s

        :return: the estimated computed Gflop/s
        """
        return self.nflop / max(self.event.profile.end - self.event.profile.start, 1)

    def gbs(self):
        """
        Transfer speed in Gb/s. Depending on the event, this can be host to device or device to device speed.

        :return: the transfer speed in Gb/s
        """
        return self.nbytes / max(self.event.profile.end - self.event.profile.start, 1)


class CLProcessingUnit(ProcessingUnit):
    """
    Processing unit in OpenCL space.

    Handles initializing the context. Kernel initilization must be done in derived classes.
    """

    def __init__(self):
        super(CLProcessingUnit, self).__init__()
        self.cl_ctx = None  # OpenCL context
        self.cl_queue = None  # OpenCL queue
        self.cl_options = None  # OpenCL compile options
        self.gpyfft_plan = None  # gpyfft plan
        self.gpyfft_shape = None  # gpyfft data shape
        self.gpyfft_axes = None  # gpyfft axes
        self.ev = []  # List of openCL events which are queued
        self.profiling = False  # profiling enabled ?
        # Keep track of events for profiling
        self.cl_event_profiling = {}
        # gpyfft plans
        self._gpyfft_plan_v = {}

    def init_cl(self, cl_ctx=None, cl_device=None, fft_size=(1, 1024, 1024), batch=True, gpu_name=None, test_fft=True,
                verbose=True, profiling=None):
        """
        Initialize the OpenCL context and creates an associated command queue

        :param cl_ctx: pyopencl.Context. If none, a default context will be created
        :param cl_device: pyopencl.Device. If none, and no context is given, the fastest GPu will be used.
        :param fft_size: the fft size to be used, for benchmark purposes when selecting GPU. different fft sizes
                         can be used afterwards?
        :param batch: if True, will benchmark using a batch 2D FFT
        :param gpu_name: a (sub)string matching the name of the gpu to be used
        :param test_fft: if True, will benchmark the GPU(s)
        :param verbose: report the GPU found and their speed
        :param profiling: if True, enable profiling for the command queue. If None, current value of self.profiling
                          is used
        :return: nothing
        """
        self.set_benchmark_fft_parameters(fft_size=fft_size, batch=batch)
        self.use_opencl(gpu_name=gpu_name, cl_ctx=cl_ctx, cl_device=cl_device, test_fft=test_fft, verbose=verbose)

        assert test_smaller_primes(fft_size[-1], self.max_prime_fft, required_dividers=(2,)) \
               and test_smaller_primes(fft_size[-2], self.max_prime_fft, required_dividers=(2,))

        self.cl_ctx = CLProcessingUnit.get_context(self.cl_device)
        if profiling is not None:
            self.profiling = profiling
        if self.profiling:
            self.cl_queue = cl.CommandQueue(self.cl_ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
        else:
            self.cl_queue = cl.CommandQueue(self.cl_ctx)
        self.cl_options = "-cl-mad-enable -cl-fast-relaxed-math"
        # Workaround OpenCL intel Iris Pro wrong calculation of lgamma()...
        if self.cl_ctx.devices[0].name.find('Iris Pro') >= 0:
            self.cl_options += " -DIRISPROBUG=1 "
        self.cl_init_kernels()

    def get_context(device):
        """
        Static method to get a context, using the static device context dictionary to avoid creating new contexts,
        which will use up the GPU memory.
        :param device: the pyOpenCL device for which a context is desired
        """
        return cl_resources.get_context(device)

    def enable_profiling(self, profiling=True):
        """
        Enable profiling for the main OpenCL queue
        :param profiling: True to enable (the default)
        :return:
        """
        self.profiling = profiling
        if self.cl_ctx is not None:
            # Context already exists, update queue. Otherwise, queue will be initialised when init_cl is called.
            if self.profiling:
                self.cl_queue = cl.CommandQueue(self.cl_ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
            else:
                self.cl_queue = cl.CommandQueue(self.cl_ctx)

    def cl_init_kernels(self):
        """
        Initialize kernels. Virtual function, must be derived.

        :return:
        """

    def cl_fft_set_plan(self, cl_data, axes=(-1, -2)):
        """
        Creates FFT plan, or updates it if the shape of the data or the axes have changed.

        .. deprecated:: 2020.2
        Use :func:`cl_fft_get_plan` instead.

        :param cl_data: the pyopencl.array.Array data
        :param axes: the FFT axes. If None, this will be set to (-2, -1) for 2D and (-3, -2, -1) for 3D
        :return: nothing
        """
        warnings.warn("CLProcessingUnit.cl_fft_set_plan() is deprecated, use cl_fft_get_plan() instead",
                      DeprecationWarning)
        self.gpyfft_plan = self.cl_fft_get_plan(cl_data, axes=axes)

    def cl_fft_get_plan(self, cl_data, axes=(-1, -2), shuffle_axes=False):
        """
        Get a FFT plan according to the given parameters. if the plan already exists, it is re-used

        :param cl_data: the pyopencl.array.Array data
        :param axes: the FFT axes. If None, this will be set to (-2, -1) for 2D and (-3, -2, -1) for 3D
        :param shuffle_axes: if True, will try permutations on the axes order to get the optimal
                             speed.
        :return: nothing
        """
        # TODO: it would be better to store a plan independent of the actual data,
        #  and then only use enqueue_arrays(..) instead of enqueue(..).
        if axes is None:
            # Use default values providing good speed with gpyfft
            if cl_data.ndim == 2:
                axes = (-1, -2)
            else:
                axes = (-1, -2, -3)
        elif len(axes) < cl_data.ndim - 1:
            # Kludge to work around "data layout not supported (only single non-transformed axis allowed)" in gpyfft
            s = list(cl_data.shape)
            n = cl_data.ndim
            for i in range(1, cl_data.ndim):
                if i - 1 not in axes and i not in axes and i - n not in axes and i - 1 - n not in axes:
                    # print(i - 1, i, i - n, i - 1 - n, axes)
                    # collapse axes
                    n0 = s.pop(0)
                    s[0] *= n0
                else:
                    break
            s = tuple(s)
            if s != cl_data.shape:
                # print("cl_fft_set_plan: collapsing data shape from ", cl_data.shape, " to ", s, '. Axes:', axes)
                cl_data = cl_data.reshape(s)
        k = (cl_data.shape, cl_data.dtype, axes)

        if k in self._gpyfft_plan_v:
            self._gpyfft_plan_v[k].data = cl_data
            return self._gpyfft_plan_v[k]

        if shuffle_axes:
            # Change the axes if necessary, but keep the original ones as key
            flops, dt, axes = cl_device_fft_speed(self.cl_device, cl_data.shape, axes, timing=True, shuffle_axes=True)

        self._gpyfft_plan_v[k] = gpyfft.FFT(self.cl_ctx, self.cl_queue, cl_data, None, axes=axes)

        return self._gpyfft_plan_v[k]

    def cl_fft_free_plans(self):
        """
        Delete the gpyfft plans from memory. Actual deletion may only occur later (gc...)
        :return: nothing
        """
        self.finish()
        self._gpyfft_plan_v = {}

    def finish(self):
        for e in self.ev:
            e.wait()
        self.cl_queue.finish()
