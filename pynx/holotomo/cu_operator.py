# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import platform
import types
import timeit
import os
import psutil
import gc
import numpy as np
from .holotomo import algo_string

from ..processing_unit.cu_processing_unit import CUProcessingUnit
import pycuda.driver as cu_drv
import pycuda.gpuarray as cua
from pycuda.elementwise import ElementwiseKernel as CU_ElK
from pycuda.reduction import ReductionKernel as CU_RedK
from pycuda.compiler import SourceModule
import skcuda.fft as cu_fft
import pycuda.tools as cu_tools

from ..processing_unit import default_processing_unit as main_default_processing_unit
from ..processing_unit.cu_processing_unit import CUProcessingUnit
from ..processing_unit.kernel_source import get_kernel_source as getks
from ..operator import has_attr_not_none, OperatorSum, OperatorPower, OperatorException
from . import cpu_operator as cpuop

from .holotomo import HoloTomo, HoloTomoDataStack, HoloTomoData, OperatorHoloTomo

my_float4 = cu_tools.get_or_register_dtype("my_float4",
                                           np.dtype([('a', '<f4'), ('b', '<f4'), ('c', '<f4'), ('d', '<f4')]))

# half = cu_tools.get_or_register_dtype("half", np.float16)
half = cu_tools.get_or_register_dtype("half", np.float32)


################################################################################################
# Patch HoloTomo class so that we can use 5*w to scale it.
# OK, so this might be ugly. There will definitely be issues if several types of operators
# are imported (e.g. OpenCL and CUDA)
# Solution (?): in a different sub-module, implement dynamical type-checking to decide which
# Scale() operator to call.


def patch_method(cls):
    def __rmul__(self, x):
        # Multiply object by a scalar.
        if np.isscalar(x) is False:
            raise OperatorException("ERROR: attempted Op1 * Op2, with Op1=%s, Op2=%s" % (str(x), str(self)))
        return Scale(x) * self

    def __mul__(self, x):
        # Multiply object by a scalar.
        if np.isscalar(x) is False:
            raise OperatorException("ERROR: attempted Op1 * Op2, with Op1=%s, Op2=%s" % (str(x), str(self)))
        return self * Scale(x)

    cls.__rmul__ = __rmul__
    cls.__mul__ = __mul__


patch_method(HoloTomo)


################################################################################################


class CUProcessingUnitHoloTomo(CUProcessingUnit):
    """
    Processing unit in CUDA space, for operations on HoloTomo objects.

    Handles initializing the context and kernels.
    """

    def __init__(self):
        super(CUProcessingUnitHoloTomo, self).__init__()
        # Stream for calculations (don't use default stream which can be blocking)
        self.cu_stream = None
        # Stream to copy data between host and GPU
        self.cu_stream_swap = None
        # Event recording when last swapping object in & out is finished
        self.cu_event_swap_obj = None
        # Event for calculation stream
        self.cu_event_calc = None
        # Event for swap stream
        self.cu_event_swap = None
        # Memory pool
        self.cu_mem_pool = None

    def init_cuda(self, cu_ctx=None, cu_device=None, fft_size=(1, 1024, 1024), batch=True, gpu_name=None, test_fft=True,
                  verbose=True):
        """
        Derived init_cuda function. Also creates in/out queues for parallel processing of large datasets.

        :param cu_ctx: pycuda.driver.Context. If none, a default context will be created
        :param cu_device: pycuda.driver.Device. If none, and no context is given, the fastest GPu will be used.
        :param fft_size: the fft size to be used, for benchmark purposes when selecting GPU. different fft sizes
                         can be used afterwards?
        :param batch: if True, will benchmark using a batch 2D FFT
        :param gpu_name: a (sub)string matching the name of the gpu to be used
        :param test_fft: if True, will benchmark the GPU(s)
        :param verbose: report the GPU found and their speed
        :return: nothing
        """
        super(CUProcessingUnitHoloTomo, self).init_cuda(cu_ctx=cu_ctx, cu_device=cu_device, fft_size=fft_size,
                                                        batch=batch,
                                                        gpu_name=gpu_name, test_fft=test_fft, verbose=verbose)
        # Stream for calculations (don't use the default stream which can be blocking)
        self.cu_stream = cu_drv.Stream()
        self.cu_stream_swap = cu_drv.Stream()
        self.cu_event_swap_obj = cu_drv.Event()
        self.cu_event_calc = cu_drv.Event()
        self.cu_event_swap = cu_drv.Event()

        # Disable CUDA helf operators, which lead to errors such as:
        #   more than one instance of overloaded function "operator-" has "C" linkage
        # Unfortunately this depends on the platform/compiler, so need a test for this
        try:
            testk = CU_ElK(name='testk', operation="d[i] *= 2", preamble='#include "cuda_fp16.h"',
                           options=self.cu_options, arguments="float *d")
            cu_d = cua.empty(128, dtype=np.float32)
            testk(cu_d)
        except cu_drv.CompileError:
            print("CUProcessingUnitHoloTomo:init_cuda(): disabling CUDA half operators")
            self.cu_options.append("-D__CUDA_NO_HALF_OPERATORS__")
            self.cu_options.append("-D__CUDA_NO_HALF2_OPERATORS__")

    def cu_init_kernels(self):
        print("HoloTomo CUDA processing unit: compiling kernels...")
        t0 = timeit.default_timer()
        # Elementwise kernels
        self.cu_scale = CU_ElK(name='cu_scale',
                               operation="d[i] = complexf(d[i].real() * scale, d[i].imag() * scale )",
                               preamble=getks('cuda/complex.cu'),
                               options=self.cu_options, arguments="pycuda::complex<float> *d, const float scale")

        self.cu_sum = CU_ElK(name='cu_sum', operation="dest[i] += src[i]",
                             options=self.cu_options,
                             arguments="pycuda::complex<float> *src, pycuda::complex<float> *dest")

        self.cu_scale_complex = CU_ElK(name='cu_scale_complex',
                                       operation="d[i] = complexf(d[i].real() * s.real() - d[i].imag() * s.imag(),"
                                                 "d[i].real() * s.imag() + d[i].imag() * s.real())",
                                       preamble=getks('cuda/complex.cu'),
                                       options=self.cu_options,
                                       arguments="pycuda::complex<float> *d, const pycuda::complex<float> s")

        self.cu_iobs2psi = CU_ElK(name='cu_iobs2psi',
                                  operation="Iobs2Psi(i, iobs, iobs_empty, psi, nb_mode, nx, ny, nz)",
                                  preamble=getks('cuda/complex.cu') + getks('holotomo/cuda/paganin_elw.cu'),
                                  options=self.cu_options,
                                  arguments="float *iobs, float *iobs_empty, pycuda::complex<float> *psi,"
                                            "const int nb_mode, const int nx, const int ny, const int nz")

        self.cu_iobs_empty2probe = CU_ElK(name='cu_iobs_empty2probe',
                                          operation="IobsEmpty2Probe(i, iobs_empty, probe, nprobe, nx, ny, nz)",
                                          preamble=getks('cuda/complex.cu') + getks('holotomo/cuda/paganin_elw.cu'),
                                          options=self.cu_options,
                                          arguments="float *iobs_empty, pycuda::complex<float> *probe,"
                                                    "const int nprobe, const int nx, const int ny, const int nz")

        self.cu_quad_phase = CU_ElK(name='cu_quad_phase',
                                    operation="QuadPhase(i, d, f, forward, scale, nb_z, nb_mode, nx, ny)",
                                    preamble=getks('cuda/complex.cu') + getks('holotomo/cuda/quad_phase_elw.cu'),
                                    options=self.cu_options,
                                    arguments="pycuda::complex<float> *d, float *f, const bool forward,"
                                              "const float scale, const int nb_z, const int nb_mode,"
                                              "const int nx, const int ny")

        self.cu_calc2obs = CU_ElK(name='cu_calc2obs',
                                  operation="Calc2Obs(i, iobs, psi, nb_mode, nx, ny)",
                                  preamble=getks('cuda/complex.cu') + getks('holotomo/cuda/calc2obs_elw.cu'),
                                  options=self.cu_options,
                                  arguments="float *iobs,pycuda::complex<float> *psi, const int nb_mode, const int nx,"
                                            "const int ny")

        self.cu_obj_probez_mult = CU_ElK(name='cu_obj_probez_mult',
                                         operation="ObjectProbeZMult(i, obj, probe, psi, dx, dy, sample_flag, nproj,"
                                                   "nb_z, nb_obj, nb_probe, nx, ny)",
                                         preamble=getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
                                                  getks('holotomo/cuda/obj_probe_mult_elw.cu'),
                                         options=self.cu_options,
                                         arguments="pycuda::complex<float>* obj, pycuda::complex<float> *probe,"
                                                   "pycuda::complex<float>* psi, float* dx, float* dy,"
                                                   "signed char* sample_flag, const int nproj, const int nb_z,"
                                                   "const int nb_obj, const int nb_probe, const int nx, const int ny")

        self.cu_obj_probe2psi_dm1 = CU_ElK(name='obj_probe2psi_dm1',
                                           operation="ObjectProbe2PsiDM1(i, obj, probe, psi, dx, dy, sample_flag, nproj,"
                                                     "nb_z, nb_obj, nb_probe, nx, ny)",
                                           preamble=getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
                                                    getks('holotomo/cuda/obj_probe_mult_dm_elw.cu'),
                                           options=self.cu_options,
                                           arguments="pycuda::complex<float>* obj, pycuda::complex<float> *probe,"
                                                     "pycuda::complex<float>* psi, float* dx, float* dy,"
                                                     "signed char* sample_flag, const int nproj, const int nb_z,"
                                                     "const int nb_obj, const int nb_probe, const int nx, const int ny")

        self.cu_obj_probe2psi_dm2 = CU_ElK(name='obj_probe2psi_dm2',
                                           operation="ObjectProbe2PsiDM2(i, obj, probe, psi, psi_old, dx, dy,"
                                                     "sample_flag, nproj, nb_z, nb_obj, nb_probe, nx, ny)",
                                           preamble=getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
                                                    getks('holotomo/cuda/obj_probe_mult_dm_elw.cu'),
                                           options=self.cu_options,
                                           arguments="pycuda::complex<float>* obj, pycuda::complex<float> *probe,"
                                                     "pycuda::complex<float>* psi, pycuda::complex<float>* psi_old,"
                                                     "float* dx, float* dy, signed char* sample_flag,"
                                                     "const int nproj, const int nb_z, const int nb_obj,"
                                                     "const int nb_probe, const int nx, const int ny")

        self.cu_psi2probemerge = CU_ElK(name='cu_psi2probemerge',
                                        operation="Psi2ProbeMerge(i, probe, probe_new, probe_norm, nb_probe, nxy, nz)",
                                        preamble=getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
                                                 getks('holotomo/cuda/psi2obj_probe_elw.cu'),
                                        options=self.cu_options,
                                        arguments="pycuda::complex<float> *probe, pycuda::complex<float> *probe_new,"
                                                  "const float *probe_norm, const int nb_probe, const int nxy,"
                                                  "const int nz")

        self.cu_paganin_fourier = CU_ElK(name='cu_paganin_fourier',
                                         operation="paganin_fourier(i, iobs, psi, alpha, px, nb_mode, nx, ny, nz)",
                                         preamble=getks('cuda/complex.cu') + getks('holotomo/cuda/paganin_elw.cu'),
                                         options=self.cu_options,
                                         arguments="float *iobs, pycuda::complex<float> *psi, float* alpha,"
                                                   "const float px, const int nb_mode, const int nx, const int ny,"
                                                   "const int nz")

        self.cu_paganin_thickness = CU_ElK(name='cu_paganin_thickness',
                                           operation="paganin_thickness(i, iobs, obj, psi, obj_phase0, iz0, delta_beta,"
                                                     "nprobe, nobj, nx, ny, nz)",
                                           preamble=getks('cuda/complex.cu') + getks('holotomo/cuda/paganin_elw.cu'),
                                           options=self.cu_options,
                                           arguments="float * iobs, pycuda::complex<float>* obj,"
                                                     "pycuda::complex<float> *psi, half* obj_phase0, const int iz0,"
                                                     "const float delta_beta, const int nprobe, const int nobj,"
                                                     "const int nx, const int ny, const int nz")

        self.cu_projection_amplitude = CU_ElK(name='cu_projection_amplitude',
                                              operation="ProjectionAmplitude(i, iobs, psi, nb_mode, nx, ny)",
                                              preamble=getks('cuda/complex.cu') +
                                                       getks('holotomo/cuda/projection_amplitude_elw.cu'),
                                              options=self.cu_options,
                                              arguments="float *iobs, pycuda::complex<float> *psi, const int nb_mode,"
                                                        "const int nx, const int ny")

        self.cu_psi2probe = CU_ElK(name="psi2probe",
                                   operation="Psi2Probe(i, obj, probe, psi, probe_new, probe_norm, dx,"
                                             "dy, sample_flag, nb_z, nb_obj, nb_probe, nx, ny,"
                                             "weight_empty)",
                                   preamble=getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
                                            getks('holotomo/cuda/psi2obj_probe_elw.cu'),
                                   options=self.cu_options,
                                   arguments="pycuda::complex<float>* obj, pycuda::complex<float> *probe,"
                                             "pycuda::complex<float>* psi, pycuda::complex<float>* probe_new,"
                                             "float* probe_norm, float* dx, float* dy, signed char* sample_flag,"
                                             "const int nb_z, const int nb_obj, const int nb_probe, const int nx,"
                                             "const int ny, const float weight_empty")

        # Reduction kernels
        # This will compute Poisson, Gaussian, Euclidian LLK as well as the sum of the calculated intensity
        self.cu_llk = CU_RedK(my_float4, neutral="my_float4(0,0,0,0)", reduce_expr="a+b",
                              preamble=getks('cuda/complex.cu') + getks('cuda/float_n.cu') +
                                       getks('holotomo/cuda/llk_red.cu'),
                              options=self.cu_options,
                              map_expr="LLKAll(i, iobs, psi, nb_mode, nx, ny)",
                              arguments="float *iobs, pycuda::complex<float> *psi, const int nb_mode, const int nx,"
                                        "const int ny")
        # This is a reduction kernel to update each projection scale factor (is that necessary ?)
        self.cu_psi2obj_probe = CU_RedK(np.float32, neutral=0, reduce_expr="a+b", name="psi2obj_probe",
                                        map_expr="Psi2ObjProbe(i, obj, obj_old, probe, psi, probe_new, probe_norm,"
                                                 "obj_phase0, dx, dy, sample_flag, nb_z, nb_obj, nb_probe, nx, ny,"
                                                 "obj_min, obj_max, reg_obj_smooth, beta_delta,"
                                                 "weight_empty)",
                                        preamble=getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
                                                 getks('holotomo/cuda/psi2obj_probe_elw.cu'),
                                        options=self.cu_options,
                                        arguments="pycuda::complex<float>* obj, pycuda::complex<float>* obj_old,"
                                                  "pycuda::complex<float> *probe, pycuda::complex<float>* psi,"
                                                  "pycuda::complex<float>* probe_new,float* probe_norm,"
                                        # "half* obj_phase0, float* dx, float* dy, signed char* sample_flag,"
                                                  "float* obj_phase0, float* dx, float* dy, signed char* sample_flag,"
                                                  "const int nb_z, const int nb_obj, const int nb_probe, const int nx,"
                                                  "const int ny, const float obj_min, const float obj_max,"
                                                  "const float reg_obj_smooth,"
                                                  "const float beta_delta, const float weight_empty")

        self.cu_scale_obs_calc = CU_RedK(np.complex64, neutral="complexf(0,0)", name='scale_obs_calc',
                                         reduce_expr="a+b", map_expr="scale_obs_calc(i, obs, calc, nx, ny, nb_mode)",
                                         preamble=getks('cuda/complex.cu') + getks('holotomo/cuda/scale_red.cu'),
                                         arguments="float *obs, pycuda::complex<float> *calc, const int nx,"
                                                   "const int ny, const int nb_mode")

        self.cu_norm_n_c = CU_RedK(np.float32, neutral=0, name='cu_norm_n_c',
                                   reduce_expr="a+b", map_expr="pow(abs(d[i]), exponent)",
                                   preamble=getks('cuda/complex.cu'),
                                   arguments="pycuda::complex<float> *d, const float exponent")

        self.cu_psi2pos_red = CU_RedK(my_float4, neutral="my_float4(0)", reduce_expr="a+b",
                                      preamble=getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
                                               getks('cuda/float_n.cu') + getks('holotomo/cuda/psi2pos.cu'),
                                      map_expr="Psi2PosShift(i, psi, obj, probe, dx, dy, nx, ny, interp)",
                                      options=self.cu_options,
                                      arguments="pycuda::complex<float>* psi, pycuda::complex<float>* obj,"
                                                "pycuda::complex<float>* probe, float *dx, float *dy,"
                                                "const int nx, const int ny, const bool interp")
        # psi2posmerge isolated kernel
        # self.cu_psi2pos_merge = CU_ElK(name='cu_psi2pos_merge',
        #                                operation="Psi2PosMerge(dxy, dx, dy, max_shift, mult)",
        #                                options=self.cu_options,
        #                                preamble=getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
        #                                         getks('cuda/float_n.cu') + getks('holotomo/cuda/psi2pos.cu'),
        #                                arguments="my_float4* dxy, float* dx, float* dy,"
        #                                          "const float max_shift, const float mult")

        psi2pos_merge_mod = SourceModule(getks('cuda/complex.cu') + getks('cuda/bilinear.cu') +
                                         getks('cuda/float_n.cu') + getks('holotomo/cuda/psi2pos.cu'),
                                         options=self.cu_options)
        self.cu_psi2pos_merge = psi2pos_merge_mod.get_function("Psi2PosMerge")

        # Convolution kernels for support update (Gaussian)
        conv16f_mod = SourceModule(getks('cuda/complex.cu') + getks('cuda/convolution16f.cu'), options=self.cu_options)
        self.gauss_convolf_16x = conv16f_mod.get_function("gauss_convolf_16x")
        self.gauss_convolf_16y = conv16f_mod.get_function("gauss_convolf_16y")
        self.gauss_convolf_16z = conv16f_mod.get_function("gauss_convolf_16z")
        conv16c_mod = SourceModule(getks('cuda/complex.cu') + getks('cuda/convolution16c.cu'), options=self.cu_options)
        self.gauss_convolc_16x = conv16c_mod.get_function("gauss_convolc_16x")
        self.gauss_convolc_16y = conv16c_mod.get_function("gauss_convolc_16y")
        self.gauss_convolc_16z = conv16c_mod.get_function("gauss_convolc_16z")

        print("HoloTomo CUDA processing unit: compiling kernels... Done (dt=%5.2fs)" % (timeit.default_timer() - t0))

        # Init memory pool
        self.cu_mem_pool = cu_tools.DeviceMemoryPool()

    def finish(self):
        super(CUProcessingUnitHoloTomo, self).finish()
        self.cu_stream_swap.synchronize()


"""
The default processing unit 
"""
default_processing_unit = CUProcessingUnitHoloTomo()


class CUOperatorHoloTomo(OperatorHoloTomo):
    """
    Base class for a operators on HoloTomo objects using OpenCL
    """

    def __init__(self, processing_unit=None):
        super(CUOperatorHoloTomo, self).__init__()

        self.Operator = CUOperatorHoloTomo
        self.OperatorSum = CUOperatorHoloTomoSum
        self.OperatorPower = CUOperatorHoloTomoPower

        if processing_unit is None:
            self.processing_unit = default_processing_unit
        else:
            self.processing_unit = processing_unit
        if self.processing_unit.cu_ctx is None:
            # OpenCL kernels have not been prepared yet, use a default initialization
            if main_default_processing_unit.cu_device is None:
                main_default_processing_unit.use_cuda()
            self.processing_unit.init_cuda(cu_device=main_default_processing_unit.cu_device,
                                           test_fft=False, verbose=False)

    def apply_ops_mul(self, p: HoloTomo):
        """
        Apply the series of operators stored in self.ops to an object.
        In this version the operators are applied one after the other to the same object (multiplication)

        :param p: the object to which the operators will be applied.
        :return: the object, after application of all the operators in sequence
        """
        return super(CUOperatorHoloTomo, self).apply_ops_mul(p)

    def prepare_data(self, p: HoloTomo):
        stack_size, nz, ny, nx = p.data.stack_size, p.data.nz, p.data.ny, p.data.nx
        nobj, nprobe = p.nb_obj, p.nb_probe
        pu = self.processing_unit

        # TODO: make sure we use pinned/page-locked memory for better performance for all data in the host
        # Make sure data is already in CUDA space, otherwise transfer it
        if p._timestamp_counter > p._cu_timestamp_counter:
            print("Copying arrays from host to GPU")
            p._cu_timestamp_counter = p._timestamp_counter

            # This will reset the contents of stackas and make sure we get the new values from host
            p._cu_stack = HoloTomoDataStack()
            p._cu_stack_swap = HoloTomoDataStack()

            p._cu_probe = cua.to_gpu(p._probe, allocator=pu.cu_mem_pool.allocate)
            p._cu_dx = cua.to_gpu(p.data.dx.astype(np.float32), allocator=pu.cu_mem_pool.allocate)
            p._cu_dy = cua.to_gpu(p.data.dy.astype(np.float32), allocator=pu.cu_mem_pool.allocate)
            p._cu_sample_flag = cua.to_gpu(p.data.sample_flag.astype(np.int8), allocator=pu.cu_mem_pool.allocate)
            p._cu_scale_factor = cua.to_gpu(p.data.scale_factor.astype(np.float32), allocator=pu.cu_mem_pool.allocate)
            # Calc quadratic phase factor for near field propagation, z-dependent
            quad_f = np.pi * p.data.wavelength * p.data.detector_distance / p.data.pixel_size_detector ** 2
            p._cu_quad_f = cua.to_gpu_async(quad_f.astype(np.float32), allocator=pu.cu_mem_pool.allocate,
                                            stream=pu.cu_stream)
            for s in (p._cu_stack, p._cu_stack_swap):
                s.psi = cua.empty(shape=p._psi.shape, dtype=np.complex64,
                                  allocator=pu.cu_mem_pool.allocate)
                s.obj = cua.empty(shape=(stack_size, nobj, ny, nx), dtype=np.complex64,
                                  allocator=pu.cu_mem_pool.allocate)
                s.obj_phase0 = cua.empty(shape=(stack_size, nobj, ny, nx), dtype=half.type,
                                         allocator=pu.cu_mem_pool.allocate)
                s.iobs = cua.empty(shape=(stack_size, nz, ny, nx), dtype=np.float32, allocator=pu.cu_mem_pool.allocate)
                s.istack = None
            # Copy data for the main (computing) stack
            if len(p.data.stack_v) > 1:
                p = SwapStack(i=0, next_i=1, out=False, copy_psi=False, verbose=False) * p
            else:
                p = SwapStack(i=0, next_i=None, out=False, copy_psi=False, verbose=False) * p

    def timestamp_increment(self, p: HoloTomo):
        p._cu_timestamp_counter += 1


# The only purpose of this class is to make sure it inherits from CUOperatorHoloTomo and has a processing unit
class CUOperatorHoloTomoSum(OperatorSum, CUOperatorHoloTomo):
    def __init__(self, op1, op2):
        # TODO: should this apply to a single stack or all ?
        if np.isscalar(op1):
            op1 = Scale1(op1)
        if np.isscalar(op2):
            op2 = Scale1(op2)
        if isinstance(op1, CUOperatorHoloTomo) is False or isinstance(op2, CUOperatorHoloTomo) is False:
            raise OperatorException(
                "ERROR: cannot add a CUOperatorHoloTomo with a non-CUOperatorHoloTomo: %s + %s" % (str(op1), str(op2)))
        # We can only have a sum of two CLOperatorHoloTomo, so they must have a processing_unit attribute.
        CUOperatorHoloTomo.__init__(self, op1.processing_unit)
        OperatorSum.__init__(self, op1, op2)

        # We need to cherry-pick some functions & attributes doubly inherited
        self.Operator = CUOperatorHoloTomo
        self.OperatorSum = CUOperatorHoloTomoSum
        self.OperatorPower = CUOperatorHoloTomoPower
        self.prepare_data = types.MethodType(CUOperatorHoloTomo.prepare_data, self)
        self.timestamp_increment = types.MethodType(CUOperatorHoloTomo.timestamp_increment, self)


# The only purpose of this class is to make sure it inherits from CLOperatorCDI and has a processing unit
class CUOperatorHoloTomoPower(OperatorPower, CUOperatorHoloTomo):
    def __init__(self, op, n):
        CUOperatorHoloTomo.__init__(self, op.processing_unit)
        OperatorPower.__init__(self, op, n)

        # We need to cherry-pick some functions & attributes doubly inherited
        self.Operator = CUOperatorHoloTomo
        self.OperatorSum = CUOperatorHoloTomoSum
        self.OperatorPower = CUOperatorHoloTomoPower
        self.prepare_data = types.MethodType(CUOperatorHoloTomo.prepare_data, self)
        self.timestamp_increment = types.MethodType(CUOperatorHoloTomo.timestamp_increment, self)


class FreePU(CUOperatorHoloTomo):
    """
    Operator freeing CUDA memory.
    """

    def __init__(self, verbose=False):
        """

        :param verbose: if True, will detail all the free'd memory and a summary
        """
        super(FreePU, self).__init__()
        self.verbose = verbose

    def op(self, p: HoloTomo):
        """

        :param p: the HoloTomo object this operator applies to
        :return: the updated HoloTomo object
        """
        self.processing_unit.finish()

        p._from_pu()
        if self.verbose:
            print("FreePU:")
        bytes = 0

        for o in dir(p):
            if isinstance(p.__getattribute__(o), cua.GPUArray):
                if self.verbose:
                    print("  Freeing: %40s %10.3fMbytes" % (o, p.__getattribute__(o).nbytes / 1e6))
                    bytes += p.__getattribute__(o).nbytes
                p.__getattribute__(o).gpudata.free()
                p.__setattr__(o, None)
        for v in (p._cu_stack, p._cu_stack_swap):
            if v is not None:
                for o in dir(v):
                    if isinstance(v.__getattribute__(o), cua.GPUArray):
                        if self.verbose:
                            print("  Freeing: %40s %10.3fMbytes" % ("_cu_stack:" + o,
                                                                    v.__getattribute__(o).nbytes / 1e6))
                            bytes += v.__getattribute__(o).nbytes
                        v.__getattribute__(o).gpudata.free()
                        v.__setattr__(o, None)

        self.processing_unit.cu_mem_pool.free_held()
        gc.collect()
        if self.verbose:
            print('FreePU total: %10.3fMbytes freed' % (bytes / 1e6))
        return p

    def prepare_data(self, p: HoloTomo):
        # Overriden to avoid transferring any data to GPU
        pass

    def timestamp_increment(self, p):
        p._cu_timestamp_counter = 0


class MemUsage(CUOperatorHoloTomo):
    """
    Print memory usage of current process (RSS on host) and used GPU memory
    """

    def op(self, p: HoloTomo):
        """

        :param p: the HoloTomo object this operator applies to
        :return: the updated HoloTomo object
        """
        process = psutil.Process(os.getpid())
        rss = process.memory_info().rss
        gpu_mem = 0

        for o in dir(p):
            if isinstance(p.__getattribute__(o), cua.GPUArray):
                gpu_mem += p.__getattribute__(o).nbytes
        for v in (p._cu_stack, p._cu_stack_swap):
            if v is not None:
                for o in dir(v):
                    if isinstance(v.__getattribute__(o), cua.GPUArray):
                        gpu_mem += v.__getattribute__(o).nbytes

        print("Mem Usage: RSS= %6.1f Mbytes (process), GPU Mem= %6.1f Mbyts (HoloTomo object)" %
              (rss / 1024 ** 2, gpu_mem / 1024 ** 2))
        return p

    def prepare_data(self, p: HoloTomo):
        # Overriden to avoid transferring any data to GPU
        pass

    def timestamp_increment(self, p):
        # This operator does nothing
        pass


class Scale1(CUOperatorHoloTomo):
    """
    Multiply the object and/or psi by a scalar (real or complex).
    If the scale is a vector which has the size of the number of projections,
    each projection (object and/or Psi) is scaled individually

    Applies only to the current stack.
    """

    def __init__(self, x, obj=True, psi=True):
        """

        :param x: the scaling factor (can be a vector with a factor for each individual projection)
        :param obj: if True, scale the object
        :param psi: if True, scale the psi array
        """
        super(Scale1, self).__init__()
        self.x = x
        self.obj = obj
        self.psi = psi

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        if np.isscalar(self.x):
            if self.x == 1:
                return p

            if np.isreal(self.x):
                scale_k = pu.cu_scale
                x = np.float32(self.x)
            else:
                scale_k = pu.cu_scale_complex
                x = np.complex64(self.x)
            if self.obj:
                scale_k(p._cu_stack.obj, x, stream=pu.cu_stream)
            if self.psi:
                scale_k(p._cu_stack.psi, x, stream=pu.cu_stream)
        else:
            # Don't assume iproj is copied in the cu stack (probably should..)
            s = p._cu_stack
            i0 = s.iproj
            for i in range(s.nb):
                if p.data.sample_flag[i0 + i]:
                    if np.isreal(self.x[i0 + i]):
                        scale_k = pu.cu_scale
                        x = np.float32(self.x[i0 + i])
                    else:
                        scale_k = pu.cu_scale_complex
                        x = np.complex64(self.x[i0 + i])
                    if self.obj:
                        scale_k(s.obj[i], x, stream=pu.cu_stream)
                    if self.psi:
                        scale_k(s.psi[i], x, stream=pu.cu_stream)

        return p


class Scale(CUOperatorHoloTomo):
    """
    Multiply the object or probe or psi by a scalar (real or complex).
    If the scale is a vector which has the size of the number of projections,
    each projection (object and/or Psi) is scaled individually

    Will apply to all projection stacks of the HoloTomo object
    """

    def __init__(self, scale, obj=True, probe=True, psi=True):
        """

        :param scale: the scaling factor
        :param obj: if True, scale the object
        :param probe: if True, scale the probe
        :param psi: if True, scale the psi array
        """
        super(Scale, self).__init__()
        self.scale = scale
        self.obj = obj
        self.probe = probe
        self.psi = psi

    def op(self, p: HoloTomo):
        pu = self.processing_unit

        if self.probe:
            if np.isreal(self.scale):
                scale_k = pu.cu_scale
                scale = np.float32(self.scale)
            else:
                scale_k = pu.cu_scale_complex
                scale = np.complex64(self.scale)
            scale_k(p._cu_probe, scale, stream=pu.cu_stream)

        if self.obj or self.psi:
            p = LoopStack(Scale1(self.scale, obj=self.obj, psi=self.psi), copy_psi=self.psi) * p

        return p


class ScaleObjProbe1(CUOperatorHoloTomo):
    """
    Compute sum of Iobs and Icalc for 1 stack.
    """

    def __init__(self, vpsi, vobs):
        """

        :param vpsi: the array in which the psi norm sum will be stored. Should have p.data.nproj elements
        :param vobs: the array in which the observed intensity sum will be stored. Should have p.data.nproj elements
        """
        super(ScaleObjProbe1, self).__init__()
        self.vpsi = vpsi
        self.vobs = vobs

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        s = p._cu_stack
        nb_mode = np.int32(p.nb_probe * p.nb_obj)
        nx = np.int32(p.data.nx)
        ny = np.int32(p.data.ny)
        nb = s.nb

        for i in range(nb):
            r = pu.cu_scale_obs_calc(s.iobs[i], s.psi[i], nx, ny, nb_mode, stream=pu.cu_stream).get()
            self.vpsi[s.iproj + i] = r.imag
            self.vobs[s.iproj + i] = r.real

        return p


class ScaleObjProbe(CUOperatorHoloTomo):
    """
    Scale object and probe to match observed intensities. The probe amplitude is scaled to match the
    average intensity in the empty beam frames, and each object projection is set to match the average
    intensity for that projection.
    """

    def __init__(self, verbose=True):
        """

        :param verbose: if True, guess what ?
        """
        super(ScaleObjProbe, self).__init__()
        self.verbose = verbose

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        # Vector of the sum of square modulus of Probe*Obj for each projection, summed over the nz distances
        vpsi = np.empty(p.data.nproj, dtype=np.float32)
        # Sum of intensities for each projection, summed over the nz distances
        vobs = np.empty(p.data.nproj, dtype=np.float32)

        # Compute all the sums. No propagation needed (Parseval)
        p = LoopStack(ScaleObjProbe1(vpsi=vpsi, vobs=vobs) * ObjProbe2Psi1()) * p
        # Find empty beam and scale probe
        nb_empty = 0
        obs_empty = 0
        psi_empty = 0
        for i in range(p.data.nproj):
            if bool(p.data.sample_flag[i]) is False:
                nb_empty += 1
                obs_empty += vobs[i]
                psi_empty += vpsi[i]
        if nb_empty == 0:
            print("No empty beam images !? Scaling probe according to average intensity (BAD!)")
            scale_probe = np.sqrt(vobs.sum() / p.data.nb_obs) \
                          / np.sqrt(pu.cu_norm_n_c(p._cu_probe, np.float32(2)).get())
        else:
            scale_probe = np.sqrt(obs_empty / psi_empty)
        if self.verbose:
            print("Scaling probe by %6.2f" % scale_probe)
        p = Scale(scale_probe, psi=False, obj=False, probe=True) * p

        # Now scale object for each projection individually
        # TODO: should we instead scale according to average ?
        scale_obj = np.sqrt(vobs / vpsi) / scale_probe
        # No real need to scale psi, but allows a consistency check
        p = Scale(scale_obj, obj=True, psi=True, probe=False) * p
        return p


class FT1(CUOperatorHoloTomo):
    """
    Forward Fourier transform.

    Applies only to the current stack.
    """

    def __init__(self, scale=True):
        """

        :param scale: if True, the FFT will be normalized.
        """
        super(FT1, self).__init__()
        self.scale = scale

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        s = p._cu_stack
        pu.cu_fft_set_plan(s.psi, batch=True, stream=pu.cu_stream)
        cu_fft.fft(s.psi, s.psi, pu.cufft_plan, scale=False)
        if self.scale:
            pu.cu_scale(s.psi, np.float32(1 / np.sqrt(p.data.nx * p.data.ny)), stream=pu.cu_stream)
        return p


class IFT1(CUOperatorHoloTomo):
    """
    Inverse Fourier transform.

    Applies only to the current stack.
    """

    def __init__(self, scale=True):
        """

        :param scale: if True, the FFT will be normalized.
        """
        super(IFT1, self).__init__()
        self.scale = scale

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        s = p._cu_stack
        pu.cu_fft_set_plan(s.psi, batch=True, stream=pu.cu_stream)
        cu_fft.ifft(s.psi, s.psi, self.processing_unit.cufft_plan, scale=False)
        if self.scale:
            pu.cu_scale(s.psi, np.float32(1 / np.sqrt(p.data.nx * p.data.ny)), stream=pu.cu_stream)
        return p


class QuadraticPhase1(CUOperatorHoloTomo):
    """
    Operator applying a quadratic phase factor for near field propagation. The factor is different for each distance,
    based on the propagation distance stored in the HoloTomo object.

    Applies only to the current stack.
    """

    def __init__(self, forward=True, scale=1):
        """
        Application of a quadratic phase factor, and optionally a scale factor.

        The actual factor is:  :math:`scale * e^{i * factor * ((ix/nx)^2 + (iy/ny)^2)}`
        where ix and iy are the integer indices of the pixels.
        The factor is stored in the HoloTomo object.

        :param forward: if True (the default), applies the scale factor for forward propagation
        :param scale: the data will be scaled by this factor. Useful to normalize before/after a Fourier transform,
                      without accessing twice the array data.
        """
        super(QuadraticPhase1, self).__init__()
        self.scale = np.float32(scale)
        self.forward = forward

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        nb_mode = np.int32(p.nb_obj * p.nb_probe)
        nz = np.int32(p.data.nz)
        ny = np.int32(p.data.ny)
        nx = np.int32(p.data.nx)
        pu.cu_quad_phase(p._cu_stack.psi, p._cu_quad_f, self.forward, self.scale,
                         nz, nb_mode, nx, ny, stream=pu.cu_stream)

        return p


class PropagateNearField1(CUOperatorHoloTomo):
    """
    Near field propagator.

    Applies only to the current stack.
    """

    def __init__(self, forward=True):
        """

        :param forward: if True (the default), perform a forward propagation based on the experimental distances.
        """
        super(PropagateNearField1, self).__init__()
        self.forward = forward

    def op(self, p):
        s = 1.0 / (p.data.nx * p.data.ny)  # Compensates for FFT scaling
        return IFT1(scale=False) * QuadraticPhase1(forward=self.forward, scale=s) * FT1(scale=False) * p


class ObjProbe2Psi1(CUOperatorHoloTomo):
    """
    Operator multiplying object views and probe to produce the initial Psi array (before propagation)
    for all projections and distances in the stack.

    Applies only to the current stack.
    """

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        s = p._cu_stack
        nz = np.int32(p.data.nz)
        nb_obj = np.int32(p.nb_obj)
        nb_probe = np.int32(p.nb_probe)
        i0 = p.data.stack_v[s.istack].iproj
        nb = np.int32(p.data.stack_v[s.istack].nb)

        # Data size
        ny = np.int32(p.data.ny)
        nx = np.int32(p.data.nx)

        pu.cu_obj_probez_mult(s.obj[0, 0], p._cu_probe, s.psi, p._cu_dx[i0:i0 + nb], p._cu_dy[i0:i0 + nb],
                              p._cu_sample_flag[i0:i0 + nb], nb, nz, nb_obj, nb_probe, nx, ny,
                              stream=pu.cu_stream)
        return p


class LLK1(CUOperatorHoloTomo):
    """
    Log-likelihood reduction kernel. Should only be used when Psi is propagated to detector space.
    This is a reduction operator - it will write llk as an argument in the HoloTomo object, and return the object.
    This operator only applies to the current stack of projections.
    If the stack number==0, the llk is re-initialized. Otherwise it is added to the current value.
    """

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        s = p._cu_stack
        nb_mode = np.int32(p.nb_probe * p.nb_obj)
        nx = np.int32(p.data.nx)
        ny = np.int32(p.data.ny)
        nb = np.int32(s.nb)
        # TODO: instead of get() the result, store it on-GPU and get only the sum when all stacks are processed
        llk = pu.cu_llk(s.iobs[:nb], s.psi, nb_mode, nx, ny, stream=pu.cu_stream).get()
        if s.istack == 0:
            p.llk_poisson = llk['a']
            p.llk_gaussian = llk['b']
            p.llk_euclidian = llk['c']
            p.nb_photons_calc = llk['d']
        else:
            p.llk_poisson += llk['a']
            p.llk_gaussian += llk['b']
            p.llk_euclidian += llk['c']
            p.nb_photons_calc += llk['d']
        return p


class LLK(CUOperatorHoloTomo):
    """
    Compute the log-likelihood for the entire set of projections.
    Using this operator will loop through all the stacks and frames, project Obj*Probe and compute
    the llk for each. The history will be updated.
    This operator should not be while running a main algorithm, which can compute the LLK during their cycles.
    """

    def __init__(self, verbose=True):
        """
        Compute the log-likelihood
        :param verbose: if True, print the log-likelihood
        """
        super(LLK, self).__init__()

        self.verbose = verbose

    def op(self, p: HoloTomo):
        p = LoopStack(op=LLK1() * PropagateNearField1() * ObjProbe2Psi1(), out=False, copy_psi=False, verbose=False) * p
        p.update_history(mode='llk', update_obj=False, update_probe=False,
                         dt=0, algorithm='LLK', verbose=self.verbose)

        return p


class ApplyAmplitude1(CUOperatorHoloTomo):
    """
    Apply the magnitude from observed intensities, keep the phase. Masked pixels (marked using <0 intensities) are
    left unchanged.

    Applies only to the current stack.
    """

    def __init__(self, calc_llk=False):
        """

        :param calc_llk: if True, the log-likelihood will be calculated for this stack.
        """
        super(ApplyAmplitude1, self).__init__()
        self.calc_llk = calc_llk

    def op(self, p):
        if self.calc_llk:
            # TODO: use a single-pass reduction kernel to apply the amplitude and compute the LLK
            p = LLK1() * p
        pu = self.processing_unit
        s = p._cu_stack
        nb_mode = np.int32(p._probe.shape[-3] * s.obj.shape[-3])
        nx = np.int32(p.data.nx)
        ny = np.int32(p.data.ny)
        nb = np.int32(p.data.stack_v[s.istack].nb)
        pu.cu_projection_amplitude(s.iobs[:nb], s.psi, nb_mode, nx, ny, stream=pu.cu_stream)
        return p


class PropagateApplyAmplitude1(CUOperatorHoloTomo):
    """
    Propagate to detector space and apply the amplitude constraint.
    """

    def __new__(cls, calc_llk=False):
        return PropagateNearField1(forward=False) * ApplyAmplitude1(calc_llk=calc_llk) * \
               PropagateNearField1(forward=True)


class Psi2ObjProbe1(CUOperatorHoloTomo):
    """
    Operator projecting the psi arrays in sample space onto the object and probe update.
    The object can be constrained to a min and max amplitude.

    Applies only to the current stack. The probe and normalisation are stored in temporary arrays
    """

    def __init__(self, update_object=True, update_probe=True, obj_max=None, obj_min=None, reg_obj_smooth=0,
                 delta_beta=0, weight_empty=1.0):
        """

        :param update_object: if True, update the object
        :param update_probe: if True, update the probe
        :param obj_max: the maximum amplitude for the object
        :param obj_min: the minimum amplitude for the object
        :param reg_obj_smooth: the coefficient (typically 0-1) to smooth the object update
        :param delta_beta: delta/beta ratio
        :param weight_empty: the relative weight of empty beam images for the probe update
        """
        super(Psi2ObjProbe1, self).__init__()
        self.update_object = update_object
        self.update_probe = update_probe
        self.obj_max = obj_max
        self.obj_min = obj_min
        self.reg_obj_smooth = np.float32(reg_obj_smooth)
        self.beta_delta = np.float32(1 / delta_beta)
        self.weight_empty = np.float32(weight_empty)

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        s = p._cu_stack
        nz = np.int32(p.data.nz)
        nb_obj = np.int32(p.nb_obj)
        nb_probe = np.int32(p.nb_probe)
        i0 = s.iproj
        nb = np.int32(s.nb)

        if self.obj_max is None:
            obj_max = np.float32(-1)
        else:
            obj_max = np.float32(self.obj_max)
        if self.obj_min is None:
            obj_min = np.float32(-1)
        else:
            obj_min = np.float32(self.obj_min)

        # Data size
        ny = np.int32(p.data.ny)
        nx = np.int32(p.data.nx)

        if s.istack == 0:
            # TODO: do not create temporary arrays here but in parent operator (AP, DM, ML...)
            p._cu_probe_new = cua.zeros(shape=p._cu_probe.shape, dtype=np.complex64, allocator=pu.cu_mem_pool.allocate)
            p._cu_probe_norm = cua.zeros(shape=(nz, ny, nx), dtype=np.float32, allocator=pu.cu_mem_pool.allocate)
            #
            p._cu_scale_new = cua.zeros(shape=p.data.nproj, dtype=np.float32, allocator=pu.cu_mem_pool.allocate)

        if self.update_object:
            # keep copy of previous object for regularisation
            obj_old = s.obj
            if self.reg_obj_smooth > 0:
                obj_old = cua.empty(shape=s.obj.shape, dtype=np.complex64, allocator=pu.cu_mem_pool.allocate)
                cu_drv.memcpy_dtod_async(src=s.obj.gpudata, dest=obj_old.gpudata, size=s.obj.nbytes,
                                         stream=pu.cu_stream)
            # TODO: as for ptycho, it would be much more efficient to avoid this python loop
            for ii in range(nb):
                r = pu.cu_psi2obj_probe(s.obj[ii, 0], obj_old[ii, 0], p._cu_probe, s.psi[ii], p._cu_probe_new,
                                        p._cu_probe_norm, s.obj_phase0[ii, 0], p._cu_dx[i0 + ii], p._cu_dy[i0 + ii],
                                        p._cu_sample_flag[i0 + ii], nz, nb_obj, nb_probe, nx, ny, obj_min, obj_max,
                                        self.reg_obj_smooth, self.beta_delta,
                                        self.weight_empty, stream=pu.cu_stream)
                # TODO: store the result directly in p._cu_scale_new[i0 + ii], like in pyOpenCL
                cu_drv.memcpy_dtod_async(src=int(r.gpudata), dest=int(p._cu_scale_new[i0 + ii].gpudata),
                                         size=r.nbytes, stream=pu.cu_stream)
        else:
            for ii in range(nb):
                pu.cu_psi2probe(s.obj[ii, 0], p._cu_probe, s.psi[ii], p._cu_probe_new, p._cu_probe_norm,
                                p._cu_dx[i0 + ii], p._cu_dy[i0 + ii], p._cu_sample_flag[i0 + ii], nz,
                                nb_obj, nb_probe, nx, ny, self.weight_empty, stream=pu.cu_stream)

        # TODO:
        # - Take into account object inertia
        # - Prepare scale factor update, by comparing each image integrated probe intensity to the average

        return p


class Psi2ProbeMerge(CUOperatorHoloTomo):
    """
    Final update of the probe from the temporary array and the normalisation
    """

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        nz = np.int32(p.data.nz)
        nb_probe = np.int32(p.nb_probe)
        nxy = np.int32(p.data.ny * p.data.nx)
        pu.cu_psi2probemerge(p._cu_probe[0, 0], p._cu_probe_new, p._cu_probe_norm, nb_probe, nxy, nz)
        # TODO : update scale factors
        # del p._cu_probe_new, p._cu_probe_norm, p._cu_scale_new
        return p


class Psi2PosShift1(CUOperatorHoloTomo):
    """
    Update projection shifts, by comparing the updated Psi array to object*probe, for a stack of frames.
    This can only be used if there is more than one distance, as the first one is used as a reference
    """

    def __init__(self, multiplier=1, max_shift=2, save_position_history=False):
        """

        :param multiplier: the computed displacements are multiplied by this value,
            for faster convergence
        :param max_displ: the displacements (at each iteration) are capped to this
            value (in pixels), after applying the multiplier.
        :param save_position_history: if True, save the position history
            in the HoloTomo object (slow, for debugging)
        """
        super(Psi2PosShift1, self).__init__()
        self.mult = np.float32(multiplier)
        self.max_shift = np.float32(max_shift)
        self.save_position_history = save_position_history

    def op(self, p: HoloTomo):
        """

        :param p: the HoloTomo object this operator applies to
        :return: the updated HoloTomo object
        """
        pu = self.processing_unit
        s = p._cu_stack
        nz = np.int32(p.data.nz)
        if nz == 1:
            return p
        i0 = s.iproj
        nb = np.int32(s.nb)

        # Data size
        ny = np.int32(p.data.ny)
        nx = np.int32(p.data.nx)

        if self.save_position_history and (has_attr_not_none(p, 'position_history') is False):
            p.position_history = [[(p.cycle, p._cu_dx[i].get(),
                                    p._cu_dy[i].get())] for i in range(p.data.nproj)]

        for ii in range(nb):
            # TODO: use multiple streams to treat different projections & distances in //
            if p.data.sample_flag[ii]:
                for iz in range(1, nz):
                    r = pu.cu_psi2pos_red(s.psi[ii, iz, 0, 0], s.obj[ii, 0], p._cu_probe[0],
                                          p._cu_dx[i0 + ii, iz], p._cu_dy[i0 + ii, iz],
                                          nx, ny, False, stream=pu.cu_stream)
                    pu.cu_psi2pos_merge(r, p._cu_dx[i0 + ii, iz], p._cu_dy[i0 + ii, iz],
                                        self.max_shift, self.mult, stream=pu.cu_stream, block=(1, 1, 1))
                if self.save_position_history:
                    p.position_history[i0 + ii].append((p.cycle, p._cu_dx[i0 + ii].get(),
                                                        p._cu_dy[i0 + ii].get()))
        return p


class AP(CUOperatorHoloTomo):
    """
    Perform alternating projections between detector and object/probe space.

    This operator applies to all projections and loops over the stacks.
    """

    def __init__(self, update_object=True, update_probe=True, nb_cycle=1, calc_llk=False,
                 show_obj_probe=0, fig_num=None, obj_min=None, obj_max=None, reg_obj_smooth=0,
                 delta_beta=-1, weight_empty=1.0, update_pos=0, pos_max_shift=2, pos_mult=1, pos_history=False):
        """

        :param update_object: update object ?
        :param update_probe: update probe ?
        :param nb_cycle: number of cycles to perform. Equivalent to AP(...)**nb_cycle
        :param calc_llk: if True, calculate llk while in Fourier space. If a positive integer is given, llk will be
            calculated every calc_llk cycle
        :param show_obj_probe: if a positive integer number N, the object & probe will be displayed every N cycle.
            By default 0 (no plot)
        :param fig_num: the number of the figure to plot the object and probe, as for ShowObjProbe()
        :param obj_min, obj_max: min and max amplitude for the object. Can be None
        :param reg_obj_smooth: the coefficient (typically 0-1) to smooth the object update
        :param delta_beta: delta/beta ratio (typically 1e2 to 1e3) - a negative value disables the constraint
        :param weight_empty: relative weight given to empty beam images for the probe update
        :param update_pos: positive integer, if >0, update positions every 'update_pos' cycle.
            (default=False or 0, positions are not updated).
        :param pos_max_shift: maximum allowed shift (in pixels) per scan position (default=2)
        :param pos_mult: multiply the calculated position shifts by this value. Useful since the calculated
            shifts usually are a fraction of the actual shift.
        :param pos_history: if True, save the position history (for debugging, slow)
        """
        super(AP, self).__init__()
        self.update_object = update_object
        self.update_probe = update_probe
        self.nb_cycle = nb_cycle
        self.calc_llk = calc_llk
        self.show_obj_probe = show_obj_probe
        self.fig_num = fig_num
        self.obj_min = obj_min
        self.obj_max = obj_max
        self.reg_obj_smooth = reg_obj_smooth
        self.delta_beta = np.float32(delta_beta)
        self.weight_empty = weight_empty
        self.update_pos = int(update_pos)
        self.pos_max_shift = pos_max_shift
        self.pos_mult = pos_mult
        self.pos_history = pos_history

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new AP operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return AP(update_object=self.update_object, update_probe=self.update_probe, nb_cycle=self.nb_cycle * n,
                  calc_llk=self.calc_llk, show_obj_probe=self.show_obj_probe, fig_num=self.fig_num,
                  obj_min=self.obj_min, obj_max=self.obj_max, reg_obj_smooth=self.reg_obj_smooth,
                  delta_beta=self.delta_beta, update_pos=self.update_pos, pos_max_shift=self.pos_max_shift,
                  pos_mult=self.pos_mult, pos_history=self.pos_history)

    def op(self, p: HoloTomo):
        t0 = timeit.default_timer()
        ic_dt = 0
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if ic % self.calc_llk == 0 or ic == self.nb_cycle - 1:
                    calc_llk = True

            ops = PropagateApplyAmplitude1(calc_llk=calc_llk) * ObjProbe2Psi1()

            if self.update_pos:
                if ic % self.update_pos == 0:
                    ops = Psi2PosShift1(multiplier=self.pos_mult, max_shift=self.pos_max_shift,
                                        save_position_history=self.pos_history) * ops

            ops = Psi2ObjProbe1(update_object=self.update_object, update_probe=self.update_probe,
                                obj_min=self.obj_min, obj_max=self.obj_max,
                                reg_obj_smooth=self.reg_obj_smooth,
                                delta_beta=self.delta_beta, weight_empty=self.weight_empty) * ops
            p = LoopStack(ops) * p
            if self.update_probe:
                p = Psi2ProbeMerge() * p

            if calc_llk:
                # Average time/cycle over the last N cycles
                dt = (timeit.default_timer() - t0) / (ic - ic_dt + 1)
                ic_dt = ic + 1
                t0 = timeit.default_timer()

                p.update_history(mode='llk', update_obj=self.update_object, update_probe=self.update_probe,
                                 update_pos=self.update_pos, dt=dt, algorithm='AP', verbose=True)
            else:
                p.history.insert(p.cycle, update_obj=self.update_object, update_probe=self.update_probe,
                                 update_pos=self.update_pos, algorithm='AP', verbose=False)
            if self.show_obj_probe:
                if ic % self.show_obj_probe == 0 or ic == self.nb_cycle - 1:
                    s = algo_string('AP', p, self.update_object, self.update_probe)
                    tit = "%s #%3d, LLKn(p)=%8.3f" % (s, ic, p.llk_poisson / p.data.nb_obs)
                    # p = cpuop.ShowObjProbe(fig_num=self.fig_num, title=tit) * p
            p.cycle += 1
        return p


class DM1(CUOperatorHoloTomo):
    """
    Equivalent to operator: 2 * ObjProbe2Psi1() - I.
    Also makes a copy of Psi in p._cu_psi_old

    Applies only to the current stack
    """

    def op(self, p: HoloTomo):
        """

        :param p: the HoloTomo object this operator applies to
        :return: the updated Ptycho object
        """
        pu = self.processing_unit
        s = p._cu_stack
        nz = np.int32(p.data.nz)
        nb_obj = np.int32(p.nb_obj)
        nb_probe = np.int32(p.nb_probe)
        i0 = p.data.stack_v[s.istack].iproj
        nb = np.int32(p.data.stack_v[s.istack].nb)

        # Data size
        ny = np.int32(p.data.ny)
        nx = np.int32(p.data.nx)

        # Copy Psi / use memory pool so should not be wasteful
        cu_drv.memcpy_dtod_async(src=s.psi.gpudata, dest=p._cu_psi_old.gpudata, size=s.psi.nbytes, stream=pu.cu_stream)

        pu.cu_obj_probe2psi_dm1(s.obj[0, 0], p._cu_probe, s.psi, p._cu_dx[i0:i0 + nb], p._cu_dy[i0:i0 + nb],
                                p._cu_sample_flag[i0:i0 + nb], nb, nz, nb_obj, nb_probe, nx, ny,
                                stream=pu.cu_stream)

        return p


class DM2(CUOperatorHoloTomo):
    """
    # Psi(n+1) = Psi(n) - P*O + Psi_fourier

    This operator assumes that Psi_fourier is the current Psi, and that Psi(n) is in p._cu_psi_old

    Applies only to the current stack
    """

    def op(self, p: HoloTomo):
        """

        :param p: the HoloTomo object this operator applies to
        :return: the updated HoloTomo object
        """
        pu = self.processing_unit
        s = p._cu_stack
        nz = np.int32(p.data.nz)
        nb_obj = np.int32(p.nb_obj)
        nb_probe = np.int32(p.nb_probe)
        i0 = p.data.stack_v[s.istack].iproj
        nb = np.int32(p.data.stack_v[s.istack].nb)

        # Data size
        ny = np.int32(p.data.ny)
        nx = np.int32(p.data.nx)

        pu.cu_obj_probe2psi_dm2(s.obj[0, 0], p._cu_probe, s.psi, p._cu_psi_old, p._cu_dx[i0:i0 + nb],
                                p._cu_dy[i0:i0 + nb], p._cu_sample_flag[i0:i0 + nb], nb, nz, nb_obj, nb_probe, nx,
                                ny, stream=pu.cu_stream)
        return p


class DM(CUOperatorHoloTomo):
    """
    Run Difference Map algorithm between detector and object/probe space.

    This operator applies to all projections and loops over the stacks.
    """

    def __init__(self, update_object=True, update_probe=True, nb_cycle=1, calc_llk=False,
                 show_obj_probe=0, fig_num=None, obj_min=None, obj_max=None, reg_obj_smooth=0,
                 delta_beta=-1, weight_empty=1.0, update_pos=0, pos_max_shift=2, pos_mult=1, pos_history=False):
        """

        :param update_object: update object ?
        :param update_probe: update probe ?
        :param nb_cycle: number of cycles to perform. Equivalent to AP(...)**nb_cycle
        :param calc_llk: if True, calculate llk while in Fourier space. If a positive integer is given, llk will be
                         calculated every calc_llk cycle
        :param show_obj_probe: if a positive integer number N, the object & probe will be displayed every N cycle.
                               By default 0 (no plot)
        :param fig_num: the number of the figure to plot the object and probe, as for ShowObjProbe()
        :param obj_min, obj_max: min and max amplitude for the object. Can be None
        :param reg_obj_smooth: the coefficient (typically 0-1) to smooth the object update
        :param delta_beta: delta/beta ratio (typically 1e2 to 1e3) - a negative value disables the constraint
        :param weight_empty: relative weight given to empty beam images for the probe update
        :param update_pos: positive integer, if >0, update positions every 'update_pos' cycle.
            (default=False or 0, positions are not updated).
        :param pos_max_shift: maximum allowed shift (in pixels) per scan position (default=2)
        :param pos_mult: multiply the calculated position shifts by this value. Useful since the calculated
            shifts usually are a fraction of the actual shift.
        :param pos_history: if True, save the position history (for debugging, slow)
        """
        super(DM, self).__init__()
        self.update_object = update_object
        self.update_probe = update_probe
        self.nb_cycle = nb_cycle
        self.calc_llk = calc_llk
        self.show_obj_probe = show_obj_probe
        self.fig_num = fig_num
        self.obj_min = obj_min
        self.obj_max = obj_max
        self.reg_obj_smooth = reg_obj_smooth
        self.delta_beta = np.float32(delta_beta)
        self.weight_empty = weight_empty
        self.update_pos = int(update_pos)
        self.pos_max_shift = pos_max_shift
        self.pos_mult = pos_mult
        self.pos_history = pos_history

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new DM operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return DM(update_object=self.update_object, update_probe=self.update_probe, nb_cycle=self.nb_cycle * n,
                  calc_llk=self.calc_llk, show_obj_probe=self.show_obj_probe, fig_num=self.fig_num,
                  obj_min=self.obj_min, obj_max=self.obj_max, reg_obj_smooth=self.reg_obj_smooth,
                  delta_beta=self.delta_beta, weight_empty=self.weight_empty, update_pos=self.update_pos,
                  pos_max_shift=self.pos_max_shift, pos_mult=self.pos_mult, pos_history=self.pos_history)

    def op(self, p: HoloTomo):
        # First loop to get a starting Psi
        p = LoopStack(ObjProbe2Psi1(), copy_psi=True) * p

        pu = self.processing_unit
        p._cu_psi_old = cua.empty(shape=p._psi.shape, dtype=np.complex64, allocator=pu.cu_mem_pool.allocate)

        t0 = timeit.default_timer()
        ic_dt = 0
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if ic % self.calc_llk == 0 or ic == self.nb_cycle - 1:
                    calc_llk = True

            ops = DM2() * PropagateApplyAmplitude1(calc_llk=calc_llk) * DM1()

            if self.update_pos:
                if ic % self.update_pos == 0:
                    ops = Psi2PosShift1(multiplier=self.pos_mult, max_shift=self.pos_max_shift,
                                        save_position_history=self.pos_history) * ops

            p = LoopStack(Psi2ObjProbe1(update_object=self.update_object, update_probe=self.update_probe,
                                        obj_min=self.obj_min, obj_max=self.obj_max, reg_obj_smooth=self.reg_obj_smooth,
                                        delta_beta=self.delta_beta, weight_empty=self.weight_empty)
                          * ops * ObjProbe2Psi1(), copy_psi=True) * p
            if self.update_probe:
                p = Psi2ProbeMerge() * p

            if calc_llk:
                # Average time/cycle over the last N cycles
                dt = (timeit.default_timer() - t0) / (ic - ic_dt + 1)
                ic_dt = ic + 1
                t0 = timeit.default_timer()

                p.update_history(mode='llk', update_obj=self.update_object, update_probe=self.update_probe,
                                 update_pos=self.update_pos, dt=dt, algorithm='DM', verbose=True)
            else:
                p.history.insert(p.cycle, update_obj=self.update_object, update_probe=self.update_probe,
                                 update_pos=self.update_pos, algorithm='DM', verbose=False)
            if self.show_obj_probe:
                if ic % self.show_obj_probe == 0 or ic == self.nb_cycle - 1:
                    s = algo_string('DM', p, self.update_object, self.update_probe)
                    tit = "%s #%3d, LLKn(p)=%8.3f" % (s, ic, p.llk_poisson / p.data.nb_obs)
                    # p = cpuop.ShowObjProbe(fig_num=self.fig_num, title=tit) * p
            p.cycle += 1

        # Cleanup
        # del p._cu_psi_old
        return p


class Calc2Obs1(CUOperatorHoloTomo):
    """
    Copy the calculated intensities to the observed ones. Can be used for simulation.
    Assumes the current Psi is already in Fourier space.

    Applies only to the current stack.
    """

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        s = p._cu_stack
        sdata = p.data.stack_v[s.istack]
        nb = np.int32(p.data.stack_v[s.istack].nb)
        nb_mode = np.int32(p.nb_obj * p.nb_probe)
        nx = np.int32(p.data.nx)
        ny = np.int32(p.data.ny)
        pu.cu_calc2obs(s.iobs[:nb], s.psi, nb_mode, nx, ny, stream=pu.cu_stream)
        cu_drv.memcpy_dtoh_async(dest=sdata.iobs[:nb], src=s.iobs[:nb].gpudata, stream=pu.cu_stream)
        return p


class Calc2Obs(CUOperatorHoloTomo):
    """
    Copy the calculated intensities to the observed ones. Can be used for simulation.
    Will apply to all projection stacks of the HoloTomo object
    """

    def __init__(self, poisson_noise=True, nb_photon=1e3):
        """

        :param poisson_noise: if True, will add Poisson noise to the calculated intensities
        :param nb_photon: the average number of photon per pixel to use for Poisson noise
        """
        super(Calc2Obs, self).__init__()
        self.poisson_noise = poisson_noise
        self.nb_photon = nb_photon

    def op(self, p: HoloTomo):
        p = LoopStack(op=Calc2Obs1() * PropagateNearField1() * ObjProbe2Psi1(),
                      out=False, copy_psi=False, verbose=False) * p
        p._from_pu()
        if self.poisson_noise:
            iobs_sum = 0
            for s in p.data.stack_v:
                iobs_sum += s.iobs[:s.nb].sum()
            scalef = self.nb_photon * p.data.nb_obs / iobs_sum
            for s in p.data.stack_v:
                s.iobs[:s.nb] = np.random.poisson(s.iobs[:s.nb] * scalef)
        p._timestamp_counter += 1
        return p


class BackPropagatePaganin1(CUOperatorHoloTomo):
    """ Back-propagation algorithm using the single-projection approach.
    Ref: Paganin et al., Journal of microscopy 206 (2002), 33–40. (DOI: 10.1046/j.1365-2818.2002.01010.x)

    This operator uses the observed intensity to calculate a low-resolution estimate of the object, given the
    delta and beta values of its refraction index.

    The result of the transformation is the calculated object as a transmission factor, i.e. if T(r) is the
    estimated thickness of the sample, it is exp(-mu * T - 2*pi/lambda * T)

    The resulting object projection is stored in the first object mode. If the object is defined with multiple modes,
    secondary ones are set to zero.

    Applies only to the current stack.
    """

    def __init__(self, iz, delta_beta, cu_iobs_empty):
        """

        :param iz: the index of the detector distance to be taken into account (by default 0) for the propagation.
                   If None, the result from all distances will be averaged.
        :param delta_beta: delta/beta ratio, with the refraction index: n = 1 - delta + i * beta
        :param cu_iobs_empty: the GPUarray with the empty beam image used for normalisation
        """
        super(BackPropagatePaganin1, self).__init__()
        self.iz = np.int32(iz)
        self.delta_beta = np.float32(delta_beta)
        self.cu_iobs_empty = cu_iobs_empty

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        s = p._cu_stack
        nb_obj = np.int32(p.nb_obj)
        nb_probe = np.int32(p.nb_probe)
        nb_mode = np.int32(p.nb_obj * p.nb_probe)
        nx = np.int32(p.data.nx)
        ny = np.int32(p.data.ny)
        nz = np.int32(p.data.nz)
        px = np.float32(p.data.pixel_size_detector)
        nb = np.int32(s.nb)

        # Note: the calculation is done on the entire stack even if actually only 1 mode and 1 z is used,
        # this is wasteful but the operator should only be run once, so it matters little

        # 1 FT normalised observed intensity
        pu.cu_iobs2psi(s.iobs[:nb], self.cu_iobs_empty, s.psi, nb_mode, nx, ny, nz, stream=pu.cu_stream)
        if False:
            # Smooth to avoid noise ?
            sigma = np.float32(3)
            nzg = np.int32(nb * nz * nb_mode)
            pu.gauss_convolc_16x(s.psi[:nb], sigma, nx, ny, nzg, block=(16, 1, 1), grid=(1, int(ny), int(nzg)),
                                 stream=pu.cu_stream)
            pu.gauss_convolc_16y(s.psi[:nb], sigma, nx, ny, nzg, block=(1, 16, 1), grid=(int(nx), 1, int(nzg)),
                                 stream=pu.cu_stream)

        p = FT1(scale=True) * p

        # 2 Paganin operator in Fourier space
        z = p.data.detector_distance
        alpha = np.array(self.delta_beta * z * p.data.wavelength / (2 * np.pi), dtype=np.float32)
        cu_alpha = cua.to_gpu_async(alpha, allocator=pu.cu_mem_pool.allocate, stream=pu.cu_stream)
        pu.cu_paganin_fourier(s.iobs[:nb], s.psi, cu_alpha, px, nb_mode, nx, ny, nz, stream=pu.cu_stream)

        # 3 Back-propagate and compute thickness and object value
        p = IFT1(scale=True) * p
        pu.cu_paganin_thickness(s.iobs[:nb], s.obj, s.psi, s.obj_phase0, self.iz, self.delta_beta,
                                nb_probe, nb_obj, nx, ny, nz, stream=pu.cu_stream)

        # Copy back unwrapped object phase as it is not done in SwapStack
        sout = p.data.stack_v[s.istack]
        cu_drv.memcpy_dtoh_async(src=s.obj_phase0.gpudata, dest=sout.obj_phase0, stream=pu.cu_stream)

        return p


class BackPropagatePaganin(CUOperatorHoloTomo):
    """
    Back-propagation algorithm using the single-projection approach.
    Ref: Paganin et al., Journal of microscopy 206 (2002), 33–40. (DOI: 10.1046/j.1365-2818.2002.01010.x)

    This operator uses the observed intensity to calculate a low-resolution estimate of the object, given the
    delta and beta values of its refraction index.

    The result of the transformation is the calculated object as a transmission factor, i.e. if T(r) is the
    estimated thickness of the sample, it is exp(-0.5*mu * T) * exp(-0.5i * delta / beta * mu * T)

    The resulting object projection is stored in the first object mode. If the object is defined with multiple modes,
    secondary ones are set to zero.

    The probe is set to a real object of modulus sqrt(iobs_empty)

    Upon return, mu*T is stored as the real part of the Psi array (first mode)

    Applies to all projection stacks.
    """

    def __init__(self, iz=0, delta_beta=300):
        """

        :param iz: the index of the detector distance to be taken into account (by default 0) for the propagation.
        :param delta_beta: delta/beta ratio, with the refraction index: n = 1 - delta + i * beta
        """
        super(BackPropagatePaganin, self).__init__()
        self.iz = iz
        self.delta_beta = delta_beta

    def op(self, p: HoloTomo):
        # Find the empty beam image, move it to GPU to use it as normalisation (on CPU)
        nb_empty = 0
        iobs_empty = np.zeros((p.data.nz, p.data.ny, p.data.nx), dtype=np.float32)

        for s in p.data.stack_v:
            for iproj in range(s.nb):
                i = s.iproj + iproj
                if bool(p.data.sample_flag[i]) is False:
                    nb_empty += 1
                    iobs_empty += s.iobs[iproj]
        if nb_empty > 0:
            iobs_empty /= nb_empty
        else:
            iobs_empty[:] = 1

        cu_iobs_empty = cua.to_gpu(iobs_empty, allocator=self.processing_unit.cu_mem_pool.allocate)

        pu = self.processing_unit
        nx = np.int32(p.data.nx)
        ny = np.int32(p.data.ny)
        nz = np.int32(p.data.nz)
        nprobe = np.int32(p.nb_probe)
        if False:
            # Smooth iobs to avoid noise ?
            sigma = np.float32(3)
            pu.gauss_convolf_16x(cu_iobs_empty, sigma, nx, ny, np.int32(nz), block=(16, 1, 1),
                                 grid=(1, int(ny), int(nz)),
                                 stream=pu.cu_stream)
            pu.gauss_convolf_16y(cu_iobs_empty, sigma, nx, ny, np.int32(nz), block=(1, 16, 1),
                                 grid=(int(ny), 1, int(nz)),
                                 stream=pu.cu_stream)

        # Put iobs_empty in probe
        pu.cu_iobs_empty2probe(cu_iobs_empty, p._cu_probe, nprobe, nx, ny, nz, stream=pu.cu_stream)

        p = LoopStack(op=BackPropagatePaganin1(iz=self.iz, delta_beta=self.delta_beta,
                                               cu_iobs_empty=cu_iobs_empty),
                      out=True, copy_psi=True, verbose=True) * p

        return p


class SwapStack(CUOperatorHoloTomo):
    """
    Operator to swap a stack of projections to or from GPU. Note that once this operation has been applied,
    the new Psi value may be undefined (empty array), if no previous array is copied in.
    Using this operator will automatically move the host stack arrays to pinned memory.
    """

    def __init__(self, i=None, next_i=None, out=False, copy_psi=False, verbose=False):
        """
        Select a new stack of frames, swapping data between the host and the GPU. This is done using a set of
        three buffers and three queues, used to perform in parallel 1) the GPU computing, 2) copying data to the GPU
        and 3) copying data from the GPU. High speed can only be achieved if host memory is page-locked (pinned).
        Note that the buffers used for processing and copying are swapped when using this operator.
        The buffers copied are: object(in/out), iobs (in), dx (in), dy(in), sample_flag(in),
        and optionally psi(in/out).

        :param i: the new stack to use. If it is not yet swapped in (in the current 'in' buffers), it is copied
                  to the GPU.
        :param next_i: the following stack to use, for which the copy to the GPU will be initiated in the 'in' queue.
        :param out: if True (the default) and if the HoloTomo object _cu_timestamp_counter > _timestamp_counter, the
                    data from the current stack in memory will be copied back using the 'out' queue.
        :param copy_psi: if True, also copy psi arrays if they are available. If False (the default), the psi arrays
                         are not copied and the corresponding GPU buffers are released.
        :param verbose: if True, print some information when used.
        """
        super(SwapStack, self).__init__()
        self.i = i
        self.next_i = next_i
        self.out = out
        self.copy_psi = copy_psi
        self.verbose = verbose

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        scu = p._cu_stack
        sswap = p._cu_stack_swap

        # Make sure tasks are finished in each stream before beginning a new one.
        # Use events so that the wait is done asynchronously on the GPU
        pu.cu_event_calc.record(pu.cu_stream)
        pu.cu_event_swap.record(pu.cu_stream_swap)
        pu.cu_stream.wait_for_event(pu.cu_event_swap)  # Data must have arrived before being processed
        pu.cu_stream_swap.wait_for_event(pu.cu_event_calc)  # Calc must be finished before swapping out

        # Note: *if* we run out of pinned memory, we could use PageLockedMemoryPool() to also swap
        # stacks of data to/from page-locked memory. Of course this would require another
        # parallel process working on the host to move data to/from the page-locked parts... And that one
        # would also need to be asynchronous...

        self.i %= len(p.data.stack_v)  # useful when looping and i + 1 == stack_size
        if self.next_i is not None:
            self.next_i %= len(p.data.stack_v)

        if scu.istack is None:  # This can happen once at the beginning
            self.out = False

        sout = None
        if self.out:
            sout = p.data.stack_v[scu.istack]
            SwapStack._to_pinned_memory(sout, p)

        if self.i != sswap.istack:
            # desired stack is not pre-loaded, so need to swap in data in main stream
            sin = p.data.stack_v[self.i]
            SwapStack._to_pinned_memory(sin, p)
            stream = pu.cu_stream
            if sout is not None:
                cu_drv.memcpy_dtoh_async(src=scu.obj.gpudata, dest=sout.obj, stream=stream)
                # No need to copy obj_phase0 to host, this should be constant, except during Paganin
                # cu_drv.memcpy_dtoh_async(src=sswap.obj_phase0.gpudata, dest=sout.obj_phase0, stream=stream)
            cu_drv.memcpy_htod_async(dest=scu.obj.gpudata, src=sin.obj, stream=stream)
            cu_drv.memcpy_htod_async(dest=scu.obj_phase0.gpudata, src=sin.obj_phase0, stream=stream)
            cu_drv.memcpy_htod_async(dest=scu.iobs.gpudata, src=sin.iobs, stream=stream)
            if self.copy_psi:
                if sout is not None:
                    cu_drv.memcpy_dtoh_async(src=scu.psi.gpudata, dest=sout.psi, stream=stream)
                cu_drv.memcpy_htod_async(dest=scu.psi.gpudata, src=sin.psi, stream=stream)
            scu.istack = self.i
            scu.iproj = sin.iproj
            scu.nb = sin.nb
            sout = None  # Copy out has been done
        else:
            # Desired stack is pre-loaded in sswap
            # Swap stacks so calculations can continue in main stack while transfers occur in p._cu_stack_swap==scu
            p._cu_stack_swap, p._cu_stack = p._cu_stack, p._cu_stack_swap
            sswap = p._cu_stack_swap

        # Desired stack is in p._cu_stack, take care of next one and out if needed, using swap stream
        stream = pu.cu_stream_swap
        sin = None
        if self.next_i is not None:
            sin = p.data.stack_v[self.next_i]
            SwapStack._to_pinned_memory(sin, p)
            sswap.istack = self.next_i
            sswap.iproj = sin.iproj
            sswap.nb = sin.nb

        # We copy object first and record an event for this - this maye be used by algorithms which need the
        # next stack of objects for regularisation
        if sout is not None:
            cu_drv.memcpy_dtoh_async(src=sswap.obj.gpudata, dest=sout.obj, stream=stream)
            # No need to copy obj_phase0 to host, this should be constant, except during Paganin
            # cu_drv.memcpy_dtoh_async(src=sswap.obj_phase0.gpudata, dest=sout.obj_phase0, stream=stream)
        if sin is not None:
            cu_drv.memcpy_htod_async(dest=sswap.obj.gpudata, src=sin.obj, stream=stream)
            cu_drv.memcpy_htod_async(dest=sswap.obj_phase0.gpudata, src=sin.obj_phase0, stream=stream)
        pu.cu_event_swap_obj.record(stream)

        # No need to copy iobs to host, this is a constant (could we ue Texture memory ?)
        if sin is not None:
            cu_drv.memcpy_htod_async(dest=sswap.iobs.gpudata, src=sin.iobs, stream=stream)
        if self.copy_psi:
            if sout is not None:
                cu_drv.memcpy_dtoh_async(src=sswap.psi.gpudata, dest=sout.psi, stream=stream)
            if sin is not None:
                cu_drv.memcpy_htod_async(dest=sswap.psi.gpudata, src=sin.psi, stream=stream)
        return p

    @staticmethod
    def _to_pinned_memory(s: HoloTomoDataStack, p: HoloTomo):
        """
        Move a given stack to pinned (pagelocked) memory, if necessary.
        :param s: the HoloTomoDataStack to be moved to pinned memory
        :return: nothing
        """
        if not s.pinned_memory:
            print("Pinning memory for stack #%2d" % s.istack)
            for o in dir(s):
                if isinstance(s.__getattribute__(o), np.ndarray):
                    old = s.__getattribute__(o)
                    # Would using the WRITECOMBINED flag be useful ? No
                    s.__setattr__(o, cu_drv.pagelocked_empty_like(old))
                    s.__getattribute__(o)[:] = old
            if s.psi is None:
                # Psi has not yet been initialised
                s.psi = cu_drv.pagelocked_empty((p.data.stack_size, p.data.nz, p.nb_obj, p.nb_probe,
                                                 p.data.ny, p.data.nx), np.complex64)
            s.pinned_memory = True


class LoopStack(CUOperatorHoloTomo):
    """
    Loop operator to apply a given operator sequentially to the complete stack of projections of a HoloTomo object.
    This operator will take care of transferring data between CPU and GPU
    """

    def __init__(self, op, out=True, copy_psi=False, verbose=False):
        """

        :param op: the operator to apply, which can be a multiplication of operators
        :param out: if True (the default) and if the HoloTomo object _cu_timestamp_counter > _timestamp_counter, the
                    data from the current stack in memory will be copied back using the 'out' queue.
        :param copy_psi: if True, when switching between stacks, also keep psi.
        :param verbose: if True, print some information when used.
        """
        super(LoopStack, self).__init__()
        self.stack_op = op
        self.out = out
        self.copy_psi = copy_psi
        self.verbose = verbose

    def op(self, p: HoloTomo):
        if len(p.data.stack_v) == 1:
            return self.stack_op * p
        else:
            for i in range(len(p.data.stack_v)):
                p = self.stack_op * SwapStack(i, next_i=(i + 1) % len(p.data.stack_v), out=self.out,
                                              copy_psi=self.copy_psi, verbose=self.verbose) * p

        return p


class TestParallelFFT(CUOperatorHoloTomo):
    """
    Test speed on a multi-view dataset, by transferring data to/from the GPU in parallel to the FFT execution using
    concurrent queues.
    """

    def __init__(self, n_iter=5, n_stack=5, n_fft=1):
        super(TestParallelFFT, self).__init__()
        self.n_iter = n_iter
        self.n_stack = n_stack
        self.n_fft = n_fft

    def op(self, p: HoloTomo):
        pu = self.processing_unit
        cu_stream_fft = cu_drv.Stream()
        cu_stream_in = pu.cu_stream_in
        cu_stream_out = pu.cu_stream_out
        stack_size, nz, nobj, nprobe, ny, nx = p._psi.shape
        cu_psi = cua.empty(shape=p._psi.shape, dtype=np.complex64, allocator=pu.cu_mem_pool.allocate)
        cu_psi_in = cua.empty(shape=p._psi.shape, dtype=np.complex64, allocator=pu.cu_mem_pool.allocate)
        cu_psi_out = cua.empty(shape=p._psi.shape, dtype=np.complex64, allocator=pu.cu_mem_pool.allocate)

        stack_size, nz, n_obj, n_probe, ny, nx = p._psi.shape
        # Create data with pinned memory, random data to avoid any smart optimisation
        vpsi = []
        for j in range(self.n_stack):
            vpsi.append(cu_drv.pagelocked_empty(p._psi.shape, np.complex64))
            vpsi[-1][:] = np.random.uniform(0, 1, p._psi.shape)
        # Allocate 3 arrays in GPU
        cu_psi = cua.to_gpu(vpsi[0])
        cu_psi_in = cua.to_gpu(vpsi[1])
        cu_psi_out = cua.to_gpu(vpsi[2])

        # First test fft on array remaining in GPU
        pu.cu_fft_set_plan(cu_psi, batch=True, stream=cu_stream_fft)
        t0 = timeit.default_timer()
        for i in range(self.n_iter * self.n_stack):
            for k in range(self.n_fft):
                cu_fft.fft(cu_psi, cu_psi, self.processing_unit.cufft_plan, scale=False)
                cu_fft.ifft(cu_psi, cu_psi, self.processing_unit.cufft_plan, scale=False)

        pu.finish()
        dt0 = timeit.default_timer() - t0
        # This measures the number of Gbyte/s for which the n_fft FFT are calculated
        # If n_fft=0 this should correspond to the total bandwidth
        gbytes = p._psi.size * 8 * 2 * self.n_iter * self.n_stack / dt0 / 1024 ** 3

        print("Time for     on-GPU %d FFT of size %dx%dx%dx%dx%dx%d: %6.3fs [%8.2f Gbyte/s]" %
              (self.n_iter * self.n_stack * self.n_fft, stack_size, nz, n_obj, n_probe, ny, nx, dt0, gbytes))

        # test fft on array transferred sequentially to/from GPU
        t0 = timeit.default_timer()
        for i in range(self.n_iter):
            for j in range(self.n_stack):
                cu_drv.memcpy_htod_async(dest=cu_psi.gpudata, src=vpsi[j])
                for k in range(self.n_fft):
                    cu_fft.fft(cu_psi, cu_psi, self.processing_unit.cufft_plan, scale=False)
                    cu_fft.ifft(cu_psi, cu_psi, self.processing_unit.cufft_plan, scale=False)
                cu_drv.memcpy_dtoh_async(src=cu_psi.gpudata, dest=vpsi[j])
        pu.cu_ctx.synchronize()
        dt1 = timeit.default_timer() - t0
        gbytes = p._psi.size * 8 * 2 * self.n_iter * self.n_stack / dt1 / 1024 ** 3
        print("Time for    i/o GPU %d FFT of size %dx%dx%dx%dx%dx%d: %6.3fs [%8.2f Gbyte/s]" %
              (self.n_iter * self.n_stack * self.n_fft, stack_size, nz, n_obj, n_probe, ny, nx, dt1, gbytes))

        # Now perform FFT while transferring in // data to and from GPU with three queues
        t0 = timeit.default_timer()
        ev_out = None
        for i in range(self.n_iter):
            for j in range(self.n_stack):
                cu_drv.memcpy_htod_async(dest=cu_psi_in.gpudata, src=vpsi[(j + 1) % self.n_stack],
                                         stream=pu.cu_stream_in)
                for k in range(self.n_fft):
                    cu_fft.fft(cu_psi, cu_psi, self.processing_unit.cufft_plan, scale=False)
                    cu_fft.ifft(cu_psi, cu_psi, self.processing_unit.cufft_plan, scale=False)
                cu_drv.memcpy_dtoh_async(src=cu_psi_out.gpudata, dest=vpsi[(j - 1) % self.n_stack],
                                         stream=pu.cu_stream_out)
                # Swap stacks
                cu_psi_in, cu_psi, cu_psi_out = cu_psi_out, cu_psi_in, cu_psi
                # Make sure tasks are finished in each stream before beginning a new one.
                # Use events so that the wait is done asynchronously on the GPU
                ev_fft = cu_drv.Event(cu_drv.event_flags.DISABLE_TIMING)
                ev_in = cu_drv.Event(cu_drv.event_flags.DISABLE_TIMING)
                ev_out = cu_drv.Event(cu_drv.event_flags.DISABLE_TIMING)
                ev_fft.record(cu_stream_fft)
                ev_in.record(cu_stream_in)
                ev_out.record(cu_stream_out)
                cu_stream_fft.wait_for_event(ev_in)  # Data must be arrived before being processed
                cu_stream_in.wait_for_event(ev_out)  # Data out must be finished before replacing by in
                cu_stream_out.wait_for_event(ev_fft)  # Processing must be finished before transfer out
        pu.finish()
        dt2 = timeit.default_timer() - t0
        gbytes = p._psi.size * 8 * 2 * self.n_iter * self.n_stack / dt2 / 1024 ** 3
        print("Time for // i/o GPU %d FFT of size %dx%dx%dx%dx%dx%d: %6.3fs [%8.2f Gbyte/s]" %
              (self.n_iter * self.n_stack * self.n_fft, stack_size, nz, n_obj, n_probe, ny, nx, dt2, gbytes))

        return p
