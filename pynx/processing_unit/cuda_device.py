# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import warnings
warnings.filterwarnings("ignore", message="creating CUBLAS context to get version number")
import numpy as np

try:
    import pycuda.driver as cu_drv

    cu_drv.init()
    import pycuda.gpuarray as gpuarray
    from pycuda.tools import make_default_context
    from pycuda.driver import Context
    # skcuda.fft import creates a context on the first available card
    import skcuda.fft as cu_fft

    has_cuda = True
except:
    has_cuda = False
import timeit

from .cu_resources import cu_resources


def cuda_device_fft_speed(d=None, fft_shape=(16, 256, 256), batch=True, verbose=False, nb_test=4, nb_cycle=1,
                          timing=False):
    """
    Compute the FFT calculation speed for a given CUDA device.
    
    :param d: the pycuda.driver.Device. If not given, the default context will be used.
    :param fft_shape=(nz,ny,nx): the shape of the complex fft transform, treated as a stack of nz 2D transforms 
                                 of size nx * ny, or as a single 3D FFT, depending on the value of 'axes'
    :param batch: if True, will perform a batch 2D FFT. Otherwise, will perform a 3D FFT.
    :param verbose: if True, print the speed and timing for the given transform
    :param nb_test: number of time the calculations will be repeated, the best result is returned
    :param timing: if True, also return the time needed for a single FFT (dt)
    :return: The computed speed in Gflop/s (if timing is False) or a tuple (flops, dt)
    """
    if d is not None:
        ctx = cu_resources.get_context(d)

    # TODO: support auto_allocate=False once skcuda version >=0.5.2 is out.
    if batch:
        plan = cu_fft.Plan(fft_shape[-2:], np.complex64, np.complex64, batch=fft_shape[0])
    else:
        plan = cu_fft.Plan(fft_shape, np.complex64, np.complex64, batch=1)

    # TODO: use curandom
    a = gpuarray.to_gpu(np.random.uniform(0, 1, fft_shape).astype(np.complex64))
    dt = 0
    # Do N passes, best result returned
    for ii in range(nb_test):
        t00 = timeit.default_timer()
        for i in range(nb_cycle):
            cu_fft.fft(a, a, plan)
            cu_fft.ifft(a, a, plan)
        Context.synchronize()
        dtnew = (timeit.default_timer() - t00) / nb_cycle
        if dt == 0:
            dt = dtnew
        elif dtnew < dt:
            dt = dtnew
    nz, ny, nx = fft_shape
    if batch:
        # 2D FFT along x and y
        flop = 2 * 5 * nx * ny * nz * np.log2(nx * ny)
        flops = flop / dt / 1e9
        if verbose:
            print("CUFFT speed: %8.2f Gflop/s [%8.4fms per %dx%d 2D transform, %8.3fms per stack of %d 2D transforms]"
                  % (flops, dt / 2 / nz * 1000, ny, nx, dt / 2 * 1000, nz))
    else:
        # 3D FFT
        flop = 2 * 5 * nx * ny * nz * np.log2(nx * ny * nz)
        flops = flop / dt / 1e9
        if verbose:
            print("CUFFT speed: %8.2f Gflop/s [%8.4fms per %dx%dx%d 3D transform]"
                  % (flops, dt / 2 * 1000, nz, ny, nx))
    if d is not None:
        # We created the context, so destroy it
        ctx.pop()
    if timing:
        return flops, dt
    return flops


def available_gpu_speed(fft_shape=(16, 256, 256), batch=True, min_gpu_mem=None, verbose=False, gpu_name=None,
                        return_dict=False):
    """
    Get a list of all available GPUs, sorted by FFT spee( Gflop/s).
    
    Args:
        fft_shape: the FFT shape against which the fft speed is calculated
        batch: if True, perform a batch 2D FFT rather than a 3D one
        min_gpu_mem: the minimum amount of gpu memory desired (bytes). Devices with less are ignored.
        verbose: if True, printout FFT speed and memory for found GPUs
        gpu_name: if given, only GPU whose name include this sub-string will be tested & reported. This can also be a
                  list of acceptable strings
        return_dict: if True, a dictionary will be returned instead of a list, with both timing and gflops listed

    Returns:
        a list of tuples (GPU device, speed (Gflop/s)), ordered by decreasing speed.
        If return_dict is True, a dictionary is returned with each entry is a dictionary with gflops and dt results
    """
    if verbose:
        s = "Computing FFT speed for available CUDA GPU"
        if gpu_name is not None:
            s += "[name=%s]" % (gpu_name)
        if fft_shape is not None:
            s += "[ranking by fft, fft_shape=%s]" % (str(fft_shape))
        print(s + ":")

    if type(gpu_name) is str:
        gpu_names = [gpu_name]
    elif type(gpu_name) is list:
        gpu_names = gpu_name
    else:
        gpu_names = None

    gpu_dict = {}
    for i in range(cu_drv.Device.count()):
        d = cu_drv.Device(i)
        if gpu_names is not None:
            skip = True
            for n in gpu_names:
                if n.lower() in d.name().lower():
                    skip = False
                    break
            if skip:
                continue
        if fft_shape is not None:
            try:
                flops, dt = cuda_device_fft_speed(d, fft_shape=fft_shape, batch=batch, verbose=False, timing=True)
            except:
                flops, dt = 0, 0
        else:
            flops, dt = -1, -1
        if verbose:
            if fft_shape is not None:
                print("%60s: %4dMb ,%7.2f Gflop/s"
                      % (d.name(), int(round(d.total_memory() // 2 ** 20)), flops))
            else:
                print("%60s: %4dMb" % (d.name(), int(round(d.total_memory() // 2 ** 20))))
        if return_dict:
            gpu_dict[d] = {'Gflop/s': flops, 'dt': dt}
        else:
            gpu_dict[d] = flops

    if return_dict:
        return gpu_dict
    else:
        return list(sorted(gpu_dict.items(), key=lambda t: -t[1]))
