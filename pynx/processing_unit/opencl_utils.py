# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import numpy as np

import gpyfft
import pyopencl as cl
import pyopencl.array as cla

from ..utils.math import primes
from ..processing_unit import default_processing_unit
from .cl_resources import cl_resources

gpyfft_max_prime_value = None


def gpyfft_max_prime(d=None, verbose=False):
    """
    Find the largest acceptable prime number in the FFT size prime factorization for clFFT (gpyfft).

    Args:
        d: the pyopencl.Device used for testing. If none is given, the test will be made on the first available GPU
        verbose: if True, outputs the performed tests

    Returns:
        The largest prime number acceptable in the FFT size prime factorization
    """
    # This is wasteful (gpyfft plans computed repeatedly, use up GPU memory...)
    global gpyfft_max_prime_value
    if gpyfft_max_prime_value is not None:
        return gpyfft_max_prime_value
    cl_ctx = None
    if d is None:
        if default_processing_unit.cl_device is None:
            pu_language = default_processing_unit.pu_language
            # This will take into account the PYNX.PU environment variable, even if we don't care about the speed,
            # in case there the user has a good reason to select one (buggy driver, permission...).
            default_processing_unit.select_gpu(language='opencl', gpu_rank=0, ranking='order', verbose=verbose)
            cl_ctx = cl_resources.get_context(default_processing_unit.cl_device)
            # We set back the device to None, because we just used ranking by order, which would prevent any
            # further selection by speed, ... (KLUDGE ?)
            default_processing_unit.cl_device = None
            default_processing_unit.pu_language = pu_language
        else:
            cl_ctx = cl_resources.get_context(default_processing_unit.cl_device)
    else:
        cl_ctx = cl.Context([d])
    cl_queue = cl.CommandQueue(cl_ctx)
    m = 2
    for p in range(3, 300):
        if len(primes(p)) == 2:
            # prime number
            try:
                if verbose:
                    print("trying 2D FFT with size 16x%d" % (p))
                cl_psi = cla.zeros(cl_queue, (16 * p, 16 * p), np.complex64)
                gpyfft_plan = gpyfft.FFT(cl_ctx, cl_queue, cl_psi, None)
                for ev in gpyfft_plan.enqueue(forward=True):
                    ev.wait()
                for ev in gpyfft_plan.enqueue(forward=False):
                    ev.wait()
                m = p
            except gpyfft.GpyFFT_Error as er:
                break
    if verbose:
        print("Largest acceptable prime number for gpyfft: %d" % (m))
    gpyfft_max_prime_value = m
    return m
