# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2008-2015 : Univ. Joseph Fourier (Grenoble 1), CEA/INAC/SP2M
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr
#         Ondrej Mandula


import platform
import os
import sys
from setuptools import setup, find_packages, Extension
from setuptools.command.install_lib import install_lib as su_install_lib
from setuptools.command.sdist import sdist as su_sdist
from pynx.version import __version__, __authors__, __copyright__, __license__, __date__, __docformat__, get_git_version

cmdclass = {}

if 'x86_64' in platform.machine():
    try:
        from Cython.Distutils import build_ext
        import numpy

        cython_modules = [
            Extension("pynx.scattering.cpu", sources=["pynx/scattering/cpu.pyx", "pynx/scattering/c_cpu.c"],
                      include_dirs=[numpy.get_include()],
                      extra_compile_args=['-O3', '-ffast-math', '-msse', '-msse2', '-mssse3', '-msse4.1',
                                          '-march=native', '-mfpmath=sse', '-fstrict-aliasing', '-pipe',
                                          '-fomit-frame-pointer', '-funroll-loops', '-ftree-vectorize'])]
        cmdclass['build_ext'] = build_ext
    except:
        cython_modules = []
else:
    cython_modules = []


class pynx_sdist(su_sdist):
    def make_release_tree(self, base_dir, files):
        super(pynx_sdist, self).make_release_tree(base_dir, files)
        try:
            # Replace git_version_placeholder by real git version
            version_file = os.path.join(base_dir, "pynx/version.py")
            vers = open(version_file).read()
            os.remove(version_file)
            with open(version_file, "w") as fh:
                fh.write(vers.replace("git_version_placeholder", get_git_version()))
        except:
            print("sdist: replacing git_version_placeholder failed")


class pynx_install_lib(su_install_lib):
    def run(self):
        super(pynx_install_lib, self).run()
        try:
            # print(self.install_dir, self.build_dir)
            # Replace git_version_placeholder by real git version
            version_file = os.path.join(self.install_dir, "pynx/version.py")
            vers = open(version_file).read()
            os.remove(version_file)
            with open(version_file, "w") as fh:
                fh.write(vers.replace("git_version_placeholder", get_git_version()))
        except:
            print("install_lib: replacing git_version_placeholder failed")


cmdclass['sdist'] = pynx_sdist
cmdclass['install_lib'] = pynx_install_lib

setup(
    name="PyNX",
    version=__version__,
    packages=find_packages(),
    scripts=['pynx/ptycho/scripts/pynx-id01pty.py', 'pynx/ptycho/scripts/pynx-id13pty.py',
             'pynx/ptycho/scripts/pynx-id16apty.py', 'pynx/ptycho/scripts/pynx-tps25apty.py',
             'pynx/ptycho/scripts/pynx-ptypy.py', 'pynx/ptycho/scripts/pynx-cristalpty.py',
             'pynx/ptycho/scripts/pynx-ptycho-analysis.py', 'pynx/ptycho/scripts/pynx-cxipty.py',
             'pynx/cdi/scripts/pynx-id10cdi.py', 'pynx/cdi/scripts/pynx-id01cdi.py',
             'pynx/utils/scripts/pynx-resolution-FSC.py', 'pynx/cdi/scripts/pynx-cdi-analysis.py',
             'pynx/ptycho/scripts/pynx-nanomaxpty.py', 'pynx/scripts/pynx-test.py',
             'pynx/ptycho/scripts/pynx-nanoscopiumpty.py', 'pynx/ptycho/scripts/pynx-simulationpty.py'],
    cmdclass=cmdclass,
    ext_modules=cython_modules,

    install_requires=['numpy>=1.5', 'scipy>=0.17', 'setuptools', 'cython', 'matplotlib', 'scikit-image',
                      'h5py>=2.9', 'hdf5plugin', 'fabio', 'silx', 'packaging', 'psutil', 'scikit-learn'],
    extras_require={'CUDA': ['pycuda>=2017', 'scikit-cuda'], 'OpenCL': ['pyopencl>=2017', 'gpyfft>=0.7.0', 'mako'],
                    'GID': ["cctbx"], 'MPI': ['mpi4py'], 'doc': ['sphinx', 'nbsphinx', 'nbsphinx-link']},
    include_package_data=True,
    data_files=[],

    # metadata for upload to PyPI
    author="vincefn",
    author_email="favre@esrf.fr",
    description="PyNX - GPU-accelerated python toolkit for coherent X-ray imaging and nano-crystallography",
    license="CeCILL-B",
    keywords="PyNX GPU OpenCL CUDA crystallography diffraction scattering coherent X-rays Imaging ptychography CDI",
    # Should also run under windows, as long as PyOpenCL and/or PyCUDA are properly installed
    platforms=["MacOS", "POSIX", "Linux"],
    url="http://ftp.esrf.fr/pub/scisoft/PyNX/",
    long_description=
    "PyNX provides Python tools for coherent X-ray imaging and nano-crystallography: \
      1) to compute the X-ray scattering for nano-structures, using GPU (CUDA or OpenCL) acceleration, including in grazing incidence conditions.\
      2) X-rays scattering atomic scattering factors\
      3) 2D X-ray wavefront propagation\
      4) tools for ptychography reconstruction\
      5) Coherent Diffraction Imaging (2D and 3D) reconstruction algorithms",
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Education',
        'License :: License :: CEA CNRS Inria Logiciel Libre License B (CeCILL-B)',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Scientific/Engineering :: Physics',
    ],
)
