# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2018-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import sys
import unittest
import traceback
import os

exclude_cuda = False
exclude_opencl = False
if 'PYNX_PU' in os.environ:
    if 'opencl' in os.environ['PYNX_PU'].lower():
        exclude_cuda = True
    elif 'cuda' in os.environ['PYNX_PU'].lower():
        exclude_opencl = True


class TestCUDA(unittest.TestCase):
    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_pycuda(self):
        msg = "Testing for pycuda - this can fail if you only use OpenCL or CPU calculations"
        try:
            import pycuda
            import_ok = True
        except ImportError:
            msg += "\n" + traceback.format_exc()
            import_ok = False
        self.assertTrue(import_ok, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_mako(self):
        msg = "Testing for mako (needed for pycuda reduction kernels)"
        try:
            import mako
            import_ok = True
        except ImportError:
            msg += "\n" + traceback.format_exc()
            import_ok = False
        self.assertTrue(import_ok, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_skcuda_fft_import(self):
        msg = "Testing for skcuda.fft (needed for CUDA FFT calculations)"
        try:
            import skcuda.fft as cu_fft
            import_ok = True
        except ImportError:
            msg += "\n" + traceback.format_exc()
            import_ok = False
        self.assertTrue(import_ok, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_cuda_gpu_device(self):
        msg = "Number of CUDA devices should be > 0"
        import pycuda.driver as cu_drv
        nb_cuda_gpu_device = cu_drv.Device.count()
        self.assertNotEqual(nb_cuda_gpu_device, 0, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_cuda_processing_unit(self):
        from pynx.processing_unit import cu_processing_unit
        from pynx.processing_unit import default_processing_unit
        pu = cu_processing_unit.CUProcessingUnit()
        pu.init_cuda(gpu_name=None, verbose=False, test_fft=False)
        default_processing_unit.set_device(pu.cu_device, verbose=False, test_fft=False)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_pycuda_reduction(self):
        msg = "Testing pycuda sum (reduction) kernel on GPU"
        import numpy as np
        import pycuda.gpuarray as cua
        from pycuda.reduction import ReductionKernel as CU_RedK
        import pycuda.driver as cu_drv
        from pynx.processing_unit.cu_resources import cu_resources
        cu_ctx = cu_resources.get_context(cu_drv.Device(0))
        cu_psi = cua.empty((100,), np.float32)
        cu_psi.fill(np.float32(1))
        cu_sum = CU_RedK(np.float32, neutral="0", reduce_expr="a+b", map_expr="psi[i]", arguments="float* psi")
        kernel_sum = cu_sum(cu_psi).get()
        # cu_drv.Context.pop()
        self.assertAlmostEqual(kernel_sum, 100, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_cuda_elementwise(self):
        msg = "Testing pycuda sum (elementwise) kernel on GPU"
        import numpy as np
        import pycuda.gpuarray as cua
        from pycuda.elementwise import ElementwiseKernel as CU_ElK
        import pycuda.driver as cu_drv
        from pynx.processing_unit.cu_resources import cu_resources
        cu_ctx = cu_resources.get_context(cu_drv.Device(0))
        cu_psi1 = cua.empty((100,), np.float32)
        cu_psi1.fill(np.float32(1))
        cu_psi2 = cua.empty((100,), np.float32)
        cu_psi2.fill(np.float32(1))
        s = (cu_psi1.get() + cu_psi2.get()).sum()
        cu_sum = CU_ElK(name='cu_add', operation="dest[i] = src[i] + dest[i]", options=None,
                        arguments="float *src, float *dest")
        cu_sum(cu_psi1, cu_psi2)
        # cu_drv.Context.pop()
        self.assertAlmostEqual(cu_psi2.get().sum(), s, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_pycuda_fft_1D(self):
        msg = "Testing 1D cufft execution"
        fft_ok = True
        try:
            import numpy as np
            import pycuda.gpuarray as cua
            import pycuda.tools as cu_tools
            from pycuda.elementwise import ElementwiseKernel as CU_ElK
            from pycuda.reduction import ReductionKernel as CU_RedK
            import pycuda.driver as cu_drv
            from pynx.processing_unit.cu_resources import cu_resources
            cu_ctx = cu_resources.get_context(cu_drv.Device(0))
            import skcuda.fft as cu_fft
            fft_shape = (256,)
            plan = cu_fft.Plan(fft_shape, np.complex64, np.complex64, batch=1)
            a = cua.empty(fft_shape, np.complex64)
            cu_fft.fft(a, a, plan)
            cu_drv.Context.synchronize()
            cu_fft.ifft(a, a, plan)
            cu_drv.Context.synchronize()
            # cu_drv.Context.pop()
        except:
            msg += "\n" + traceback.format_exc()
            fft_ok = False

        self.assertTrue(fft_ok, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_pycuda_fft_2D(self):
        msg = "Testing 2D cufft execution"
        fft_ok = True
        try:
            import numpy as np
            import pycuda.gpuarray as cua
            import pycuda.tools as cu_tools
            from pycuda.elementwise import ElementwiseKernel as CU_ElK
            from pycuda.reduction import ReductionKernel as CU_RedK
            import pycuda.driver as cu_drv
            from pynx.processing_unit.cu_resources import cu_resources
            cu_ctx = cu_resources.get_context(cu_drv.Device(0))
            import skcuda.fft as cu_fft
            fft_shape = (256, 256)
            plan = cu_fft.Plan(fft_shape, np.complex64, np.complex64, batch=1)
            a = cua.empty(fft_shape, np.complex64)
            cu_fft.fft(a, a, plan)
            cu_drv.Context.synchronize()
            cu_fft.ifft(a, a, plan)
            cu_drv.Context.synchronize()
            # cu_drv.Context.pop()
        except:
            msg += "\n" + traceback.format_exc()
            fft_ok = False

        self.assertTrue(fft_ok, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_pycuda_fft_2D_stacked(self):
        msg = "Testing 3D cufft execution"
        fft_ok = True
        try:
            import numpy as np
            import pycuda.gpuarray as cua
            import pycuda.tools as cu_tools
            from pycuda.elementwise import ElementwiseKernel as CU_ElK
            from pycuda.reduction import ReductionKernel as CU_RedK
            import pycuda.driver as cu_drv
            from pynx.processing_unit.cu_resources import cu_resources
            cu_ctx = cu_resources.get_context(cu_drv.Device(0))
            import skcuda.fft as cu_fft
            fft_shape = (16, 128, 128)
            plan = cu_fft.Plan(fft_shape[-2:], np.complex64, np.complex64, batch=fft_shape[0])
            a = cua.empty(fft_shape, np.complex64)
            cu_fft.fft(a, a, plan)
            cu_drv.Context.synchronize()
            cu_fft.ifft(a, a, plan)
            cu_drv.Context.synchronize()
            # cu_drv.Context.pop()
        except:
            msg += "\n" + traceback.format_exc()
            fft_ok = False

        self.assertTrue(fft_ok, msg=msg)


    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_pycuda_fft_3D(self):
        msg = "Testing 3D cufft execution"
        fft_ok = True
        try:
            import numpy as np
            import pycuda.gpuarray as cua
            import pycuda.tools as cu_tools
            from pycuda.elementwise import ElementwiseKernel as CU_ElK
            from pycuda.reduction import ReductionKernel as CU_RedK
            import pycuda.driver as cu_drv
            from pynx.processing_unit.cu_resources import cu_resources
            cu_ctx = cu_resources.get_context(cu_drv.Device(0))
            import skcuda.fft as cu_fft
            fft_shape = (128, 128, 128)
            plan = cu_fft.Plan(fft_shape, np.complex64, np.complex64, batch=1)
            a = cua.empty(fft_shape, np.complex64)
            cu_fft.fft(a, a, plan)
            cu_drv.Context.synchronize()
            cu_fft.ifft(a, a, plan)
            cu_drv.Context.synchronize()
            # cu_drv.Context.pop()
        except:
            msg += "\n" + traceback.format_exc()
            fft_ok = False

        self.assertTrue(fft_ok, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_pycuda_fft_3D_delete(self):
        # Test the workaround to https://github.com/lebedov/scikit-cuda/issues/308 for CUDA 11
        msg = "Testing 3D cufft with plan destruction & re-creation"

        fft_ok = True
        try:
            import numpy as np
            import pycuda.gpuarray as cua
            import pycuda.tools as cu_tools
            from pycuda.elementwise import ElementwiseKernel as CU_ElK
            from pycuda.reduction import ReductionKernel as CU_RedK
            import pycuda.driver as cu_drv
            from pynx.processing_unit.cu_processing_unit import default_processing_unit as pu
            import skcuda.fft as cu_fft
            pu.init_cuda(test_fft=False, verbose=False)

            fft_shape = (128, 128)
            plan = pu.cu_fft_get_plan(fft_shape, batch=1, dtype_src=np.complex64, dtype_dest=np.complex64)
            a = cua.to_gpu(np.random.uniform(0, 1, fft_shape).astype(np.complex64))
            cu_fft.fft(a, a, plan)
            tmp = cua.sum(a)

            # This should be inhibited for CUDA 11
            pu.cu_fft_free_plans()

            plan = pu.cu_fft_get_plan(fft_shape, batch=1, dtype_src=np.complex64, dtype_dest=np.complex64)
            cu_fft.fft(a, a, plan)
            tmp = cua.sum(a)

            cu_drv.Context.synchronize()
        except:
            msg += "\n" + traceback.format_exc()
            fft_ok = False

        self.assertTrue(fft_ok, msg=msg)


    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_cuda_device_available_gpu_speed(self):
        msg = "Testing pynx.processing_unit.cuda_device.available_gpu_speed()"
        try:
            from pynx.processing_unit.cuda_device import available_gpu_speed
            tmp = available_gpu_speed(fft_shape=(16, 32, 32), verbose=False)
            ok = True
        except:
            msg += "\n" + traceback.format_exc()
            ok = False

        self.assertTrue(ok, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_cu_processing_unit_init(self):
        msg = "Testing pynx.processing_unit.cu_processing_unit.CUProcessingUnit.init_cuda"
        try:
            from pynx.processing_unit.cu_processing_unit import CUProcessingUnit
            u = CUProcessingUnit()
            u.init_cuda(fft_size=(32, 64, 64), verbose=False)
            ok = True
        except:
            msg += "\n" + traceback.format_exc()
            ok = False

        self.assertTrue(ok, msg=msg)

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests excluded")
    def test_processing_unit_select_gpu_cu(self):
        msg = "Testing pynx.processing_unit.ProcessingUnit.select_gpu(language='cuda')"
        try:
            from pynx.processing_unit import ProcessingUnit
            u = ProcessingUnit()
            u.set_benchmark_fft_parameters((16, 32, 32), batch=True)
            u.select_gpu(language='cuda', verbose=False)
            ok = True
        except:
            msg += "\n" + traceback.format_exc()
            ok = False

        self.assertTrue(ok, msg=msg)


def suite():
    loadTests = unittest.defaultTestLoader.loadTestsFromTestCase
    test_suite = unittest.TestSuite([loadTests(TestCUDA)])
    return test_suite


if __name__ == '__main__':
    res = unittest.TextTestRunner(verbosity=2, descriptions=False).run(suite())
