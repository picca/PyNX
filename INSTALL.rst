Installation
============
*PyNX* supports python version 3.5 and above (3.4 is deprecated)

You should follow `Full installation in a python virtualenv`_, which can be used on any system which supports a python
virtual environment. Installation has been tested on linux (e.g. debian >=8) systems as well as macOS computers.

If you are using an nVidia card and already have CUDA development tools, you can also use the
:ref:`quick install <quick_install>` which only uses ``pip``.

Generic Instructions
====================
*PyNX* is focused on using Graphical Processing Units (GPU) for faster calculations, so you will need:

* a GPU (which can be an integrated GPU)
* an OpenCL installation (drivers and libraries)
* and/or CUDA (which gives better performance), which requires CUDA drivers and development tools (nvcc)

PyNX should still work on a CPU only, but without any optimisation, and will therefore be very slow, especially for
3D CDI and Ptychography algorithms.

.. _quick_install:

Quick installation (nVidia/CUDA only)
-------------------------------------
This allows to install PyNX quickly (but *without OpenCL support*), assuming that you already have:

 * a python (>=3.5) installation
 * pip for python package installation
 * already-installed CUDA development tools (nvcc)

You can install PyNX and the dependencies using the following commands:

.. code-block:: bash

  pip install --upgrade pip
  pip install setuptools wheel --upgrade
  pip install numpy cython scipy matplotlib ipython notebook scikit-image ipywidgets ipympl
  pip install h5py hdf5plugin h5glance silx fabio
  pip install pycuda scikit-cuda
  pip install http://ftp.esrf.fr/pub/scisoft/PyNX/pynx-latest.tar.bz2

Once this has been run, you can :ref:`test pynx <testing>`

Full installation in a python virtualenv
----------------------------------------
The following script should work on any POSIX (Linux, MacOS X) system, and requires:

 * a python (>=3.5) installation
 * pip for python package installation
 * GPU dependencies, for CUDA and/or OpenCL (at least one should be present, both can be used):

   * OpenCL libraries (out-of-the box on MacOS X, using nvidia/AMD drivers on Linux)
   * CUDA libraries and development tools
 * git, cmake and standard development tools (for C/C++/python, depending on the operating system)

The script can be found in the source code as 'install_scripts/install-pynx-venv.sh',
or can be downloaded from http://ftp.esrf.fr/pub/scisoft/PyNX/install-scripts/install-pynx-venv.sh

Once this has been run, you can :ref:`test pynx <testing>`

Development version
-------------------
If you want to live on the wild side, you can install the (public) development version (updated nightly) using:

.. code-block:: bash

  pip install http://ftp.esrf.fr/pub/scisoft/PyNX/pynx-devel-nightly.tar.bz2

.. _testing:

Testing the installation
------------------------
Once installed, you can test pynx from the console by using:

.. code-block:: bash

  pynx-test.py

To also test live-plotting, you can run:

.. code-block:: bash

  pynx-test.py live_plot

You can also run more specific tests using command-line keywords (combinations are possible):
 * ``pynx-test.py processing_unit`` : only run basic OpenCL and CUDA tests
 * ``pynx-test.py cdi`` : only run CDI tests
 * ``pynx-test.py cdi_runner`` : only run CDI runner tests
 * ``pynx-test.py ptycho`` : only run ptychography tests
 * ``pynx-test.py ptycho_runner`` : only run ptychography runner tests
 * ``pynx-test.py cuda`` : only run CUDA tests
 * ``pynx-test.py opencl`` : only run opencl tests

Dependencies
------------
Requirements:

* git, cmake and standard development tools (compilers, headers...)

* Python packages (all installable using pip):

 * numpy, scipy, matplotlib
 * cython (version>=0.24 for gpyfft)
 * scikit-image, scikit-learn
 * h5py hdf5plugin
 * silx fabio
 * psutil

* Recommended:

 * ipython, notebook
 * pyqt5, pyopengl (for silx viewer)
 * pyfftw (https://pypi.python.org/pypi/pyFFTW) for cpu calculations
 * sphinx and nbsphinx to generate documentation

* For OpenCL

 * pyopencl (>=2016.1 for gpyfft), mako
 * clFFT and gpyfft (>=0.7.0) (these two must be installed from source)

* For CUDA:

 * CUDA development tools (nvcc)
 * pycuda
 * scikit-cuda

* Optionally:

 * the cctbx library, if you want to use the *pynx.scattering.gid* module for grazing incidence scattering.
   This is a bit complex to install, so it should probably be installed first, before all other packages.

 * pandoc for sphinx documentation generation
