# !/bin/bash

############################# NOTES
# An 8Gb image is not large enought to install all the requirements (CUDA toolkit) and libraries.
# 20Gb suggested for test, plus what is needed for data

# This is an installation of debian 10 (Buster, testing as of 03/2018) from the AWS Debian 9 image

############################# Upgrade to debian 10 ##########################################

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get clean

sed -i 's/stretch/buster/g' /etc/apt/sources.list

echo "deb http://httpredir.debian.org/debian/ buster contrib non-free" >> buster-contrib-nonfree-backports.list
sudo mv buster-contrib-nonfree-backports.list /etc/apt/sources.list.d/

sudo apt-get update
sudo apt-get upgrade -y         # INTERACTIVE
sudo apt-get dist-upgrade -y    # INTERACTIVE

############################# Install base packages ##########################################

sudo apt-get install -y git cmake vim xauth x11-apps fftw3 fftw3-dev aptitude g++ eog rsync

# We install matplotlib to get all graphical packages dependencies, it will be later installed in the virtualenv
sudo apt-get install -y python3-matplotlib python3-virtualenv virtualenv python3-dev

########################## nVidia packages (not availble yet for stretch) #######################################
sudo apt-get install -y nvidia-driver nvidia-smi libcuda1 nvidia-cuda-dev nvidia-cuda-toolkit clinfo    # INTERACTIVE

sudo apt-get autoremove -y
sudo apt-get clean

# Needed to activate drivers and deactivate nouveau
sudo /sbin/reboot                                      # REBOOT

# Need manual install of nvidia kernel module ? WHY ??
sudo dkms autoinstall -m nvidia
sudo modprobe nvidia

########################### NEW USER
sudo adduser pynx                       # INTERACTIVE: enter password

sudo apt-get install fail2ban -y        # More secure
sudo vim /etc/fail2ban/jail.conf        # need to whitelist esrf and epn-campus
    => ignoreip = 127.0.0.1/8 ::1 160.103.0.0/16 193.49.0.0/16

sudo service fail2ban restart

sudo vim /etc/ssh/sshd_config           # INTERACTIVE: Enable PasswordAuthentication

sudo service sshd restart

############### The following should be done as the user who will do the calculations, or a shared directory

# Use the 'install-pynx-venv.sh' script for installation

su - pynx
cd
wget http://ftp.esrf.fr/pub/scisoft/PyNX/install-scripts/install-pynx-venv.sh
chmod +x install-pynx-venv.sh
./install-pynx-venv.sh pynx-env python3.7


#####################################################################################################################
# The following is necessary as of 01/2019, because nvidia-graphics-drivers version 390.87 is not
# compatible with nvidia-cuda-toolkit version 9.2.
# Cf https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=919647   (if bug is solved, not necessary anymore !)

echo "deb-src http://httpredir.debian.org/debian/ experimental main contrib non-free" >> experimental-src.list
sudo mv experimental-src.list /etc/apt/sources.list.d/

sudo apt install devscripts
sudo apt-get build-dep nvidia-graphics-drivers

mkdir deb_src
apt-get source nvidia-graphics-drivers/experimental
cd nvidia-graphics-drivers-410.93
debuild -us -uc
cd ..
sudo apt remove nvidia-smi libxnvctrl0 nvidia-persistenced nvidia-settings # Remove old packages
sudo apt install nvidia-modprobe    # Still 390.87 version, but needed for 410.x ?
sudo dpkg -i libcuda1_410.93-1_amd64.deb libegl-nvidia0_410.93-1_amd64.deb libgles-nvidia1_410.93-1_amd64.deb libgles-nvidia2_410.93-1_amd64.deb libglx-nvidia0_410.93-1_amd64.deb libglx-nvidia0_410.93-1_amd64.deb libnvcuvid1_410.93-1_amd64.deb libnvidia-cfg1_410.93-1_amd64.deb libnvidia-compiler_410.93-1_amd64.deb libnvidia-eglcore_410.93-1_amd64.deb libnvidia-fatbinaryloader_410.93-1_amd64.deb libnvidia-glcore_410.93-1_amd64.deb libnvidia-ml1_410.93-1_amd64.deb libnvidia-ptxjitcompiler1_410.93-1_amd64.deb nvidia-alternative_410.93-1_amd64.deb nvidia-detect_410.93-1_amd64.deb nvidia-driver-bin_410.93-1_amd64.deb nvidia-driver-libs_410.93-1_amd64.deb nvidia-driver_410.93-1_amd64.deb nvidia-egl-common_410.93-1_amd64.deb nvidia-egl-icd_410.93-1_amd64.deb nvidia-kernel-dkms_410.93-1_amd64.deb nvidia-kernel-support_410.93-1_amd64.deb nvidia-legacy-check_410.93-1_amd64.deb nvidia-opencl-common_410.93-1_amd64.deb nvidia-opencl-icd_410.93-1_amd64.deb nvidia-smi_410.93-1_amd64.deb nvidia-vdpau-driver_410.93-1_amd64.deb nvidia-vulkan-common_410.93-1_amd64.deb nvidia-vulkan-icd_410.93-1_amd64.deb xserver-xorg-video-nvidia_410.93-1_amd64.deb libgl1-nvidia-glvnd-glx_410.93-1_amd64.deb libnvidia-glvkspirv_410.93-1_amd64.deb

sudo /sbin/reboot                                      # REBOOT
