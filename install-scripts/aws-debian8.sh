# !/bin/bash

############################# NOTES
# An 8Gb image is not large enought to install all the requirements (CUDA toolkit) and libraries.
# 20Gb suggested for test, plus what is needed for data

# (apt-get is old school, should just use apt !)

############################# Install packages ####################################################
sudo apt-get update
sudo apt-get dist-upgrade -y

cd
rm -f jessie-contrib-nonfree-backports.list
echo "deb http://httpredir.debian.org/debian/ jessie contrib non-free" >> jessie-contrib-nonfree-backports.list
echo "deb http://httpredir.debian.org/debian/ jessie-backports main contrib non-free" >> jessie-contrib-nonfree-backports.list
sudo mv jessie-contrib-nonfree-backports.list  /etc/apt/sources.list.d/
sudo apt-get update

sudo apt-get install -y git cmake vim xauth x11-apps fftw3 fftw3-dev aptitude g++ eog

# We install matplotlib to get all graphical packages dependencies, it will be later installed in the virtualenv
sudo apt-get install -y python3-matplotlib python3-virtualenv virtualenv python3-dev

# Safer before installing nvidia drivers - to have up-to-date kernel
sudo  /sbin/reboot                                      # REBOOT

########################## nVidia drivers #######################################
# Note: do NOT install opencl-headers from backports, as it is an OpenCL 2.0 version...
sudo apt-get install -y opencl-headers
sudo apt-get -t jessie-backports install -y nvidia-driver nvidia-smi libcuda1 nvidia-cuda-dev nvidia-cuda-toolkit clinfo

# Needed to activate drivers and maybe deactivate nouveau
sudo  /sbin/reboot                                      # REBOOT

########################### NEW USER
sudo adduser pynx                       # INTERACTIVE: enter password
sudo apt-get install fail2ban

sudo vim /etc/ssh/sshd_config           # INTERACTIVE: Enable PasswordAuthentication

sudo service sshd restart

############### The following should be done as the user who will do the calculations, or a shared directory
########################## Python virtual environment ###########################
export BASEDIR=$PWD
cd $BASEDIR
mkdir -p pynx-env
virtualenv -p python3.4 pynx-env/
source pynx-env/bin/activate

########################## PyNX dependencies
pip install --upgrade pip setuptools wheel
pip install numpy cython scipy matplotlib ipython notebook scikit-image --upgrade
pip install mako pybind11 pyopencl h5py hdf5plugin silx pillow lxml fabio pycuda scikit-cuda pyopengl --upgrade

# SIP must be installed from source, and as it installs a header in ..env/include/python3.4m , that include dir cannot be a symlink to the original python include dir
cd $BASEDIR/pynx-env/include
mv python3.4m python3.4m-lnk
mkdir python3.4m
cp -Rf python3.4m-lnk/* python3.4m/
rm -Rf python3.4m-lnk
cd $BASEDIR/dev
wget https://downloads.sourceforge.net/project/pyqt/sip/sip-4.19.3/sip-4.19.3.tar.gz
tar -xzf sip-4.19.3.tar.gz
cd sip-4.19.3/
python configure.py
make install

pip install pyqt5 --upgrade --no-deps


########################## clFFT
# Install clfft, also in the virtualenv (recent versions allow prime numbers up to 13)
mkdir -p $BASEDIR/pynx-env/dev
cd $BASEDIR/pynx-env/dev
git clone https://github.com/clMathLibraries/clFFT.git
cd clFFT
git checkout tags/v2.12.2                               # NB: newer git requires cmake>=3.1 !
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$BASEDIR/pynx-env ../src
make all install

# Make sure LD_LIBRARY_PATH and LIBRARY_PATH is included when activating the python virtual environment, so that
# the clfft library can be found
if  [[ "$OSTYPE" == "linux"* ]]; then
    mv $BASEDIR/pynx-env/bin/activate $BASEDIR/pynx-env/bin/activate.ORIG
    awk '{sub("unset VIRTUAL_ENV","if [ -n \"$_OLD_LD_LIBRARY_PATH\" ] ; then\n        LD_LIBRARY_PATH=\"$_OLD_LD_LIBRARY_PATH\"\n      export LD_LIBRARY_PATH\n      unset _OLD_LD_LIBRARY_PATH\n        LIBRARY_PATH=\"$_OLD_LIBRARY_PATH\"\n      export LIBRARY_PATH\n      unset _OLD_LIBRARY_PATH\n    fi\n    unset VIRTUAL_ENV")};1' $BASEDIR/pynx-env/bin/activate.ORIG > $BASEDIR/pynx-env/bin/activate_tmp
    awk '{sub("export VIRTUAL_ENV","export VIRTUAL_ENV\n\n_OLD_LD_LIBRARY_PATH=\"$LD_LIBRARY_PATH\"\nLD_LIBRARY_PATH=\"$VIRTUAL_ENV/lib:$VIRTUAL_ENV/lib64:$LD_LIBRARY_PATH\"\nexport LD_LIBRARY_PATH\n \nOLD_LIBRARY_PATH=\"$LIBRARY_PATH\"\nLIBRARY_PATH=\"$VIRTUAL_ENV/lib:$VIRTUAL_ENV/lib64:$LIBRARY_PATH\"\nexport LIBRARY_PATH\n")};1' $BASEDIR/pynx-env/bin/activate_tmp > $BASEDIR/pynx-env/bin/activate
    #rm -f $BASEDIR/pynx-env/bin/activate_tmp
elif [[ "$OSTYPE" == "darwin"* ]]; then
    mv $BASEDIR/pynx-env/bin/activate $BASEDIR/pynx-env/bin/activate.ORIG
    awk '{sub("unset VIRTUAL_ENV","if [ -n \"$_OLD_DYLD_LIBRARY_PATH\" ] ; then\n        DYLD_LIBRARY_PATH=\"$_OLD_DYLD_LIBRARY_PATH\"\n      export DYLD_LIBRARY_PATH\n      unset _OLD_DYLD_LIBRARY_PATH\n    fi\n    unset VIRTUAL_ENV")};1' $BASEDIR/pynx-env/bin/activate.ORIG > $BASEDIR/pynx-env/bin/activate_tmp
    awk '{sub("export VIRTUAL_ENV","export VIRTUAL_ENV\n\n_OLD_DYLD_LIBRARY_PATH=\"$DYLD_LIBRARY_PATH\"\nDYLD_LIBRARY_PATH=\"$VIRTUAL_ENV/lib:$VIRTUAL_ENV/lib64:$DYLD_LIBRARY_PATH\"\nexport DYLD_LIBRARY_PATH\n")};1' $BASEDIR/pynx-env/bin/activate_tmp > $BASEDIR/pynx-env/bin/activate
    rm -f $BASEDIR/pynx-env/bin/activate_tmp
fi

# Activate again with the new environment variable
deactivate
cd $BASEDIR
source pynx-env/bin/activate

################################# Install gpyfft
#more tricky ! May require hand modifications in setup.py..
cd $BASEDIR/pynx-env/dev
git clone https://github.com/geggo/gpyfft.git
cd gpyfft
## Get working version, gpyfft too often has incompatible changes
git checkout tags/v0.7.0
mv setup.py setup.py.ORIG
awk '{sub("/Users/gregor/Devel/","'"$BASEDIR"'" "/pynx-env/dev/")};1' setup.py.ORIG > tmp1.py
awk '{sub("/home/gregor/devel/","'"$BASEDIR"'" "/pynx-env/dev/")};1' tmp1.py > tmp2.py
awk '{sub("/usr/local/lib64","'"$BASEDIR"'" "/pynx-env/lib/")};1' tmp2.py > tmp1.py
awk '{sub("os.path.join\\(CLFFT_DIR, \47src\47, \47include\47\\)", "os.path.join(CLFFT_DIR, \"src\", \"include\"), \"'"$BASEDIR"'" "/pynx-env/include/\"")};1' tmp1.py > setup.py

rm -f tmp*.py
python setup.py install


################################## PyNX install
cd $BASEDIR/pynx-env/dev
# From git: requires a (free, open) gitlab account on https://gitlab.esrf.fr
git clone https://gitlab.esrf.fr/favre/PyNX.git pynx
# Alternatively, get the last released version from ftp:
# NB: 'http_proxy=' is required from inside the ESRF to deactivate the proxy
# curl -O http://ftp.esrf.fr/pub/scisoft/PyNX/pynx-latest.tar.bz2
# tar -xjf pynx-latest.tar.bz2
cd pynx
python setup.py install

