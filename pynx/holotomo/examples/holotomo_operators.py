# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import numpy as np
import matplotlib.pyplot as plt
import imageio
from scipy.misc import face, ascent
from pynx.holotomo import *
from pynx.holotomo.utils import simulate_probe
from pynx.wavefront import Wavefront
from pynx.wavefront import operator as wop
from pynx.test.speed import SpeedTest
from pynx.ptycho.simulation import spiral_archimedes

# Basic parameters
delta=1e-6
beta=3e-9
wavelength=12.3984e-10/17.5
thickness = 20e-6
pixel_size = 50e-9
ny, nx = 512, 512
nb_proj = 15
stack_size = 3
nz = 3
dz = 0.03  # Base distance
vz = (np.arange(nz) + 1) * dz

# Simulate probe (propagation distances)
probe0 = simulate_probe((ny,nx), np.ones(nz)*0.2, nb_line_h=10, nb_line_v=10,
                        pixel_size=pixel_size, wavelength=wavelength, amplitude=10)
probe0 = probe0.reshape((nz, 1, ny, nx))

# Create object
k = 2*np.pi/wavelength
mu = 2 * k * beta
print(mu*thickness, k*delta*thickness)
obj0 = np.empty((nb_proj, 512, 512), dtype=np.complex64)

# Get photo as model
if os.path.exists('Fox_-_British_Wildlife_Centre_%2817429406401%29.jpg') is False:
    os.system('curl -O https://upload.wikimedia.org/wikipedia/commons/1/16/Fox_-_British_Wildlife_Centre_%2817429406401%29.jpg')
img = np.array(imageio.imread("Fox_-_British_Wildlife_Centre_%2817429406401%29.jpg")[:,:,1]).astype(np.float32)
img /= img.max()
nyi, nxi = img.shape
x, y = spiral_archimedes(200, nb_proj-1)
for i in range(0, nb_proj-1):
    yc, xc = nyi//2+int(y[i]), nxi//2+int(x[i])
    ny2, nx2 = ny//2, nx//2
    obj0[i] = np.exp(-k * (-1j * delta + beta) * thickness * img[yc-ny2:yc+ny2, xc-nx2:xc+nx2])

# last projection is without an object (empty beam)
obj0[-1] = 0

nb_mode = 1
obj0 = np.reshape(obj0, (nb_proj, nb_mode, ny, nx))

sample_flag = np.ones(nb_proj, dtype=np.bool)
sample_flag[-1] = False

iobs = np.ones((nb_proj, nz, ny, nx), dtype=np.float32)

# Create HoloTomoData
data = HoloTomoData(iobs, pixel_size_detector=pixel_size, wavelength=wavelength, detector_distance=vz,
               stack_size=stack_size, sample_flag=sample_flag)

# Create PCI object
p = HoloTomo(data=data, obj=obj0.copy(), probe=probe0)

# Copy calculated intensity to obs
p = Calc2Obs(poisson_noise=True, nb_photon=1e4) * p

if True:
    p.set_probe(np.ones_like(p._probe))
    # p.set_obj(np.ones((nb_proj, ny, nx)))
    p.set_obj(np.random.uniform(0.95, 1, (nb_proj, ny, nx)) * np.exp(1j*np.random.uniform(0., 1, (nb_proj, ny, nx))))
    #p = ScaleObjProbe(obj_max=1, verbose=True) * p

p = DM(update_probe=True, obj_min=0.99, obj_max=1.0, calc_llk=10)**20 * p
p = AP(update_probe=True, obj_min=0.99, obj_max=1.0, calc_llk=10)**20 * p
