# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

__all__ = ['CDI', 'OperatorCDI', 'save_cdi_data_cxi']

import time
import timeit
import sys
import warnings
from copy import deepcopy
import numpy as np
from scipy.fftpack import fftn, ifftn, fftshift, fftfreq
from scipy.signal import convolve
from ..operator import Operator, OperatorException, has_attr_not_none
from ..version import get_git_version

_pynx_version = get_git_version()
from ..utils.history import History

try:
    # We must import hdf5plugin before h5py, even if it is not used here.
    import hdf5plugin
except:
    pass
import h5py


class CDI:
    """
    Reconstruction class for two or three-dimensional coherent diffraction imaging data.
    """

    def __init__(self, iobs, support=None, obj=None, mask=None,
                 pixel_size_detector=None, wavelength=None, detector_distance=None):
        """
        Constructor. All arrays are assumed to be centered in (0,0) to avoid fftshift

        :param iobs: 2D/3D observed diffraction data (intensity).
          Assumed to be corrected and following Poisson statistics, will be converted to float32.
          Dimensions should be divisible by 4 and have a prime factor decomposition up to 7.
          Internally, the following special values are used:
          * values<=-1e19 are masked. Among those, values in ]-1e38;-1e19] are estimated values,
            stored as -(iobs_est+1)*1e19, which can be used to make a loose amplitude projection.
            Values <=-1e38 are masked (no amplitude projection applied), just below the minimum
            float32 value
          * -1e19 < values <= 1 are observed but used as free pixels
          If the mask is not supplied, then it is assumed that the above special values are
          used.
        :param support: initial support in real space (1 = inside support, 0 = outside)
        :param obj: initial object. If None, will be initialized to a complex object, with uniform random
          0<modulus<1 and 0<phase<2pi. The data is assumed to be centered in (0,0) to avoid fft shifts.
        :param mask: mask for the diffraction data (0: valid pixel, >0: masked)
        :param pixel_size_detector: detector pixel size (meters)
        :param wavelength: experiment wavelength (meters)
        :param detector_distance: detector distance (meters)
        """
        self.iobs = iobs.astype(np.float32)
        # Iobs sum, taking into account masked and free pixels (stored as -iobs-1)
        self.iobs_sum = (self.iobs * (self.iobs >= 0)).sum()
        self.iobs_sum -= (self.iobs[np.logical_and(self.iobs > -1e19, self.iobs < 0)] + 1).sum()

        if support is not None:
            self._support = support.copy().astype(np.int8)
        else:
            # Support will be later updated
            self._support = np.ones_like(iobs, dtype=np.int8)

        if obj is None:
            a, p = np.random.uniform(0, 1, iobs.shape), np.random.uniform(0, 2 * np.pi, iobs.shape)
            if self._support is not None:
                self._obj = (a * np.exp(1j * p) * self._support).astype(np.complex64)
            else:
                self._obj = (a * np.exp(1j * p)).astype(np.complex64)
        else:
            self._obj = obj.copy().astype(np.complex64)

        self._is_in_object_space = True

        if mask is not None:
            # Avoid needing to store/transfer a mask in GPU. Flag masked pixels with iobs < -1e38
            self.iobs[mask > 0] = -1.05e38

        # reciprocal space kernel for the point spread function. This is used for partial coherence correction.
        self._k_psf = None

        self.pixel_size_detector = pixel_size_detector
        self.wavelength = wavelength
        self.detector_distance = detector_distance
        self.pixel_size_object = None
        self.lambdaz = None

        # Experimental parameters
        if self.wavelength is not None and self.detector_distance is not None:
            self.lambdaz = self.wavelength * self.detector_distance
        if self.lambdaz is not None and self.pixel_size_detector is not None and iobs is not None:
            # TODO: correctly compute pixel size, at least for x and y, when number of pixels differ along directions
            self.pixel_size_object = self.lambdaz / (self.pixel_size_detector * self.iobs.shape[-1])
        # TODO: correctly compute orthonormalisation matrix in direct space if all parameters are known

        # Variables for log-likelihood statistics
        self.llk_poisson = 0
        self.llk_gaussian = 0
        self.llk_euclidian = 0
        self.llk_poisson_free = 0
        self.llk_gaussian_free = 0
        self.llk_euclidian_free = 0
        self.nb_photons_calc = 0

        if mask is not None:
            self.nb_observed_points = (mask == 0).sum()
        else:
            self.nb_observed_points = (self.iobs > -1e19).sum()

        # Normally free pixels will be initialised later, but if this is a copy they may already be flagged
        self.nb_free_points = ((self.iobs > -1e19) * (self.iobs < 0)).sum()

        self.llk_support = None  # Current value of the support log-likelihood (support regularization)
        self.llk_support_reg_fac = None  # Regularization factor for the support log-likelihood
        if support is not None:
            self.nb_point_support = support.sum()
        else:
            self.nb_point_support = 0

        # Max amplitude reported during support update (after smoothing)
        self._obj_max = 0

        # The timestamp counter record when the cdi or support data was last altered, either in the host or the
        # GPU memory.
        self._timestamp_counter = 1
        self._cl_timestamp_counter = 0
        self._cu_timestamp_counter = 0

        # Record the number of cycles (RAAR, HIO, ER, CF, etc...), which can be used to make some parameters
        # evolve, e.g. for support update
        self.cycle = 0

        # History record
        self.history = History()
        # This is used to report cycle timings
        self.history_llk_t0 = None
        self.history_llk_t0_cycle = None

    def get_x_y_z(self):
        """
        Get 1D arrays of x and y (z if 3d) coordinates, taking into account the pixel size. The arrays are centered
        at (0,0) - i.e. with the origin in the corner for FFT puroposes. x is an horizontal vector and y vertical, 
        and (if 3d) z along third dimension.

        :return: a tuple (x, y) or (x, y, z) of 1D numpy arrays
        """
        if self.iobs.ndim == 2:
            ny, nx = self.iobs.shape
            x, y = np.arange(-nx // 2, nx // 2, dtype=np.float32), \
                   np.arange(-ny // 2, ny // 2, dtype=np.float32)[:, np.newaxis]
            return fftshift(x) * self.pixel_size_object, fftshift(y) * self.pixel_size_object
        else:
            nz, ny, nx = self.iobs.shape
            x, y, z = np.arange(-nx // 2, nx // 2, dtype=np.float32), \
                      np.arange(-ny // 2, ny // 2, dtype=np.float32)[:, np.newaxis], \
                      np.arange(-nz // 2, nz // 2, dtype=np.float32)[:, np.newaxis, np.newaxis]
            return fftshift(x) * self.pixel_size_object, fftshift(y) * self.pixel_size_object, \
                   fftshift(z) * self.pixel_size_object

    def get_x_y(self):
        return self.get_x_y_z()

    def copy(self, copy_history_llk=False, copy_free_pixels=False):
        """
        Creates a copy (without any reference passing) of this object, unless copy_obj is False.

        :param copy_history_llk: if True, the history, cycle number, llk, etc.. are copied to the new object.
        :param copy_free_pixels: if True, copy the distribution of free pixels to the new CDI object. Otherwise the
                                 new CDI object does not have free pixels initialised.
        :return: a copy of the object.
        """
        iobs = self.iobs.copy()
        mask = self.iobs <= -1e19

        if copy_free_pixels is False:
            tmp = np.logical_and(self.iobs > -1e19, self.iobs < 0)
            iobs[tmp] = -(self.iobs[tmp] + 1)

        cdi = CDI(iobs, support=self._support, obj=self._obj, mask=mask,
                  pixel_size_detector=self.pixel_size_detector, wavelength=self.wavelength,
                  detector_distance=self.detector_distance)

        if copy_free_pixels:
            cdi.nb_free_points = self.nb_free_points

        if copy_history_llk:
            cdi.history = deepcopy(self.history)
            cdi.llk_poisson = self.llk_poisson
            cdi.llk_gaussian = self.llk_gaussian
            cdi.llk_euclidian = self.llk_euclidian
            cdi.llk_poisson_free = self.llk_poisson_free
            cdi.llk_gaussian_free = self.llk_gaussian_free
            cdi.llk_euclidian_free = self.llk_euclidian_free
            cdi.nb_photons_calc = self.nb_photons_calc
            cdi.cycle = self.cycle
        return cdi

    def in_object_space(self):
        """

        :return: True if the current obj array is in object space, False otherwise.
        """
        return self._is_in_object_space

    def _from_gpu(self):
        """
        Internal function to get relevant arrays from GPU memory
        :return: Nothing
        """
        if self._timestamp_counter < self._cl_timestamp_counter:
            self._obj = self._cl_obj.get()
            self._support = self._cl_support.get()
            # if has_attr_not_none(self, '_cl_k_psf'):
            #     self._k_psf = self._cl_k_psf.get()
            self._timestamp_counter = self._cl_timestamp_counter
        if self._timestamp_counter < self._cu_timestamp_counter:
            self._obj = self._cu_obj.get()
            self._support = self._cu_support.get()
            # if has_attr_not_none(self, '_cu_k_psf'):
            #     self._k_psf = self._cu_k_psf.get()
            self._timestamp_counter = self._cu_timestamp_counter

    def get_obj(self, shift=False):
        """
        Get the object data array. This will automatically get the latest data, either from GPU or from the host
        memory, depending where the last changes were made.

        :param shift: if True, the data array will be fft-shifted so that the center of the data is in the center
                      of the array, rather than in the corner (the default).
        :return: the 2D or 3D CDI numpy data array
        """
        self._from_gpu()
        if shift:
            return fftshift(self._obj)
        else:
            return self._obj

    def get_support(self, shift=False):
        """
        Get the support array. This will automatically get the latest data, either from GPU or from the host
        memory, depending where the last changes were made.

        :param shift: if True, the data array will be fft-shifted so that the center of the data is in the center
                      of the array, rather than in the corner (the default).
        :return: the 2D or 3D CDI numpy data array
        """
        self._from_gpu()

        if shift:
            return fftshift(self._support)
        else:
            return self._support

    def get_iobs(self, shift=False):
        """
        Get the observed intensity data array.

        :param shift: if True, the data array will be fft-shifted so that the center of the data is in the center
                      of the array, rather than in the corner (the default).
        :return: the 2D or 3D CDI numpy data array
        """
        if shift:
            return fftshift(self.iobs)
        else:
            return self.iobs

    def set_obj(self, obj, shift=False):
        """
        Set the object data array. Assumed to be in object (not Fourier) space

        :param obj: the 2D or 3D CDI numpy data array (complex64 numpy array)
        :param shift: if True, the data array will be fft-shifted so that the center of the stored data is
                      in the corner of the array. [default: the array is already shifted]
        :return: nothing
        """
        if shift:
            self._obj = fftshift(obj).astype(np.complex64)
        else:
            self._obj = obj.astype(np.complex64)
        if self._timestamp_counter <= self._cl_timestamp_counter:
            self._timestamp_counter = self._cl_timestamp_counter + 1
        if self._timestamp_counter <= self._cu_timestamp_counter:
            self._timestamp_counter = self._cu_timestamp_counter + 1
        self._is_in_object_space = True

    def set_support(self, support, shift=False):
        """
        Set the support data array.

        :param obj: the 2D or 3D CDI numpy data array (complex64 numpy array)
        :param shift: if True, the data array will be fft-shifted so that the center of the stored data is
                      in the corner of the array. [default: the array is already shifted]
        :return: nothing
        """
        if shift:
            self._support = fftshift(support).astype(np.int8)
        else:
            self._support = support.astype(np.int8)
        if self._timestamp_counter <= self._cl_timestamp_counter:
            self._timestamp_counter = self._cl_timestamp_counter + 1
        if self._timestamp_counter <= self._cu_timestamp_counter:
            self._timestamp_counter = self._cu_timestamp_counter + 1
        self.nb_point_support = self._support.sum()

    def set_iobs(self, iobs, shift=False):
        """
        Set the observed intensity data array.

        :param iobs: the 2D or 3D CDI numpy data array (float32 numpy array)
        :param shift: if True, the data array will be fft-shifted so that the center of the stored data is
                      in the corner of the array. [default: the array is already shifted]
        :return: nothing
        """
        if shift:
            self.iobs = fftshift(iobs).astype(np.float32)
        else:
            self.iobs = iobs.astype(np.float32)
        self.iobs_sum = self.iobs[self.iobs > 0].sum()

        if self._timestamp_counter <= self._cl_timestamp_counter:
            self._timestamp_counter = self._cl_timestamp_counter + 1
        if self._timestamp_counter <= self._cu_timestamp_counter:
            self._timestamp_counter = self._cu_timestamp_counter + 1

    def set_mask(self, mask, shift=False):
        """
        Set the mask data array. Note that since the mask is stored by setting observed intensities of masked
        pixels to negative values, it is not possible to un-mask pixels.

        :param obj: the 2D or 3D CDI mask array
        :param shift: if True, the data array will be fft-shifted so that the center of the stored data is
                      in the corner of the array. [default: the array is already shifted]
        :return: nothing
        """
        if mask is None:
            return
        if shift:
            mask = fftshift(mask).astype(np.int8)
        iobs = self.get_iobs()
        iobs[mask > 0] = -1.05e38
        self.set_iobs(iobs)

    def init_free_pixels(self, ratio=5e-2, island_radius=3, exclude_zone_center=0.05, verbose=False):
        """
        Random selection of 'free' pixels in the observed intensities, to be used as unbiased indicator.
        :param ratio: (approximate) relative number of pixels to be included in the free mask
        :param island_radius: free island radius, to avoid pixel correlation due to finit object size
        :param exclude_zone_center: the relative radius of the zone to be excluded near the center
        :param verbose: if True, be verbose
        :return: nothing. Free pixel values are modified as iobs_free = -iobs - 1
        """
        # Clear previous free mask
        tmp = np.logical_and(self.iobs > -1e19, self.iobs < 0)
        self.iobs[tmp] = -(self.iobs[tmp] + 1)

        if True:
            if self.iobs.ndim == 3:
                nb = int(self.iobs.size * ratio / (4 / 3 * 3.14 * island_radius ** 3))
                nz, ny, nx = self.iobs.shape
                iz = np.random.randint(island_radius, nz - island_radius, nb)
                iy = np.random.randint(island_radius, ny - island_radius, nb)
                ix = np.random.randint(island_radius, nx - island_radius, nb)
                m = np.zeros_like(self.iobs, dtype=np.bool)
                # There may be faster ways to do this. But nb is small, so a sparse approach could be faster
                for dx in range(-island_radius, island_radius + 1):
                    for dy in range(-island_radius, island_radius + 1):
                        for dz in range(-island_radius, island_radius + 1):
                            if (dx ** 2 + dy ** 2 + dz ** 2) <= island_radius ** 2:
                                m[iz + dz, iy + dy, ix + dx] = True
            else:
                if island_radius == 0:
                    nb = int(self.iobs.size * ratio)
                else:
                    nb = int(self.iobs.size * ratio / (3.14 * island_radius ** 2))
                ny, nx = self.iobs.shape
                iy = np.random.randint(island_radius, ny - island_radius, nb)
                ix = np.random.randint(island_radius, nx - island_radius, nb)
                m = np.zeros_like(self.iobs, dtype=np.bool)
                # There may be faster ways to do this. But nb is small, so a sparse approach could be faster
                for dx in range(-island_radius, island_radius + 1):
                    for dy in range(-island_radius, island_radius + 1):
                        if (dx ** 2 + dy ** 2) <= island_radius ** 2:
                            m[iy + dy, ix + dx] = True
                            # print(dx, dy, m.sum())

        else:
            # Init kernel
            if self.iobs.ndim == 2:
                tmp = np.arange(-island_radius, island_radius + 1)
                y, x = np.meshgrid(tmp, tmp, indexing='ij')
                k = (np.sqrt(x ** 2 + y ** 2) <= island_radius).astype(np.float32)
            else:
                tmp = np.arange(-island_radius, island_radius + 1)
                z, y, x = np.meshgrid(tmp, tmp, tmp, indexing='ij')
                k = (np.sqrt(x ** 2 + y ** 2 + z ** 2) <= island_radius).astype(np.float32)

            nk = k.sum()
            m = (np.random.uniform(0, 1, self.iobs.shape) < (ratio / nk)).astype(np.float32)
            m = (convolve(m, k, 'same') > 0.5).astype(np.int8)

        # Exclude masked pixels
        m[self.iobs <= -1e19] = False

        # Exclude zone-center
        if m.ndim == 2:
            ny, nx = m.shape
            y, x = np.meshgrid(fftfreq(ny), fftfreq(nx), indexing='ij')
            m[np.sqrt(x ** 2 + y ** 2) < exclude_zone_center] = False
        else:
            nz, ny, nx = m.shape
            z, y, x = np.meshgrid(fftfreq(nz), fftfreq(ny), fftfreq(nx), indexing='ij')
            m[np.sqrt(x ** 2 + y ** 2 + z ** 2) < exclude_zone_center] = False
        m = m > 0

        # Flag free pixels
        self.iobs[m] = -self.iobs[m] - 1
        # print("finished", timeit.default_timer() - t0)

        n = m.sum()
        self.nb_free_points = n
        if verbose:
            print('Initialized free mask with %d pixels (%6.3f%%)' % (n, 100 * n / m.size))
        self.set_iobs(self.iobs)  # This will set the timestamp

    def set_free_pixel_mask(self, m, verbose=False, shift=False):
        """
        Set the free pixel mask. Assumes the free pixel mask is correct (excluding center, bad pixels)
        :param m: the boolean mask (1=masked, 0 otherwise)
        :param shift: if True, the data array will be fft-shifted so that the center of the stored data is
        in the corner of the array. [default: the array is already shifted]
        :return: nothing
        """
        if shift:
            m = fftshift(m)
        # Clear previous free mask
        tmp = np.logical_and(self.iobs > -1e38, self.iobs < 0)
        self.iobs[tmp] = -(self.iobs[tmp] + 1)

        # Flag free pixels
        self.iobs[m] = -self.iobs[m] - 1
        # print("finished", timeit.default_timer() - t0)

        n = m.sum()
        self.nb_free_points = n
        if verbose:
            print('Set free mask with %d pixels (%6.3f%%)' % (n, 100 * n / m.size))
        self.set_iobs(self.iobs)  # This will set the timestamp

    def get_free_pixel_mask(self):
        return np.logical_and(self.iobs > -1e19, self.iobs < -0.5).astype(np.bool)

    def save_data_cxi(self, filename, sample_name=None, experiment_id=None, instrument=None, process_parameters=None):
        """
        Save the diffraction data (observed intensity, mask) to an HDF% CXI file.
        :param filename: the file name to save the data to
        :param sample_name: optional, the sample name
        :param experiment_id: the string identifying the experiment, e.g.: 'HC1234: Siemens star calibration tests'
        :param instrument: the string identifying the instrument, e.g.: 'ESRF id10'
        :param process_parameters: a dictionary of parameters which will be saved as a NXcollection
        :return: Nothing. a CXI file is created
        """
        # Remove mask
        iobs = self.iobs.copy()
        mask = self.iobs <= -1e19
        iobs[mask] = 0
        # Remove free mask
        tmp = iobs < 0
        iobs[tmp] = -(iobs[tmp] + 1)

        save_cdi_data_cxi(filename, iobs=iobs, wavelength=self.wavelength,
                          detector_distance=self.detector_distance,
                          pixel_size_detector=self.pixel_size_detector, mask=mask, sample_name=sample_name,
                          experiment_id=experiment_id, instrument=instrument, iobs_is_fft_shifted=True,
                          process_parameters=process_parameters)

    def save_obj_cxi(self, filename, sample_name=None, experiment_id=None, instrument=None, note=None, crop=0,
                     save_psf=False, process_notes=None, process_parameters=None, append=False, **kwargs):
        """
        Save the result of the optimisation (object, support) to an HDF5 CXI file.
        :param filename: the file name to save the data to
        :param sample_name: optional, the sample name
        :param experiment_id: the string identifying the experiment, e.g.: 'HC1234: Siemens star calibration tests'
        :param instrument: the string identifying the instrument, e.g.: 'ESRF id10'
        :param note: a string with a text note giving some additional information about the data, a publication...
        :param crop: integer, if >0, the object will be cropped arount the support, plus a margin of 'crop' pixels.
        :param save_psf: if True, also save the psf (if present)
        :param process_notes: a dictionary which will be saved in '/entry_N/process_1'. A dictionary entry
        can also be a 'note' as keyword and a dictionary as value - all key/values will then be saved
        as separate notes. Example: process={'program': 'PyNX', 'note':{'llk':1.056, 'nb_photons': 1e8}}
        :param process_parameters: a dictionary of parameters which will be saved as a NXcollection
        :param append: if True and the file already exists, the result will be saved in a new entry.
        :return: Nothing. a CXI file is created
        """

        if 'process' in kwargs:
            warnings.warn("CDI.save_obj_cxi(): parameter 'process' is deprecated,"
                          "use process_notes and process_parameters instead.")
            if process_notes is None:
                process_notes = kwargs['process']
            else:
                warnings.warn("CDI.save_obj_cxi(): parameter 'process' ignored as 'process_notes' is also given")

        if append:
            f = h5py.File(filename, "a")
            if "cxi_version" not in f:
                f.create_dataset("cxi_version", data=150)
            i = 1
            while True:
                if 'entry_%d' % i not in f:
                    break
                i += 1
            entry = f.create_group("/entry_%d" % i)
            entry_path = "/entry_%d" % i
            f.attrs['default'] = "/entry_%d" % i
            if "/entry_last" in entry:
                del entry["/entry_last"]
            entry["/entry_last"] = h5py.SoftLink("/entry_%d" % i)
        else:
            f = h5py.File(filename, "w")
            f.create_dataset("cxi_version", data=150)
            entry = f.create_group("/entry_1")
            entry_path = "/entry_1"
            f.attrs['default'] = 'entry_1'
            entry["/entry_last"] = h5py.SoftLink("/entry_1")

        f.attrs['creator'] = 'PyNX'
        # f.attrs['NeXus_version'] = '2018.5'  # Should only be used when the NeXus API has written the file
        f.attrs['HDF5_Version'] = h5py.version.hdf5_version
        f.attrs['h5py_version'] = h5py.version.version

        entry.attrs['NX_class'] = 'NXentry'
        # entry.create_dataset('title', data='1-D scan of I00 v. mr')
        entry.attrs['default'] = 'data_1'
        entry.create_dataset("program_name", data="PyNX %s" % _pynx_version)
        entry.create_dataset("start_time", data=time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime(time.time())))
        if experiment_id is not None:
            entry.create_dataset("experiment_id", data=experiment_id)

        if note is not None:
            note_1 = entry.create_group("note_1")
            note_1.create_dataset("data", data=note)
            note_1.create_dataset("type", data="text/plain")

        if sample_name is not None:
            sample_1 = entry.create_group("sample_1")
            sample_1.attrs['NX_class'] = 'NXsample'
            sample_1.create_dataset("name", data=sample_name)

        obj = self.get_obj(shift=True)
        sup = self.get_support(shift=True)
        if crop > 0:
            # crop around support
            if self.iobs.ndim == 3:
                l0 = np.nonzero(sup.sum(axis=(1, 2)))[0].take([0, -1]) + np.array([-crop, crop])
                if l0[0] < 0: l0[0] = 0
                if l0[1] >= sup.shape[0]: l0[1] = -1

                l1 = np.nonzero(sup.sum(axis=(0, 2)))[0].take([0, -1]) + np.array([-crop, crop])
                if l1[0] < 0: l1[0] = 0
                if l1[1] >= sup.shape[1]: l1[1] = -1

                l2 = np.nonzero(sup.sum(axis=(0, 1)))[0].take([0, -1]) + np.array([-crop, crop])
                if l2[0] < 0: l2[0] = 0
                if l2[1] >= sup.shape[2]: l2[1] = -1
                obj = obj[l0[0]:l0[1], l1[0]:l1[1], l2[0]:l2[1]]
                sup = sup[l0[0]:l0[1], l1[0]:l1[1], l2[0]:l2[1]]
            else:
                l0 = np.nonzero(sup.sum(axis=1))[0].take([0, -1]) + np.array([-crop, crop])
                if l0[0] < 0: l0[0] = 0
                if l0[1] >= sup.shape[0]: l0[1] = -1

                l1 = np.nonzero(sup.sum(axis=0))[0].take([0, -1]) + np.array([-crop, crop])
                if l1[0] < 0: l1[0] = 0
                if l1[1] >= sup.shape[1]: l1[1] = -1

                obj = obj[l0[0]:l0[1], l1[0]:l1[1]]
                sup = sup[l0[0]:l0[1], l1[0]:l1[1]]

        image_1 = entry.create_group("image_1")
        image_1.create_dataset("data", data=obj, chunks=True, shuffle=True, compression="gzip")
        image_1.attrs['NX_class'] = 'NXdata'  # Is image_1 a well-formed NXdata or not ?
        image_1.attrs['signal'] = 'data'
        if obj.ndim == 3:
            image_1.attrs['interpretation'] = 'scaler'  # NeXus specs don't make sense
        else:
            image_1.attrs['interpretation'] = 'image'
        image_1.create_dataset("mask", data=sup, chunks=True, shuffle=True, compression="gzip")
        image_1["support"] = h5py.SoftLink('%s/image_1/mask' % entry_path)
        image_1.create_dataset("data_type", data="electron density")  # CXI, not NeXus
        image_1.create_dataset("data_space", data="real")  # CXI, not NeXus ?
        if self.pixel_size_object is not None:
            s = self.pixel_size_object * np.array(obj.shape)
            image_1.create_dataset("image_size", data=s)
            if False:  # TODO: x, y (z) axes description (scale), when accessible
                # See proposeition: https://www.nexusformat.org/2014_axes_and_uncertainties.html
                #
                # The following is only true if pixel size is the same along X and Y. Not always the case !
                ny, nx = obj.shape[-2:]
                x = (np.arange(nx) - nx // 2) * self.pixel_size_object
                y = (np.arange(ny) - ny // 2) * self.pixel_size_object
                image_1.create_dataset("x", data=x)
                image_1.create_dataset("y", data=y)
                image_1.attrs['axes'] = ". y x"  # How does this work ? "x y" or ['x', 'y'] ????
                # image_1.attrs['x_indices'] = [-1, ]
                # image_1.attrs['y_indices'] = [-2, ]

        instrument_1 = image_1.create_group("instrument_1")
        instrument_1.attrs['NX_class'] = 'NXinstrument'
        if instrument is not None:
            instrument_1.create_dataset("name", data=instrument)

        if self.wavelength is not None:
            nrj = 12.3984 / (self.wavelength * 1e10) * 1.60218e-16
            source_1 = instrument_1.create_group("source_1")
            source_1.attrs['NX_class'] = 'NXsource'
            source_1.attrs['note'] = 'Incident photon energy (instead of source energy), for CXI compatibility'
            source_1.create_dataset("energy", data=nrj)
            source_1["energy"].attrs['units'] = 'J'

            beam_1 = instrument_1.create_group("beam_1")
            beam_1.attrs['NX_class'] = 'NXbeam'
            beam_1.create_dataset("incident_energy", data=nrj)
            beam_1["incident_energy"].attrs['units'] = 'J'
            beam_1.create_dataset("incident_wavelength", data=self.wavelength)
            beam_1["incident_wavelength"].attrs['units'] = 'm'

        detector_1 = instrument_1.create_group("detector_1")
        detector_1.attrs['NX_class'] = 'NXdetector'
        if self.detector_distance is not None:
            detector_1.create_dataset("distance", data=self.detector_distance)
            detector_1["distance"].attrs['units'] = 'm'

        if self.pixel_size_detector is not None:
            detector_1.create_dataset("x_pixel_size", data=self.pixel_size_detector)
            detector_1["x_pixel_size"].attrs['units'] = 'm'
            detector_1.create_dataset("y_pixel_size", data=self.pixel_size_detector)
            detector_1["y_pixel_size"].attrs['units'] = 'm'

        if self._k_psf is not None and save_psf:
            # Likely this is due to partial coherence and not the detector, but it is still analysed as a PSF
            # TODO: only save center of psf
            detector_1.create_dataset("point_spread_function", data=self._k_psf, chunks=True, shuffle=True,
                                      compression="gzip")

        # Add shortcut to the main data
        data_1 = entry.create_group("data_1")
        data_1["data"] = h5py.SoftLink('%s/image_1/data' % entry_path)
        data_1.attrs['NX_class'] = 'NXdata'
        data_1.attrs['signal'] = 'data'
        if obj.ndim == 3:
            data_1.attrs['interpretation'] = 'scaler'  # NeXus specs don't make sense'
        else:
            data_1.attrs['interpretation'] = 'image'
        # TODO: x, y (z) axes description (scale), when accessible

        command = ""
        for arg in sys.argv:
            command += arg + " "
        process_1 = image_1.create_group("process_1")
        process_1.attrs['NX_class'] = 'NXprocess'
        process_1.create_dataset("program", data='PyNX')  # NeXus spec
        process_1.create_dataset("version", data="%s" % _pynx_version)  # NeXus spec
        process_1.create_dataset("command", data=command)  # CXI spec

        if process_notes is not None:  # Notes
            for k, v in process_notes.items():
                if isinstance(v, str):
                    if k not in process_1:
                        process_1.create_dataset(k, data=v)
                elif isinstance(v, dict) and k == 'note':
                    # Save this as notes:
                    for kk, vv in v.items():
                        i = 1
                        while True:
                            note_s = 'note_%d' % i
                            if note_s not in process_1:
                                break
                            i += 1
                        note = process_1.create_group(note_s)
                        note.attrs['NX_class'] = 'NXnote'
                        note.create_dataset("description", data=kk)
                        # TODO: also save values as floating-point if appropriate
                        note.create_dataset("data", data=str(vv))
                        note.create_dataset("type", data="text/plain")

        # Configuration & results of process: custom ESRF data policy
        # see https://gitlab.esrf.fr/sole/data_policy/blob/master/ESRF_NeXusImplementation.rst
        config = process_1.create_group("configuration")
        config.attrs['NX_class'] = 'NXcollection'
        if process_parameters is not None:
            for k, v in process_parameters.items():
                if k == 'free_pixel_mask':
                    k = 'free_pixel_mask_input'
                if v is not None:
                    if type(v) is dict:
                        # This can happen if complex configuration is passed on
                        if len(v):
                            kd = config.create_group(k)
                            kd.attrs['NX_class'] = 'NXcollection'
                            for kk, vv in v.items():
                                kd.create_dataset(kk, data=vv)
                    else:
                        config.create_dataset(k, data=v)
        if self.iobs is not None and 'iobs_shape' not in config:
            config.create_dataset('iobs_shape', data=self.iobs.shape)

        # Save the free pixel mask so that it can be re-used
        mask_free_pixel = self.get_free_pixel_mask()
        if mask_free_pixel.sum():
            config.create_dataset('free_pixel_mask', data=fftshift(mask_free_pixel.astype(np.bool)), chunks=True,
                                  shuffle=True, compression="gzip")
            config['free_pixel_mask'].attrs['note'] = "Mask of pixels used for free log-likelihood calculation"
            if mask_free_pixel.ndim == 3:
                config['free_pixel_mask'].attrs['interpretation'] = 'scaler'
            else:
                config['free_pixel_mask'].attrs['interpretation'] = 'image'

        results = process_1.create_group("results")
        results.attrs['NX_class'] = 'NXcollection'
        llk = self.get_llk(normalized=True)
        results.create_dataset('llk_poisson', data=llk[0])
        results.create_dataset('llk_gaussian', data=llk[1])
        results.create_dataset('llk_euclidian', data=llk[2])
        results.create_dataset('free_llk_poisson', data=llk[3])
        results.create_dataset('free_llk_gaussian', data=llk[4])
        results.create_dataset('free_llk_euclidian', data=llk[5])
        results.create_dataset('nb_point_support', data=self.nb_point_support)
        results.create_dataset('cycle_history', data=self.history.as_numpy_record_array())
        for k in self.history.keys():
            results.create_dataset('cycle_history_%s' % k, data=self.history[k].as_numpy_record_array())

        f.close()

    def get_llkn(self):
        """
        Get the poisson normalized log-likelihood, which should converge to 1 for a statistically ideal fit
        to a Poisson-noise  dataset.

        :return: the normalized log-likelihood, for Poisson noise.
        """
        warnings.warn("cdi.get_llkn is deprecated. Use cdi.get_llk instead", DeprecationWarning)
        return self.get_llk(noise='poisson', normalized=True)

    def get_llk(self, noise=None, normalized=True):
        """
        Get the normalized log-likelihoods, which should converge to 1 for a statistically ideal fit.

        :param noise: either 'gaussian', 'poisson' or 'euclidian', will return the corresponding log-likelihood.
        :param normalized: if True, will return normalized values so that the llk from a statistically ideal model
                           should converge to 1
        :return: the log-likelihood, or if noise=None, a tuple of the three (poisson, gaussian, euclidian)
                 log-likelihoods.
        """
        n = 1
        nfree = 1
        if normalized:
            n = 1 / max((self.nb_observed_points - self.nb_point_support - self.nb_free_points), 1e-10)
            if self.nb_free_points > 0:
                nfree = 1 / self.nb_free_points
            else:
                nfree = 0

        if noise is None:
            return self.llk_poisson * n, self.llk_gaussian * n, self.llk_euclidian * n, \
                   self.llk_poisson_free * nfree, self.llk_gaussian_free * nfree, self.llk_euclidian_free * nfree
        elif 'poiss' in str(noise).lower():
            return self.llk_poisson * n
        elif 'gauss' in str(noise).lower():
            return self.llk_gaussian * n
        elif 'eucl' in str(noise).lower():
            return self.llk_euclidian * n

    def reset_history(self):
        """
        Reset history, and set current cycle to zero
        :return: nothing
        """
        self.history = History()
        self.cycle = 0

    def update_history(self, mode='llk', verbose=False, **kwargs):
        """
        Update the history record.
        :param mode: either 'llk' (will record new log-likelihood, nb_photons_calc, average value..),
        or 'algorithm' (will only update the algorithm) or 'support'.
        :param verbose: if True, print some info about current process
        :param kwargs: other parameters to be recorded, e.g. support_threshold=xx, dt=
        :return: nothing
        """
        if mode == 'llk':
            llk = self.get_llk()
            algo = ''
            if self.history_llk_t0_cycle is not None:
                dt = (timeit.default_timer() - self.history_llk_t0) / (self.cycle - self.history_llk_t0_cycle)
                kwargs['dt'] = dt
            else:
                dt = 0
            self.history_llk_t0 = timeit.default_timer()
            self.history_llk_t0_cycle = self.cycle
            if 'algorithm' in kwargs:
                algo = kwargs['algorithm']
            if verbose:
                print("%4s #%3d LLK= %8.3f[%8.3f](p) %8.3f[%8.3f](g) %8.3f[%8.3f](e), nb photons=%e, "
                      "support:nb=%6d (%6.3f%%) average=%10.2f max=%10.2f, dt/cycle=%6.4fs" % (
                          algo, self.cycle, llk[0], llk[3], llk[1], llk[4], llk[2], llk[5], self.nb_photons_calc,
                          self.nb_point_support, self.nb_point_support / self._obj.size * 100,
                          np.sqrt(self.nb_photons_calc / self.nb_point_support), self._obj_max, dt))
            self.history.insert(self.cycle, llk_poisson=llk[0], llk_poisson_free=llk[3],
                                llk_gaussian=llk[1], llk_gaussian_free=llk[4], llk_euclidian=llk[2],
                                llk_euclidian_free=llk[2], nb_photons_calc=self.nb_photons_calc,
                                obj_average=np.sqrt(self.nb_photons_calc / self.nb_point_support), **kwargs)
        elif mode == 'support':
            self.history.insert(self.cycle, support_size=self.nb_point_support, obj_max=self._obj_max,
                                obj_average=np.sqrt(self.nb_photons_calc / self.nb_point_support))
        elif 'algo' in mode:
            if 'algorithm' in kwargs:
                self.history.insert(self.cycle, algorithm=kwargs['algorithm'])

    def __rmul__(self, x):
        """
        Multiply object (by a scalar).

        This is a placeholder for a function which will be replaced when importing either CUDA or OpenCL operators.
        If called before being replaced, will raise an error

        :param x: the scalar by which the wavefront will be multiplied
        :return:
        """
        if np.isscalar(x):
            raise OperatorException(
                "ERROR: attempted Op1 * Op2, with Op1=%s, Op2=%s. Did you import operators ?" % (str(x), str(self)))
        else:
            raise OperatorException("ERROR: attempted Op1 * Op2, with Op1=%s, Op2=%s." % (str(x), str(self)))

    def __mul__(self, x):
        """
        Multiply object (by a scalar).

        This is a placeholder for a function which will be replaced when importing either CUDA or OpenCL operators.
        If called before being replaced, will raise an error

        :param x: the scalar by which the wavefront will be multiplied
        :return:
        """
        if np.isscalar(x):
            raise OperatorException(
                "ERROR: attempted Op1 * Op2, with Op1=%s, Op2=%s. Did you import operators ?" % (str(self), str(x)))
        else:
            raise OperatorException("ERROR: attempted Op1 * Op2, with Op1=%s, Op2=%s." % (str(self), str(x)))

    def __str__(self):
        return "CDI"


def save_cdi_data_cxi(filename, iobs, wavelength=None, detector_distance=None, pixel_size_detector=None, mask=None,
                      sample_name=None, experiment_id=None, instrument=None, note=None, iobs_is_fft_shifted=False,
                      process_parameters=None):
    """
    Save the diffraction data (observed intensity, mask) to an HDF5 CXI file, NeXus-compatible.
    :param filename: the file name to save the data to
    :param iobs: the observed intensity
    :param wavelength: the wavelength of the experiment (in meters)
    :param detector_distance: the detector distance (in meters)
    :param pixel_size_detector: the pixel size of the detector (in meters)
    :param mask: the mask indicating valid (=0) and bad pixels (>0)
    :param sample_name: optional, the sample name
    :param experiment_id: the string identifying the experiment, e.g.: 'HC1234: Siemens star calibration tests'
    :param instrument: the string identifying the instrument, e.g.: 'ESRF id10'
    :param iobs_is_fft_shifted: if true, input iobs (and mask if any) have their origin in (0,0[,0]) and will be shifted
    back to centered-versions before being saved.
    :param process_parameters: a dictionary of parameters which will be saved as a NXcollection
    :return: Nothing. a CXI file is created
    """
    f = h5py.File(filename, "w")
    f.attrs['file_name'] = filename
    f.attrs['file_time'] = time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime(time.time()))
    if instrument is not None:
        f.attrs['instrument'] = instrument
    f.attrs['creator'] = 'PyNX'
    # f.attrs['NeXus_version'] = '2018.5'  # Should only be used when the NeXus API has written the file
    f.attrs['HDF5_Version'] = h5py.version.hdf5_version
    f.attrs['h5py_version'] = h5py.version.version
    f.attrs['default'] = 'entry_1'
    f.create_dataset("cxi_version", data=140)

    entry_1 = f.create_group("entry_1")
    entry_1.create_dataset("program_name", data="PyNX %s" % _pynx_version)
    entry_1.create_dataset("start_time", data=time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime(time.time())))
    entry_1.attrs['NX_class'] = 'NXentry'
    # entry_1.create_dataset('title', data='1-D scan of I00 v. mr')
    entry_1.attrs['default'] = 'data_1'

    if experiment_id is not None:
        entry_1.create_dataset("experiment_id", data=experiment_id)

    if note is not None:
        note_1 = entry_1.create_group("note_1")
        note_1.create_dataset("data", data=note)
        note_1.create_dataset("type", data="text/plain")

    if sample_name is not None:
        sample_1 = entry_1.create_group("sample_1")
        sample_1.create_dataset("name", data=sample_name)

    instrument_1 = entry_1.create_group("instrument_1")
    instrument_1.attrs['NX_class'] = 'NXinstrument'
    if instrument is not None:
        instrument_1.create_dataset("name", data=instrument)

    if wavelength is not None:
        nrj = 12.3984 / (wavelength * 1e10) * 1.60218e-16
        source_1 = instrument_1.create_group("source_1")
        source_1.attrs['NX_class'] = 'NXsource'
        source_1.attrs['note'] = 'Incident photon energy (instead of source energy), for CXI compatibility'
        source_1.create_dataset("energy", data=nrj)
        source_1["energy"].attrs['units'] = 'J'

        beam_1 = instrument_1.create_group("beam_1")
        beam_1.attrs['NX_class'] = 'NXbeam'
        beam_1.create_dataset("incident_energy", data=nrj)
        beam_1["incident_energy"].attrs['units'] = 'J'
        beam_1.create_dataset("incident_wavelength", data=wavelength)
        beam_1["incident_wavelength"].attrs['units'] = 'm'

    detector_1 = instrument_1.create_group("detector_1")
    detector_1.attrs['NX_class'] = 'NX_detector'

    if detector_distance is not None:
        detector_1.create_dataset("distance", data=detector_distance)
        detector_1["distance"].attrs['units'] = 'm'
    if pixel_size_detector is not None:
        detector_1.create_dataset("x_pixel_size", data=pixel_size_detector)
        detector_1["x_pixel_size"].attrs['units'] = 'm'
        detector_1.create_dataset("y_pixel_size", data=pixel_size_detector)
        detector_1["y_pixel_size"].attrs['units'] = 'm'
    if iobs_is_fft_shifted:
        detector_1.create_dataset("data", data=fftshift(iobs), chunks=True, shuffle=True,
                                  compression="gzip")
    else:
        detector_1.create_dataset("data", data=iobs, chunks=True, shuffle=True,
                                  compression="gzip")

    if mask is not None:
        if mask.sum() != 0:
            if iobs_is_fft_shifted:
                detector_1.create_dataset("mask", data=fftshift(mask), chunks=True, shuffle=True,
                                          compression="gzip")
            else:
                detector_1.create_dataset("mask", data=mask, chunks=True, shuffle=True, compression="gzip")
    if False:
        # Basis vector - this is the default CXI convention, so could be skipped
        # This corresponds to a 'top, left' origin convention
        basis_vectors = np.zeros((2, 3), dtype=np.float32)
        basis_vectors[0, 1] = -pixel_size_detector
        basis_vectors[1, 0] = -pixel_size_detector
        detector_1.create_dataset("basis_vectors", data=basis_vectors)

    data_1 = entry_1.create_group("data_1")
    data_1.attrs['NX_class'] = 'NXdata'
    data_1["data"] = h5py.SoftLink('/entry_1/instrument_1/detector_1/data')
    data_1.attrs['signal'] = 'data'
    if iobs.ndim == 3:
        data_1.attrs['interpretation'] = 'scaler'  # NeXus specs don't make sense
    else:
        data_1.attrs['interpretation'] = 'image'

    # Remember how import was done
    command = ""
    for arg in sys.argv:
        command += arg + " "
    process_1 = data_1.create_group("process_1")
    process_1.attrs['NX_class'] = 'NXprocess'
    process_1.create_dataset("program", data='PyNX')  # NeXus spec
    process_1.create_dataset("version", data="%s" % _pynx_version)  # NeXus spec
    process_1.create_dataset("command", data=command)  # CXI spec

    # Configuration & results of process: custom ESRF data policy
    # see https://gitlab.esrf.fr/sole/data_policy/blob/master/ESRF_NeXusImplementation.rst
    if process_parameters is not None:
        config = process_1.create_group("configuration")
        config.attrs['NX_class'] = 'NXcollection'
        for k, v in process_parameters.items():
            if v is not None:
                if type(v) is dict:
                    # This can happen if complex configuration is passed on
                    if len(v):
                        kd = config.create_group(k)
                        kd.attrs['NX_class'] = 'NXcollection'
                        for kk, vv in v.items():
                            kd.create_dataset(kk, data=vv)
                else:
                    config.create_dataset(k, data=v)

    f.close()


class OperatorCDI(Operator):
    """
    Base class for an operator on CDI objects, not requiring a processing unit.
    """

    def timestamp_increment(self, cdi):
        cdi._timestamp_counter += 1
