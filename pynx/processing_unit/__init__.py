# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import os
import warnings
from .cuda_device import has_cuda
from .opencl_device import has_opencl
import numpy as np
from scipy.fftpack import fftn, ifftn, fftshift

try:
    from mpi4py import MPI

    mpi = MPI.COMM_WORLD
    if mpi.Get_size() == 1:
        mpi = None
except ImportError:
    mpi = None


class ProcessingUnitException(Exception):
    pass


# Exclude languages based on PYNX_PU environment variable
if 'PYNX_PU' in os.environ:
    pynx_py_env = os.environ['PYNX_PU'].lower().split('.')
    if 'cuda' in pynx_py_env[0]:
        if has_cuda is False:
            raise ProcessingUnitException('PyNX processing unit: CUDA language is selected from the PYNX_PU '
                                          'environment variable, but CUDA is not available !')
        has_opencl = False
        print("PyNX processing unit: select CUDA language from PYNX_PU environment variable")
    elif 'opencl' in pynx_py_env[0]:
        if has_opencl is False:
            raise ProcessingUnitException('PyNX processing unit: OpenCL language is selected from the PYNX_PU '
                                          'environment variable, but OpenCL is not available !')
        has_cuda = False
        print("PyNX processing unit: select OpenCL language from PYNX_PU environment variable")
    elif 'cpu' in pynx_py_env[0]:
        has_opencl = False
        has_cuda = False
        print("PyNX processing unit: select CPU from PYNX_PU environment variable - OpenCL and CUDA will be ignored")


class ProcessingUnit(object):

    def __init__(self):
        # Generic parameters
        self.pu_language = None  # 'cpu', 'opencl' or 'cuda' - None if un-initialised
        self.max_prime_fft = None
        # OpenCL device
        self.cl_device = None
        # CUDA device
        self.cu_device = None

        # Default fft size for benchmarking
        self.benchmark_fft_size = (16, 400, 400)
        # if True, will perform a batch FFT (e.g. 16 2D FFT of size 256x256 with the default parameters)
        self.benchmark_fft_batch = True

    def enable_profiling(self, profiling=True):
        """
        Enable profiling (for OpenCL only)
        :param profiling: True to enable (the default)
        :return:
        """
        print('Profiling can only be enabled when using OpenCL')

    def set_benchmark_fft_parameters(self, fft_size, batch):
        """
        Set FFT size and axis for benchmarking processing units. fft_size=(16, 400, 400) and batch=True is the
        default and will perform a batch 2D FFT on 16 2D arrays.
        :param benchmark_fft_size: FFT size (3D or stacked 2D) for benchmarking.
        :param batch: if True, will perform a stacked 2D FFT. Otherwise a 3D FFT will be performed
        :return: nothing
        """
        self.benchmark_fft_size = fft_size
        self.benchmark_fft_batch = batch

    def set_device(self, d=None, verbose=True, test_fft=True):
        """
        Set the computing device to be used. A quick FFT test is also performed.

        :param d: either a pyopencl.Device or pycuda.Driver.Device. Otherwise the language is set to 'cpu'
        :param verbose: if True, will print information about the used device
        :param test_fft: if True (the default), the FFT speed is evaluated.
        :return: True if the FFT calculation was correctly achieved.
        """
        if has_opencl:
            from . import opencl_device
            from .opencl_utils import gpyfft_max_prime
            if self.benchmark_fft_batch:
                fft_axis = (-2, -1)
            else:
                fft_axis = None
            if type(d) is opencl_device.cl.Device:
                if test_fft:
                    opencl_device.cl_device_fft_speed(d, self.benchmark_fft_size, fft_axis, True)
                self.cl_device = d
                self.pu_language = 'opencl'
                self.max_prime_fft = gpyfft_max_prime(d)
                if verbose:
                    print("Using OpenCL GPU: %s" % (self.cl_device.name))
                return True
        if has_cuda:
            from . import cuda_device
            if type(d) is cuda_device.cu_drv.Device:
                self.cu_device = d
                self.pu_language = 'cuda'
                self.max_prime_fft = 7
                if verbose:
                    print("Using CUDA GPU: %s" % (self.cu_device.name()))
                return True
        if verbose:
            print("Using CPU")
        self.pu_language = 'cpu'
        self.max_prime_fft = 2 ** 16 + 1

    def use_opencl(self, gpu_name=None, platform=None, cl_ctx=None, cl_device=None, test_fft=True, verbose=True):
        """
        Use an OpenCL device for computing. This method should only be called to more selectively choose an OpenCL
        device from a known context or platform. select_gpu() should be preferred.

        :param gpu_name: a (sub)string matching the name of the gpu to be used
        :param platform: a (sub)string matching the name of the opencl platform to be used
        :param cl_ctx: if already know, a pyopencl context can be supplied
        :param cl_device: if already know, a pyopencl device can be supplied
        :param test_fft: if True, will benchmark the devices using FFT
        :return: True if an opencl device was found and FFT calculations are working (using gpyfft & clFFT) with it
        """
        if has_opencl is False:
            return self.set_device(test_fft=test_fft, verbose=verbose)
        if cl_ctx is not None and type(cl_ctx) is opencl_device.cl.Context:
            return self.set_device(cl_ctx.devices[0], test_fft=test_fft, verbose=verbose)
        if cl_device is not None and type(cl_device) is opencl_device.cl.Device:
            return self.set_device(cl_device, test_fft=test_fft, verbose=verbose)
        # Try to find the fastest GPU
        if self.benchmark_fft_batch:
            fft_axis = (-2, -1)
        else:
            fft_axis = None
        if test_fft:
            gpu_speed = opencl_device.available_gpu_speed(cl_platform=platform, fft_shape=self.benchmark_fft_size,
                                                          axes=fft_axis, min_gpu_mem=None, verbose=verbose,
                                                          gpu_name=gpu_name)
        else:
            gpu_speed = opencl_device.available_gpu_speed(cl_platform=platform, fft_shape=None, axes=None,
                                                          min_gpu_mem=None, verbose=verbose, gpu_name=gpu_name)
        if len(gpu_speed) == 0:
            return self.set_device(verbose=verbose)
        if gpu_name is not None:
            found_gpu = False
            for g in gpu_speed:
                if g[0].name.lower().count(gpu_name.lower()) > 0:
                    return self.set_device(g[0], test_fft=False, verbose=verbose)
            return found_gpu
        return self.set_device(gpu_speed[0][0], test_fft=False, verbose=verbose)

    def use_cuda(self, gpu_name=None, cu_ctx=None, cu_device=None, test_fft=False, verbose=True):
        """
        Use a CUDA device for computing. This method should only be called to more selectively choose an CUDA
        device from a known context or device. select_gpu() should be preferred.

        :param gpu_name: a (sub)string matching the name of the gpu to be used
        :param cu_ctx: if already known, a pycuda context can be supplied
        :param cu_device: if already know, a pycuda device can be supplied
        :param test_fft: if True, will benchmark the devices using FFT
        :return: True if an cuda device was found and FFT calculations are working with it
        """
        if has_cuda is False:
            return self.set_device(test_fft=test_fft, verbose=verbose)
        if cu_ctx is not None and type(cu_ctx) is cuda_device.cu_drv.Context:
            return self.set_device(cu_ctx.get_device(), test_fft=test_fft, verbose=verbose)
        if cu_device is not None and type(cu_device) is cuda_device.cu_drv.Device:
            return self.set_device(cu_device, test_fft=test_fft, verbose=verbose)
        # Try to find the fastest GPU
        if test_fft:
            gpu_speed = cuda_device.available_gpu_speed(fft_shape=self.benchmark_fft_size,
                                                        batch=self.benchmark_fft_batch, verbose=verbose)
        else:
            gpu_speed = cuda_device.available_gpu_speed(fft_shape=None, batch=None, verbose=verbose)

        if len(gpu_speed) == 0:
            return False
        if gpu_name is not None:
            for g in gpu_speed:
                if g[0].name().lower().count(gpu_name.lower()) > 0:
                    return self.set_device(g[0], test_fft=False, verbose=verbose)
            return self.set_device()
        return self.set_device(gpu_speed[0][0], test_fft=False, verbose=verbose)

    def use_gpu(self, gpu_name=None, gpu_rank=0, ranking="fft_speed", language=None):
        warnings.warn('pynx.ProcessingUnit.use_gpu() is deprecated, please use select_gpu() instead',
                      DeprecationWarning)
        return self.select_gpu(gpu_name=gpu_name, gpu_rank=gpu_rank, ranking=ranking, language=language)

    def select_gpu(self, gpu_name=None, gpu_rank=0, ranking="fft_speed", language=None, verbose=False):

        """
        Automatically select the fastest GPU, either from CUDA or OpenCL.
        If no gpu name or language is given, but there is an environment variable 'PYNX_PU', that variable will
        be used to determine the processing unit used. This variable (case-insensitive) can correspond to a language
        (OpenCL, CUDA or CPU) or match a gpu name (e.g. 'Titan'), or be a combination separated by a dot,
        e.g. 'OpenCL.Titan', or be a language and a rank, e.g. 'CUDA.2' to select the 3rd CUDA device - note that in
        this case, the rank always corresponds to the order of the device, and not the fft speed. Last case, the first
        part can match the gpu name, and the second part can be a rank (if using a device with multiple identical GPU).

        If MPI is used and multiple devices are available, the selection is made according to the MPI rank,
        after the gpu name filtering is taken into account.

        This will call set_device() with the selected device.

        :param gpu_name: a (sub)string matching the name of the gpu to be used. This can also be a list of
                         acceptable gpu names.
        :param gpu_rank: the rank of the GPU to use. If ranking is 'fft_speed', this will select from the fastest
                         GPU. If ranking is 'order', it will just find the GPU matching the name, in the order
                         they are found.
        :param ranking: default is 'fft_speed' (or 'fft'), all GPU are tested for FFT speed, and chosen from that
                        ranking. If ranking is 'order', they will be listed as they are found by CUDA/OpenCL.
                        This is useful to select a GPU just based on its name, without the overhead of the FFT test.
                        Ignored if using MPI.
        :param language: either 'opencl', 'cuda', 'cpu', or None. If None, the preferred language is cuda>opencl>cpu
        :param verbose: True or False (default) for verbosity
        :return: True if a GPU (or CPU) could be selected. An exception is returned if no GPU was found,
                 unless language was 'cpu'.
        :raise Exception: if no GPU could be selected, and the language was not 'cpu'. The message will indicate
                          if a gpu_name/language/gpu_rank was specified, but could not be selected.
        """

        benchmark_results = []

        if 'PYNX_PU' in os.environ:
            if pynx_py_env[0] not in ['opencl', 'cuda', 'cpu']:
                if len(pynx_py_env[0]) > 0:
                    gpu_name = pynx_py_env[0]

            if len(pynx_py_env) == 2:
                if len(pynx_py_env[1]) > 0:
                    s = pynx_py_env[1]
                    try:
                        gpu_rank = int(s)
                        if gpu_rank < 100:
                            ranking = 'order'
                            if verbose:
                                print("PyNX processing unit: use #%d device from "
                                      "PYNX_PU environment variable" % gpu_rank)
                        else:
                            # that's for the case where the card number (e.g. 1080) is given...
                            gpu_name = s
                            if verbose:
                                print("PyNX processing unit: searching '%s' GPU from PYNX_PU environment variable" % s)

                    except ValueError:
                        gpu_name = s
                        if verbose:
                            print("PyNX processing unit: searching '%s' GPU from PYNX_PU environment variable" % s)

        if mpi is not None:
            ranking = 'mpi'

        if ranking not in ['fft_speed', 'fft']:
            # This will deactivate FFT speed, just reporting found GPU devices
            fft_shape = None
        else:
            fft_shape = self.benchmark_fft_size

        test_cuda = False
        if language is None:
            test_cuda = True
        elif 'cuda' in language.lower():
            test_cuda = True

        if test_cuda:
            # Test first CUDA - if selection is by name, CUDA devices will be reported before OpenCL
            if has_cuda:
                benchmark_results += cuda_device.available_gpu_speed(fft_shape=fft_shape,
                                                                     batch=self.benchmark_fft_batch, min_gpu_mem=None,
                                                                     verbose=verbose, gpu_name=gpu_name)
            if language is not None:
                if 'cuda' in language.lower():
                    if len(benchmark_results) == 0:
                        s = 'Desired GPU language is CUDA, but no device found !'
                        if gpu_name is not None:
                            s += ' [gpu_name=%s]' % str(gpu_name)
                        raise Exception(s)

        if len(benchmark_results) and mpi is not None:
            # if we have more than 1 CUDA GPU available, use MPI rank to select it
            nb = len(benchmark_results)
            r = mpi.Get_rank()
            return self.set_device(benchmark_results[r % nb][0], test_fft=False, verbose=verbose)

        test_opencl = False
        if language is None:
            test_opencl = True
        elif 'opencl' in language.lower():
            test_opencl = True
        if test_opencl:
            if has_opencl:
                if self.benchmark_fft_batch:
                    fft_axis = (-2, -1)
                else:
                    fft_axis = None
                benchmark_results += opencl_device.available_gpu_speed(fft_shape=fft_shape, axes=fft_axis,
                                                                       min_gpu_mem=None, verbose=verbose,
                                                                       gpu_name=gpu_name)
            if language is not None:
                if 'opencl' in language.lower():
                    if len(benchmark_results) == 0:
                        s = 'Desired GPU language is OpenCL, but no device found !'
                        if gpu_name is not None:
                            s += ' [gpu_name=%s]' % str(gpu_name)
                        raise Exception(s)

        if language is not None:
            if 'cpu' in language.lower():
                return self.set_device(test_fft=False, verbose=verbose)

        if gpu_name is not None:
            if 'cpu' in str(gpu_name).lower():
                return self.set_device(test_fft=False, verbose=verbose)

        if len(benchmark_results):
            if mpi is not None:
                # if we have more than 1 GPU available, use MPI rank to select it
                # This requires that a language be selected, or each device will be listed twice for cuda+opencl
                nb = len(benchmark_results)
                r = mpi.Get_rank()
                return self.set_device(benchmark_results[gpu_rank][r % nb], test_fft=False, verbose=verbose)

            if ranking == 'fft_speed':
                benchmark_results = sorted(benchmark_results, key=lambda t: -t[1])
            if len(benchmark_results) <= gpu_rank:
                if gpu_name is not None:
                    s = ' [gpu_name=%s]' % str(gpu_name)
                else:
                    s = ''
                raise Exception('Desired GPU%s rank is %d, but only %d GPU found !' % (s, len(benchmark_results),
                                                                                       gpu_rank))
            return self.set_device(benchmark_results[gpu_rank][0], test_fft=False, verbose=verbose)

        s = "Could not find a suitable GPU. Please check GPU name or CUDA/OpenCL installation"

        if gpu_name is not None:
            s += " [name=%s]" % str(gpu_name)
        raise Exception(s)

    @classmethod
    def get_context(cls, device):
        """
        Static method to get a context, using the static device context dictionary to avoid creating new contexts,
        which will use up the GPU memory.
        This abstract function is implemented in CUProcessingUnit and CLProcessingUnit classes, the base version should
        never be called.
        :param device: the pyCUDA or pyOpenCL device for which a context is desired
        :return: the OpenCL or
        """
        raise Exception('ProcessingUnit.get_context() is an abstract function, which should not be called. '
                        'You probably want to use a CLProcessingUnit or CUProcessingUnit.')

    def finish(self):
        """
        Wait till all processing unit calculations are finished

        Virtual method, should be derived for CUDA or OpenCL
        :return: Nothing
        """
        pass

    def synchronize(self):
        """
        Wait till all processing unit calculations are finished

        Virtual method, should be derived for CUDA or OpenCL
        :return: Nothing
        """
        warnings.warn("ProcessingUnit.synchronize() is deprecated. Use finish() instead.", DeprecationWarning)
        return self.finish()

    def fft(self, d, axes=None, normalize=False):
        """
        Perform an out-of-place FFT transform.

        This function is mostly a placeholder for derived GPU implementations, which could be useful for
        occasional FT on large arrays.
        :param d: the array to be transformed
        :param normalize: if True, the FFT will keep the norm of the array (abs(d)**2).sum()
        :param axes: the transform axes
        :return: the result of the FFT transform
        """
        # TODO: derive and accelerate this function for different processing units
        d = fftn(d, axes=axes)
        if normalize:
            d /= np.sqrt(d.size)
        return d

    def ifft(self, d, axes=None, normalize=False):
        """
        Perform an out-of-place FFT transform.

        This function is mostly a placeholder for derived GPU implementations, which could be useful for
        occasional FT on large arrays.
        :param d: the array to be transformed
        :param normalize: if True, the FFT will keep the norm of the array (abs(d)**2).sum()
        :param axes: the transform axes
        :return: the result of the inverse FFT transform
        """
        # TODO: derive and accelerate this function for different processing units
        d = fftn(d, axes=axes)
        if normalize:
            d *= np.sqrt(d.size)
        return d


# The default processing unit
default_processing_unit = ProcessingUnit()
