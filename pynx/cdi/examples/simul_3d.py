# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import numpy as np
from scipy.fftpack import ifftshift, fftshift, fft2
from matplotlib import pyplot as plt
from pynx.utils.pattern import fibonacci_urchin
from pynx.cdi import *

# Test on a simulated pattern
n = 128

# Object coordinates
tmp = np.arange(-n // 2, n // 2, dtype=np.float32)
z, y, x = np.meshgrid(tmp, tmp, tmp, indexing='ij')
r = np.sqrt(x ** 2 + y ** 2 + z ** 2)

support = None
if True:
    # 'Urchin' object (3d analog to a Siemens star
    obj0 = fibonacci_urchin(n, nb_rays=12, r_max=30, nb_rings=2)
    # Start from a slightly loose support
    support = r < 35
elif True:
    # Parallelepiped - should be trivial to reconstruct !
    obj0 = (abs(x) < 12) * (abs(y) < 10) * (abs(z) < 16)
    # Start from a slightly loose support
    support = (abs(x) < 20) * (abs(y) < 20) * (abs(z) < 25)

iobs = abs(ifftshift(fft2(fftshift(obj0.astype(np.complex64))))) ** 2
iobs = np.random.poisson(iobs * 1e10 / iobs.sum())
mask = np.zeros_like(iobs, dtype=np.int8)
if False:
    # Try masking some values in the central beam - more difficult !!
    iobs[255:257, 255:257, 255:257] = 0
    mask[255:257, 255:257, 255:257] = 1

plt.figure(figsize=(8, 8))

cdi = CDI(fftshift(iobs), obj=None, support=fftshift(support), mask=fftshift(mask), wavelength=1e-10,
          pixel_size_detector=55e-6)

# Do 50*4 cycles of HIO, displaying object every 50 cycle
cdi = (ShowCDI(fig_num=1) * RAAR() ** 50) ** 4 * cdi

# Compute LLK
IFT() * LLK() * FT() * cdi
print("LLK_n = %8.3f" % (cdi.get_llk(noise='poisson')))

for i in range(20):
    # Support update operator
    s = 0.25 + 2 * np.exp(-i / 4)
    sup = SupportUpdate(threshold_relative=0.4, smooth_width=s, force_shrink=False)

    # Do 40 cycles of HIO or RAAR, then 5 cycles of ER before support update
    cdi = ER() ** 5 * RAAR() ** 40 * cdi

    # Update support & display current object & diffraction
    cdi = ShowCDI(fig_num=1) * sup * cdi

    IFT() * LLK() * FT() * cdi

    print("RAAR+ER #%3d: LLK_n = %8.3f" % (i * 45, cdi.get_llk(noise='poisson')))

# Finish with ER then ML
for i in range(5):
    # Do 20 cycles of ML
    cdi = ML(reg_fac=1e-2) ** 20 * cdi

    # Display current object & diffraction
    cdi = ShowCDI(fig_num=1) * cdi

    IFT() * LLK() * FT() * cdi

    print("ML #%3d: LLK_n = %8.3f" % (i * 20, cdi.get_llk(noise='poisson')))
