.. toctree::
   :maxdepth: 2

.. _tutorial_scripts:

Tutorials
=========

Quick start information for `Ptychography`_ and `Coherent Diffraction Imaging`_ tools.

Coherent Diffraction Imaging
----------------------------
:doc:`CDI scripts <script-cdi>`

Example notebooks:
  * :doc:`CDI reconstruction of the ESRF logo <cdi-esrf-logo-id10>`

Ptychography
------------
:doc:`Introduction to Ptychography command-line scripts <script-ptycho>`

Example notebooks:
  * :doc:`Introduction using Ptychography operators <ptycho_operators>`
  * :doc:`Positions optimisation with an experimental dataset (CXI) <position-optim-CXI>`

Scattering
----------
Example notebooks:
  * :doc:`Fast 2D and 3D scattering calculations <scattering-fhkl>`
  * :doc:`A more elaborate example <scattering-2D-detector>`,
    including a focused beam illumination of a nano-crystal,
    and real coordinates on a 2D detector.


Wavefront
---------
Example notebooks:
  * :doc:`Introduction to Wavefront operators <wavefront_operators>`
  * :doc:`Example of Paganin back-propagation <paganin>`
