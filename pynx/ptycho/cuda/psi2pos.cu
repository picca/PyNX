/* -*- coding: utf-8 -*-
*
* PyNX - Python tools for Nano-structures Crystallography
*   (c) 2019-present : ESRF-European Synchrotron Radiation Facility
*       authors:
*         Vincent Favre-Nicolin, favre@esrf.fr
*/

/** Estimate the shift between the back-projected Psi array and the calculated object*probe array.
* This reduction returns ((dopx[i].conj() * dpsi[i]).real(), (dopx[i].conj() * dpsi[i]).real(),
*                          abs(dopx[i])**2, abs(dopy[i])**2)
* where:
*  dopx[i] is the derivative of the object along x, for pixel i, multiplied by the probe (similarly dopy along y)
*  dpsi[i] is the difference between the back-projected Psi array (after Fourier constraints) and obj*probe, at pixel i.
*
* Only the first mode is taken into account.
* if interp=false, nearest pixel interpolation is used for the object*probe calculation.
*/
__device__ my_float4 Psi2PosShift(const int i, complexf* psi, complexf* obj, complexf* probe, float* cx, float* cy,
                                  const float pixel_size, const float f, const int nx, const int ny, const int nxo,
                                  const int nyo, const int ii, const bool interp)
{
  const int prx = i % nx;
  const int pry = i / nx;

  // Coordinates in Psi array (origin at (0,0)). Assume nx ny are multiple of 2
  const int iy = pry - ny/2 + ny * (pry<(ny/2));
  const int ix = prx - nx/2 + nx * (prx<(nx/2));
  const int ipsi  = ix + iy * nx ;

  // Apply Quadratic phase factor after far field back-propagation
  const float y = (pry - ny/2) * pixel_size;
  const float x = (prx - nx/2) * pixel_size;
  const float tmp = -f*(x*x+y*y);

  // NOTE WARNING: if the argument becomes large (e.g. > 2^15, depending on implementation), native sin and cos may be wrong.
  float s, c;
  __sincosf(tmp , &s, &c);

  const complexf o = bilinear(obj, cx[ii]+prx, cy[ii]+pry, 0, nxo, nyo, interp, false);
  const complexf p = probe[i];
  const complexf dpsi = complexf(psi[ipsi].real()*c - psi[ipsi].imag()*s - (o.real()*p.real() - o.imag()*p.imag()) ,
                                 psi[ipsi].imag()*c + psi[ipsi].real()*s - (o.real()*p.imag() + o.imag()*p.real()));

  // Assume we have some buffer before reaching the array border
  // Gradient is calculated with subpixel interpolation.
  const complexf dox =   bilinear(obj, cx[ii]+prx+0.5f, cy[ii]+pry     , 0, nxo, nyo, true, false)
                       - bilinear(obj, cx[ii]+prx-0.5f, cy[ii]+pry     , 0, nxo, nyo, true, false);
  const complexf doy =   bilinear(obj, cx[ii]+prx     , cy[ii]+pry+0.5f, 0, nxo, nyo, true, false)
                       - bilinear(obj, cx[ii]+prx     , cy[ii]+pry-0.5f, 0, nxo, nyo, true, false);
  const complexf dopx = complexf(dox.real() * p.real() - dox.imag() * p.imag(),
                                 dox.real() * p.imag() + dox.imag() * p.real());
  const complexf dopy = complexf(doy.real() * p.real() - doy.imag() * p.imag(),
                                 doy.real() * p.imag() + doy.imag() * p.real());
  return my_float4(dopx.real() * dpsi.real() + dopx.imag() * dpsi.imag(),
                   dopy.real() * dpsi.real() + dopy.imag() * dpsi.imag(),
                   dopx.real() * dopx.real() + dopx.imag() * dopx.imag(),
                   dopy.real() * dopy.real() + dopy.imag() * dopy.imag());
}

/** Compute the shifts, and return the average
*
*/
__device__ complexf Psi2PosRed(const int i, my_float4* dxy, const float mult, const float max_shift,
                               const float min_shift, const float threshold, const int nb)
{
  float dx = dxy[i].x / fmaxf(dxy[i].z, 1e-30f) * mult;
  float dy = dxy[i].y / fmaxf(dxy[i].w, 1e-30f) * mult;

  if(dxy[i].z < threshold) dx = 0;
  if(dxy[i].w < threshold) dy = 0;

  const float dr  = sqrt(dx * dx + dy * dy);

  if(dr < min_shift)
  {
    dxy[i].x = 0;
    dxy[i].y = 0;
    return complexf(0,0);
  }
  if(dr > max_shift)
  {
    dx *= max_shift / dr;
    dy *= max_shift / dr;
  }

  dxy[i].x = dx;
  dxy[i].y = dy;
  return complexf(dx / nb, dy / nb);
}
