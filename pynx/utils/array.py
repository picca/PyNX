# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import numpy as np
from scipy.ndimage.measurements import center_of_mass
from scipy.fftpack import fftn, ifftn, fftshift, fftfreq


def rebin(a, rebin_f, normalize=False):
    """
     Rebin a 2 or 3-dimensional array. If its dimensions are not a multiple of rebin_f, the array will be cropped.
     
    Args:
        a: the array to resize
        rebin_f: the rebin factor - pixels will be summed by groups of rebin_f x rebin_f (x rebin_f). This can
                 also be a tuple/list of rebin values along each axis, e.g. rebin_f=(4,1,2) for a 3D array
        normalize: if True, the average value of the array will be kept, by dividing the result by rebin_f**a.ndim.
                   By default nor normalization is applied to preserve statistical properties.

    Returns:

    """
    ndim = a.ndim
    if type(rebin_f) is int:
        rebin_f = [rebin_f] * ndim
    else:
        assert ndim == len(rebin_f), "Rebin: number of dimensions does not agree with number of rebin values:" + str(
            rebin_f)
    if ndim == 2:
        ny, nx = a.shape
        a = a[:ny - (ny % rebin_f[0]), :nx - (nx % rebin_f[1])]
        sh = ny // rebin_f[0], rebin_f[0], nx // rebin_f[1], rebin_f[1]
        if normalize:
            if isinstance(rebin_f, int):
                return a.reshape(sh).sum(axis=(1, 3)) / rebin_f ** ndim
            else:
                n = rebin_f[0]
                for r in rebin_f[1:]:
                    n *= r
                return a.reshape(sh).sum(axis=(1, 3)) / n
        else:
            return a.reshape(sh).sum(axis=(1, 3))
    elif ndim == 3:
        nz, ny, nx = a.shape
        a = a[:nz - (nz % rebin_f[0]), :ny - (ny % rebin_f[1]), :nx - (nx % rebin_f[2])]
        sh = nz // rebin_f[0], rebin_f[0], ny // rebin_f[1], rebin_f[1], nx // rebin_f[2], rebin_f[2]
        if normalize:
            if isinstance(rebin_f, int):
                return a.reshape(sh).sum(axis=(1, 3, 5)) / rebin_f ** ndim
            else:
                n = rebin_f[0]
                for r in rebin_f[1:]:
                    n *= r
                return a.reshape(sh).sum(axis=(1, 3, 5)) / n
        else:
            return a.reshape(sh).sum(axis=(1, 3, 5))
    elif ndim == 4:
        n3, nz, ny, nx = a.shape
        a = a[:n3 - (n3 % rebin_f[0]), :nz - (nz % rebin_f[1]), :ny - (ny % rebin_f[2]), :nx - (nx % rebin_f[3])]
        sh = n3 // rebin_f[0], rebin_f[0], nz // rebin_f[1], rebin_f[1], ny // rebin_f[2], rebin_f[2], \
             nx // rebin_f[3], rebin_f[3]
        a = a.reshape(sh)
        # print("rebin(): a.shape=", a.shape)
        if normalize:
            if isinstance(rebin_f, int):
                return a.sum(axis=(1, 3, 5, 7)) / rebin_f ** ndim
            else:
                n = rebin_f[0]
                for r in rebin_f[1:]:
                    n *= r
                return a.sum(axis=(1, 3, 5, 7)) / n
        else:
            return a.sum(axis=(1, 3, 5, 7))
    else:
        raise Exception("pynx.utils.array.rebin() only accept arrays of dimensions 2, 3 and 4")


def center_array_2d(a, other_arrays=None, threshold=0.2, roi=None, iz=None):
    """
    Center an array in 2D so that its absolute value barycenter is in the middle.
    If the array is 3D, it is summed along the first axis to determine the barycenter, and all frames along the first
    axis are shifted.
    The array is 'rolled' so that values shifted from the right appear on the left, etc...
    Shifts are integer - no interpolation is done.

    Args:
        a: the array to be shifted, can be a floating point or complex 2D or 3D array.
        other_arrays: can be another array or a list of arrays to be shifted by the same amount as a
        threshold: only the pixels above the maximum amplitude * threshold will be used for the barycenter
        roi: tuple of (x0, x1, y0, y1) corners coordinate of ROI to calculate center of mass
        iz: if a.ndim==3, the centering will be done based on the center of mass of the absolute value summed over all
            2D stacks. If iz is given, the center of mass will be calculated just on that stack

    Returns:
        the shifted array if only one is given or a tuple of the shifted arrays.
    """
    if a.ndim == 3:
        if iz is None:
            tmp = abs(a).astype(np.float32).sum(axis=0)
        else:
            tmp = abs(a[iz]).astype(np.float32)
    else:
        tmp = abs(a).astype(np.float32)

    if threshold is not None:
        tmp *= tmp > (tmp.max() * threshold)

    y0, x0 = center_of_mass(tmp)

    if roi is not None:
        xo, x1, yo, y1 = roi
        tmproi = tmp[yo:y1, xo:x1]
        y0, x0 = center_of_mass(tmproi)
        y0 += yo
        x0 += xo

    ny, nx = tmp.shape
    dx, dy = (int(round(nx // 2 - x0)), int(round(ny // 2 - y0)))
    # print("Shifting by: dx=%6.2f dy=%6.2f" % (dx, dy))

    # Multi-axis shift is supported only in numpy version >= 1.12 (2017)
    a1 = np.roll(np.roll(a, dy, axis=-2), dx, axis=-1)
    if other_arrays is None:
        return a1
    else:
        if type(other_arrays) is list:
            v = []
            for b in other_arrays:
                v.append(np.roll(b, (dy, dx), axis=(-2, -1)))
            return a1, v
        else:
            return a1, np.roll(other_arrays, (dy, dx), axis=(-2, -1))


def crop_around_support(obj: np.ndarray, sup: np.ndarray, margin=0):
    """

    :param obj: the array to be cropped (2D or 3D)
    :param sup: the support, either a boolean or integer array, with the same dimensions as a, 0 (False) indicating
                the pixels outside the support.
    :param margin: the number or pixels to be added on all sides of the array
    :return: a tuple with (cropped array, cropped support), keeping only pixels inside the support
    """
    if obj.ndim == 3:
        l0 = np.nonzero(sup.sum(axis=(1, 2)))[0].take([0, -1]) + np.array([-margin, margin])
        if l0[0] < 0:
            l0[0] = 0
        if l0[1] >= sup.shape[0]:
            l0[1] = -1

        l1 = np.nonzero(sup.sum(axis=(0, 2)))[0].take([0, -1]) + np.array([-margin, margin])
        if l1[0] < 0:
            l1[0] = 0
        if l1[1] >= sup.shape[1]:
            l1[1] = -1

        l2 = np.nonzero(sup.sum(axis=(0, 1)))[0].take([0, -1]) + np.array([-margin, margin])
        if l2[0] < 0:
            l2[0] = 0
        if l2[1] >= sup.shape[2]:
            l2[1] = -1
        obj = obj[l0[0]:l0[1], l1[0]:l1[1], l2[0]:l2[1]]
        sup = sup[l0[0]:l0[1], l1[0]:l1[1], l2[0]:l2[1]]
    else:
        l0 = np.nonzero(sup.sum(axis=1))[0].take([0, -1]) + np.array([-margin, margin])
        if l0[0] < 0:
            l0[0] = 0
        if l0[1] >= sup.shape[0]:
            l0[1] = -1

        l1 = np.nonzero(sup.sum(axis=0))[0].take([0, -1]) + np.array([-margin, margin])
        if l1[0] < 0:
            l1[0] = 0
        if l1[1] >= sup.shape[1]:
            l1[1] = -1

        obj = obj[l0[0]:l0[1], l1[0]:l1[1]]
        sup = sup[l0[0]:l0[1], l1[0]:l1[1]]

    return obj, sup


def interp_linear(a: np.ndarray, dr, mask: np.ndarray = None, return_weight=False):
    """ Perform a bi/tri-linear interpolation of a 2D/3D array.

    :param a: the 2D or 3D array to interpolate
    :param dr: the shift (along each dimension) of the array for the interpolation
    :param mask: the mask of values (True or >0 values are masked) to compute the interpolation
    :param return_weight: if true, also return the weight of the interpolation, which should be equal to 1 if no
                          point needed for the interpolation were masked, and a value between 0 and 1 otherwise.
    :return: a masked interpolated array
    """
    if mask is None:
        mask = np.ones_like(a, dtype=np.int8)
    else:
        mask = (mask == 0).astype(np.int8)

    if a.ndim == 2:
        ax = 0, 1
        y, x = dr
        y0, x0 = np.int(np.floor(y)), np.int(np.floor(x))
        dy, dx = y - y0, x - x0
        aa = np.roll(a, [y0, x0], axis=ax)
        m = np.roll(mask, [y0, x0], axis=ax)
        v = aa * (1 - dy) * (1 - dx) * m \
            + np.roll(aa, [0, 1], axis=ax) * (1 - dy) * dx * np.roll(m, [0, 1], axis=ax) \
            + np.roll(aa, [1, 0], axis=ax) * dy * (1 - dx) * np.roll(m, [1, 0], axis=ax) \
            + np.roll(aa, [1, 1], axis=ax) * dy * dx * np.roll(m, [1, 1], axis=ax)
        w = (1 - dy) * (1 - dx) * m + (1 - dy) * dx * np.roll(m, [0, 1], axis=ax) \
            + dy * (1 - dx) * np.roll(m, [1, 0], axis=ax) + dy * dx * np.roll(m, [1, 1], axis=ax)
    elif a.ndim == 3:
        ax = 0, 1, 2
        z, y, x = dr
        z0, y0, x0 = np.int(np.floor(z)), np.int(np.floor(y)), np.int(np.floor(x))
        dz, dy, dx = z - z0, y - y0, x - x0
        aa = np.roll(a, [z0, y0, x0], axis=ax)
        m = np.roll(mask, [z0, y0, x0], axis=ax)
        v = aa * (1 - dz) * (1 - dy) * (1 - dx) * m \
            + np.roll(aa, [0, 0, 1], axis=ax) * (1 - dz) * (1 - dy) * dx * np.roll(m, [0, 0, 1], axis=ax) \
            + np.roll(aa, [0, 1, 0], axis=ax) * (1 - dz) * dy * (1 - dx) * np.roll(m, [0, 1, 0], axis=ax) \
            + np.roll(aa, [0, 1, 1], axis=ax) * (1 - dz) * dy * dx * np.roll(m, [0, 1, 1], axis=ax) \
            + np.roll(aa, [1, 0, 0], axis=ax) * dz * (1 - dy) * (1 - dx) * np.roll(m, [1, 0, 0], axis=ax) \
            + np.roll(aa, [1, 0, 1], axis=ax) * dz * (1 - dy) * dx * np.roll(m, [1, 0, 1], axis=ax) \
            + np.roll(aa, [1, 1, 0], axis=ax) * dz * dy * (1 - dx) * np.roll(m, [1, 1, 0], axis=ax) \
            + np.roll(aa, [1, 1, 1], axis=ax) * dz * dy * dx * np.roll(m, [1, 1, 1], axis=ax)
        w = (1 - dz) * (1 - dy) * (1 - dx) * m + (1 - dz) * (1 - dy) * dx * np.roll(m, [0, 0, 1], axis=ax) \
            + (1 - dz) * dy * (1 - dx) * np.roll(m, [0, 1, 0], axis=ax) \
            + (1 - dz) * dy * dx * np.roll(m, [0, 1, 1], axis=ax) \
            + dz * (1 - dy) * (1 - dx) * np.roll(m, [1, 0, 0], axis=ax) \
            + dz * (1 - dy) * dx * np.roll(m, [1, 0, 1], axis=ax) \
            + dz * dy * dz * np.roll(m, [1, 1, 0], axis=ax) \
            + dz * dy * dx * np.roll(m, [1, 1, 1], axis=ax)

    tmp = w < 1e-6
    if return_weight:
        return np.ma.masked_array(v / (w + 1e-6 * tmp), mask=tmp), w
    return np.ma.masked_array(v / (w + 1e-6 * tmp), mask=tmp)


def array_derivative(a, dr, mask=None, phase=False):
    """ Compute the derivative of an array along a given direction

    :param a: the complex array for which the derivative will be calculated
    :param dr: the shift in pixels (with a value along each of the array dimensions)
       the value returned will be (a(r+dr)-a[r-dr])/2/norm2(dr), or if
       one of the values is masked, e.g. (a(r+dr)-a[r])/norm2(dr)
    :param phase: if True, will return instead the derivative of the phase of
       the supplied complex array. Will return an error if the supplied
       array is not complex.
    :return: a masked array of the gradient.
    """
    dr = np.array(dr, dtype=np.float32)
    a0, w0 = np.ma.masked_array(a, mask=mask), (mask == 0).astype(np.float32)
    ap, wp = interp_linear(a, -dr, mask=mask, return_weight=True)
    am, wm = interp_linear(a, dr, mask=mask, return_weight=True)
    n = np.sqrt((dr ** 2).sum())
    if phase:
        d = np.angle(ap / am) / (2 * n) * wp * wm + np.angle(ap / a0) / n * wp * w0 * (1 - wm) + np.angle(
            a0 / am) / n * w0 * wm * (1 - wp)
    else:
        d = (ap - am) / (2 * n) * wp * wm + (ap - a0) / n * wp * w0 * (1 - wm) + (a0 - am) / n * w0 * wm * (1 - wp)
    m = (wp * wm + wp * w0 * (1 - wm) + w0 * wm * (1 - wp)) == 0
    return np.ma.masked_array(d, mask=m)


def fourier_shift(a, shift, axes=None, positivity=False):
    """ Sub-pixel shift of an array. The return type will be the same as the input.

    :param a: the array to shift, with N dimensions
    :param shift: the shift along each axis
    :param axes: a tuple of the axes along which the shift is performed.
                 If None, all axes are transformed
    :param positivity: if True, all values <0 will be set to zero for the output
    :return: the fft-shifted array
    """
    if axes is None:
        axes = range(a.ndim)
    shifts = np.zeros(a.ndim,dtype=np.float32)
    assert len(axes) == len(shift)
    for i in range(len(axes)):
        shifts[axes[i]] = shift[i]
    af = fftn(a, axes=axes)
    xi = [fftfreq(a.shape[i]) * shifts[i] for i in range(a.ndim)]
    k = np.array(np.meshgrid(*xi, indexing='ij')).sum(axis=0)
    af *= np.exp(-2j * np.pi * k)
    r = ifftn(af, axes=axes)
    if r.dtype != a.dtype:
        if a.dtype in [np.float, np.float32, np.float64]:
            r = r.real.astype(a.dtype)
            if positivity:
                r[r < 0] = 0
        else:
            r = r.astype(a.dtype)
    return r
