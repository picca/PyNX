#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2018-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

"""
This package includes tests for the CDI command-line scripts.
"""

import os
import sys
import subprocess
import unittest
import tempfile
import shutil
import warnings

has_mpi = False
try:
    from mpi4py import MPI
    import shutil

    if shutil.which('mpiexec') is not None:
        has_mpi = True
except ImportError:
    pass

from pynx.ptycho.test.test_ptycho import make_ptycho_data, make_ptycho_data_cxi
from pynx.processing_unit import has_cuda, has_opencl

exclude_cuda = False
exclude_opencl = False
if 'PYNX_PU' in os.environ:
    if 'opencl' in os.environ['PYNX_PU'].lower():
        exclude_cuda = True
    elif 'cuda' in os.environ['PYNX_PU'].lower():
        exclude_opencl = True


class TestPtychoRunner(unittest.TestCase):
    """
    Class for tests of the Ptycho runner scripts
    """

    @classmethod
    def setUpClass(cls):
        cls.tmp_dir = tempfile.mkdtemp()

    @classmethod
    def tearDownClass(cls):
        if True:
            # print("Removing temporary directory: %s" % cls.tmp_dir)
            shutil.rmtree(cls.tmp_dir)
        else:
            print('Leaving test data in:', cls.tmp_dir)

    @unittest.skipIf('cuda' in sys.argv or exclude_opencl, "OpenCL tests skipped")
    @unittest.skipUnless('live_plot' in sys.argv or 'liveplot' in sys.argv, "live plot tests skipped")
    @unittest.skipIf(has_opencl is False, 'no OpenCL support')
    def test_ptycho_runner_cxi_liveplot_opencl(self):
        my_env = os.environ.copy()
        my_env["PYNX_PU"] = "opencl"
        path = make_ptycho_data_cxi(dsize=160, nb_frame=40, nb_photons=1e9, dir_name=self.tmp_dir)
        with subprocess.Popen(['pynx-cxipty.py', 'data=%s' % path, 'probe=gaussian,400e-9x400e-9',
                               'algorithm=ML**20,DM**20,nbprobe=2,DM**20,nbprobe=1,probe=1',
                               'verbose=10', 'save=all', 'saveplot', 'liveplot'],
                              stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.tmp_dir, env=my_env) as p:
            stdout, stderr = p.communicate(timeout=200)
            res = p.returncode
            self.assertFalse(res, msg=stderr.decode())

    @unittest.skipIf('cuda' in sys.argv or exclude_opencl, "OpenCL tests skipped")
    @unittest.skipIf(has_opencl is False, 'no OpenCL support')
    def test_ptycho_runner_cxi_opencl(self):
        my_env = os.environ.copy()
        my_env["PYNX_PU"] = "opencl"
        path = make_ptycho_data_cxi(dsize=160, nb_frame=40, nb_photons=1e9, dir_name=self.tmp_dir)
        with subprocess.Popen(['pynx-cxipty.py', 'data=%s' % path, 'probe=gaussian,400e-9x400e-9',
                               'algorithm=analysis,ML**20,DM**20,nbprobe=2,DM**20,nbprobe=1,probe=1',
                               'verbose=10', 'save=all', 'saveplot'],
                              stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.tmp_dir, env=my_env) as p:
            stdout, stderr = p.communicate(timeout=200)
            res = p.returncode
            self.assertFalse(res, msg=stderr.decode())

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests skipped")
    @unittest.skipUnless('live_plot' in sys.argv or 'liveplot' in sys.argv, "live plot tests skipped")
    @unittest.skipIf(has_cuda is False, 'no CUDA support')
    def test_ptycho_runner_cxi_liveplot_cuda(self):
        my_env = os.environ.copy()
        my_env["PYNX_PU"] = "cuda"
        path = make_ptycho_data_cxi(dsize=160, nb_frame=40, nb_photons=1e9, dir_name=self.tmp_dir)
        with subprocess.Popen(['pynx-cxipty.py', 'data=%s' % path, 'probe=gaussian,400e-9x400e-9',
                               'algorithm=ML**20,DM**20,nbprobe=2,DM**20,nbprobe=1,probe=1',
                               'verbose=10', 'save=all', 'saveplot', 'liveplot'],
                              stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.tmp_dir, env=my_env) as p:
            stdout, stderr = p.communicate(timeout=200)
            res = p.returncode
            self.assertFalse(res, msg=stderr.decode())

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests skipped")
    @unittest.skipIf(has_cuda is False, 'no CUDA support')
    def test_ptycho_runner_cxi_cuda(self):
        my_env = os.environ.copy()
        my_env["PYNX_PU"] = "cuda"
        path = make_ptycho_data_cxi(dsize=160, nb_frame=40, nb_photons=1e9, dir_name=self.tmp_dir)
        with subprocess.Popen(['pynx-cxipty.py', 'data=%s' % path, 'probe=gaussian,400e-9x400e-9',
                               'algorithm=analysis,ML**20,DM**20,nbprobe=2,DM**20,nbprobe=1,probe=1',
                               'verbose=10', 'save=all', 'saveplot'],
                              stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.tmp_dir, env=my_env) as p:
            stdout, stderr = p.communicate(timeout=200)
            res = p.returncode
            self.assertFalse(res, msg=stderr.decode())

    @unittest.skipIf('opencl' in sys.argv or exclude_cuda, "CUDA tests skipped")
    @unittest.skipIf(has_cuda is False, 'no CUDA support')
    @unittest.skipIf(has_mpi is False, 'no MPI support')
    def test_ptycho_runner_cxi_cuda_mpi(self):
        my_env = os.environ.copy()
        my_env["PYNX_PU"] = "cuda"
        path = make_ptycho_data_cxi(dsize=160, nb_frame=40, nb_photons=1e9, dir_name=self.tmp_dir)
        with subprocess.Popen(['mpiexec', '-n', '2', 'pynx-cxipty.py',
                               'data=%s' % path, 'probe=gaussian,400e-9x400e-9', 'mpi=split',
                               'algorithm=analysis,ML**20,DM**20,nbprobe=2,DM**20,nbprobe=1,probe=1',
                               'verbose=10', 'save=all', 'saveplot'],
                              stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.tmp_dir, env=my_env) as p:
            stdout, stderr = p.communicate(timeout=200)
            res = p.returncode
            self.assertFalse(res, msg=stderr.decode())

    @unittest.skipIf('cuda' in sys.argv or exclude_opencl, "OpenCL tests skipped")
    @unittest.skipIf(has_opencl is False, 'no OpenCL support')
    @unittest.skipIf(has_mpi is False, 'no MPI support')
    def test_ptycho_runner_cxi_opencl_mpi(self):
        my_env = os.environ.copy()
        my_env["PYNX_PU"] = "opencl"
        path = make_ptycho_data_cxi(dsize=160, nb_frame=40, nb_photons=1e9, dir_name=self.tmp_dir)
        with subprocess.Popen(['mpiexec', '-n', '2', 'pynx-cxipty.py',
                               'data=%s' % path, 'probe=gaussian,400e-9x400e-9', 'mpi=split',
                               'algorithm=analysis,ML**20,DM**20,nbprobe=2,DM**20,nbprobe=1,probe=1',
                               'verbose=10', 'save=all', 'saveplot'],
                              stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.tmp_dir, env=my_env) as p:
            stdout, stderr = p.communicate(timeout=200)
            res = p.returncode
            self.assertFalse(res, msg=stderr.decode())

    def test_ptycho_runner_simulation(self):
        my_env = os.environ.copy()
        with subprocess.Popen(['pynx-simulationpty.py', 'frame_nb=64', 'frame_size=128',
                               'algorithm=analysis,ML**20,DM**20,nbprobe=1,probe=1',
                               'verbose=10', 'save=all', 'saveplot'],
                              stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.tmp_dir, env=my_env) as p:
            stdout, stderr = p.communicate(timeout=200)
            res = p.returncode
            self.assertFalse(res, msg=stderr.decode())


def suite():
    load_tests = unittest.defaultTestLoader.loadTestsFromTestCase
    test_suite = unittest.TestSuite([load_tests(TestPtychoRunner)])
    return test_suite


if __name__ == '__main__':
    # sys.stdout = io.StringIO()
    warnings.simplefilter('ignore')
    res = unittest.TextTestRunner(verbosity=2, descriptions=False).run(suite())
