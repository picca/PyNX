# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import numpy as np
from scipy.fftpack import fftshift
from scipy.signal import medfilt2d
from scipy.ndimage import gaussian_filter
import pylab as plt
from pynx.cdi import *

# Test on an experimental 3D pattern from id10
# iobs = np.load("/Users/favre/data/201701id10-AlTiNi/AlTiNi_sample1b_3D.npz")['data']
iobs = np.load("AlTiNi_sample1b_3D.npz")['data']

if False:
    # Crop the data to run it faster with less memory (laptop !)...
    dn = 96
    iobs = iobs[256 - dn:256 + dn, 256 - dn:256 + dn, 256 - 96:256 + dn]
else:
    dn = None

n = len(iobs)

# Object coordinates
tmp = np.arange(-n // 2, n // 2, dtype=np.float32)
z, y, x = np.meshgrid(tmp, tmp, tmp, indexing='ij')
rxy = np.sqrt(x ** 2 + y ** 2)
r = np.sqrt(x ** 2 + y ** 2 + z ** 2)

# Mask, >0 value are masked
if False:
    mask = np.empty_like(iobs, dtype=np.bool)
    for i in range(len(mask)):
        mask[i] = medfilt2d(iobs[i], 3)

    mask = (~mask).astype(np.int8)
elif True:
    # id10 way: all zero values are masked
    mask = (iobs <= 0).astype(np.int8)
else:
    # Mask as tight as possible
    mask = np.zeros_like(iobs, dtype=np.int8)
    mask += (y > 0) * (rxy > 230)
    mask += (y < 0) * (rxy > 250)
    mask += abs(y / (x + 1e-6)) < (39. / 230)
    mask += (rxy < 30) * (iobs == 0)

if True:
    # Symmetrize the data to lower the masked region (assumes a real-valued object..)
    tmp = (mask == 0).astype(np.float32)
    tmp += np.fliplr(tmp)
    mask = mask + np.fliplr(mask)
    tmp += 1e-6 * mask
    iobs = (iobs + np.fliplr(iobs)) / tmp

# Support
if False:
    support = (r < (70 * n / 512)).astype(np.int8)
else:
    # Cheat, start close to solution - only for 512**3 input data, otherwise scaling is needed !
    # sample_av = np.load('/Users/favre/data/201701id10-AlTiNi/average_AlTiNi_sample1b_3D_50_1.npz')['data']
    sample_av = np.load('average_AlTiNi_sample1b_3D_50_1.npz')['data']
    n0 = len(sample_av) // 2
    if dn is not None:
        support = abs(sample_av[n0 - dn:n0 + dn, n0 - dn:n0 + dn, n0 - 96:n0 + dn])
    else:
        # sample_av is only 384^3 pixels
        support = np.zeros_like(iobs)
        support[256 - n0:256 + n0, 256 - n0:256 + n0, 256 - n0:256 + n0] = abs(sample_av)
    support = (gaussian_filter(support, 2) > (support.max() / 20)).astype(np.int8)

# Starting from a real object
obj0 = (np.random.uniform(0, 1, iobs.shape) * support).astype(np.complex64)

# Scale the object to the Fourier amplitudes-this is necessary to avoid starting from completely wrong scaling
# in the masked part of the diffraction data
cdi = ScaleObj(method='F') * cdi

plt.figure(figsize=(8, 8))

cdi = CDI(fftshift(iobs), obj=None, support=fftshift(support), mask=fftshift(mask), wavelength=1e-10,
          pixel_size_detector=55e-6)

# Do 50*4 cycles of HIO/RAAR, displaying object every 50 cycle
cdi = (ShowCDI(fig_num=1) * RAAR() ** 50) ** 4 * cdi

# Compute LLK
IFT() * LLK() * FT() * cdi
print("LLK_n = %8.3f" % (cdi.get_llk(noise='poisson')))

for i in range(20):
    # Support update operator
    s = 0.25 + 2 * np.exp(-i / 4)
    sup = SupportUpdate(threshold_relative=0.3, threshold_percentile=50, smooth_width=s, force_shrink=True)

    # Do 40 cycles of HIO or RAAR, then 20 cycles of ER
    cdi = ER() ** 20 * RAAR() ** 40 * cdi

    # Do 20 cycles of ML
    cdi = ML(reg_fac=1e-2) ** 20 * cdi

    # Update support & display current object & diffraction
    cdi = ShowCDI(fig_num=1) * sup * cdi

    IFT() * LLK() * FT() * cdi

    print("LLK_n = %8.3f" % (cdi.get_llk(noise='poisson')))
