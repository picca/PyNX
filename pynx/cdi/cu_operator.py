# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

__all__ = ['default_processing_unit', 'AutoCorrelationSupport', 'FreePU', 'FT', 'IFT', 'FourierApplyAmplitude', 'ER',
           'CF', 'HIO', 'RAAR', 'GPS', 'ML', 'SupportUpdate', 'ScaleObj', 'LLK', 'LLKSupport', 'DetwinHIO',
           'DetwinRAAR', 'SupportExpand', 'ObjConvolve', 'ShowCDI', 'EstimatePSF', 'InterpIobsMask', 'ApplyAmplitude']

import warnings
import types
import gc
from random import randint
import numpy as np
from scipy.fftpack import fftn, ifftn, fftshift
from scipy.ndimage.measurements import center_of_mass

from ..processing_unit.cu_processing_unit import CUProcessingUnit
from ..processing_unit.kernel_source import get_kernel_source as getks
from ..processing_unit import default_processing_unit as main_default_processing_unit
import pycuda.driver as cu_drv
import pycuda.gpuarray as cua
import pycuda.tools as cu_tools
from pycuda.elementwise import ElementwiseKernel as CU_ElK
from pycuda.reduction import ReductionKernel as CU_RedK
from pycuda.compiler import SourceModule
import skcuda.fft as cu_fft

from ..operator import has_attr_not_none, OperatorException, OperatorSum, OperatorPower

from .cdi import OperatorCDI, CDI
from .cpu_operator import ShowCDI as ShowCDICPU
from ..utils.phase_retrieval_transfer_function import plot_prtf

my_float4 = cu_tools.get_or_register_dtype("my_float4",
                                           np.dtype([('a', '<f4'), ('b', '<f4'), ('c', '<f4'), ('d', '<f4')]))

my_float8 = cu_tools.get_or_register_dtype("my_float8",
                                           np.dtype([('a', '<f4'), ('b', '<f4'), ('c', '<f4'), ('d', '<f4'),
                                                     ('e', '<f4'), ('f', '<f4'), ('g', '<f4'), ('h', '<f4')]))


################################################################################################
# Patch CDI class so that we can use 5*w to scale it.
# OK, so this might be ugly. There will definitely be issues if several types of operators
# are imported (e.g. OpenCL and CUDA)
# Solution (?): in a different sub-module, implement dynamical type-checking to decide which
# Scale() operator to call.


def patch_method(cls):
    def __rmul__(self, x):
        # Multiply object by a scalar.
        if np.isscalar(x) is False:
            raise OperatorException("ERROR: attempted Op1 * Op2, with Op1=%s, Op2=%s" % (str(x), str(self)))
        return Scale(x) * self

    def __mul__(self, x):
        # Multiply object by a scalar.
        if np.isscalar(x) is False:
            raise OperatorException("ERROR: attempted Op1 * Op2, with Op1=%s, Op2=%s" % (str(self), str(x)))
        return self * Scale(x)

    cls.__rmul__ = __rmul__
    cls.__mul__ = __mul__


patch_method(CDI)


################################################################################################


class CUProcessingUnitCDI(CUProcessingUnit):
    """
    Processing unit in OpenCL space, for 2D and 3D CDI operations.

    Handles initializing the context and kernels.
    """

    def __init__(self):
        super(CUProcessingUnitCDI, self).__init__()

    def cu_init_kernels(self):
        # Elementwise kernels
        self.cu_scale = CU_ElK(name='cu_scale',
                               operation="d[i] = complexf(d[i].real() * s, d[i].imag() * s);",
                               preamble=getks('cuda/complex.cu'),
                               options=self.cu_options,
                               arguments="pycuda::complex<float> *d, const float s")

        self.cu_sum = CU_ElK(name='cu_sum',
                             operation="dest[i] += src[i]",
                             preamble=getks('cuda/complex.cu'),
                             options=self.cu_options,
                             arguments="pycuda::complex<float> *src, pycuda::complex<float> *dest")

        self.cu_mult = CU_ElK(name='cu_mult',
                              operation="dest[i] *= src[i]",
                              options=self.cu_options,
                              arguments="float *src, float *dest")

        self.cu_mult_complex = CU_ElK(name='cu_mult_complex',
                                      operation="dest[i] = pycuda::complex<float>(dest[i].real() * src[i].real() - dest[i].imag() * src[i].imag(), dest[i].real() * src[i].imag() + dest[i].imag() * src[i].real())",
                                      options=self.cu_options,
                                      arguments="pycuda::complex<float> *src, pycuda::complex<float> *dest")

        # TODO: find a justification for the minimum numerator value
        self.cu_div_float = CU_ElK(name='cu_div_float', operation="dest[i] = src[i] / fmax(dest[i],1e-8f)",
                                   arguments="float *src, float *dest")

        self.cu_scale_complex = CU_ElK(name='cu_scale_complex',
                                       operation="d[i] = complexf(d[i].real() * s.real() - d[i].imag() * s.imag(), d[i].real() * s.imag() + d[i].imag() * s.real());",
                                       preamble=getks('cuda/complex.cu'),
                                       options=self.cu_options,
                                       arguments="pycuda::complex<float> *d, const pycuda::complex<float> s")

        self.cu_square_modulus = CU_ElK(name='cu_square_modulus',
                                        operation="dest[i] = dot(src[i],src[i]);",
                                        preamble=getks('cuda/complex.cu'),
                                        options=self.cu_options,
                                        arguments="float *dest, pycuda::complex<float> *src")

        self.cu_apply_amplitude = CU_ElK(name='cu_apply_amplitude',
                                         operation="ApplyAmplitude(i, iobs, dcalc, scale_in, scale_out, zero_mask,"
                                                   "confidence_interval_factor, confidence_interval_factor_mask_min,"
                                                   "confidence_interval_factor_mask_max)",
                                         preamble=getks('cuda/complex.cu') + getks('cdi/cuda/apply_amplitude_elw.cu'),
                                         options=self.cu_options,
                                         arguments="float *iobs, pycuda::complex<float> *dcalc, const float scale_in,"
                                                   "const float scale_out, const signed char zero_mask,"
                                                   "const float confidence_interval_factor,"
                                                   "const float confidence_interval_factor_mask_min,"
                                                   "const float confidence_interval_factor_mask_max")

        self.cu_apply_amplitude_icalc = CU_ElK(name='cu_apply_amplitude_icalc',
                                               operation="ApplyAmplitudeIcalc(i, iobs, dcalc, icalc, scale_in,"
                                                         "scale_out, zero_mask, confidence_interval_factor,"
                                                         "confidence_interval_factor_mask_min,"
                                                         "confidence_interval_factor_mask_max)",
                                               preamble=getks('cuda/complex.cu') + getks(
                                                   'cdi/cuda/apply_amplitude_elw.cu'),
                                               options=self.cu_options,
                                               arguments="float *iobs, pycuda::complex<float> *dcalc, float *icalc,"
                                                         "const float scale_in, const float scale_out,"
                                                         "const signed char zero_mask,"
                                                         "const float confidence_interval_factor,"
                                                         "const float confidence_interval_factor_mask_min,"
                                                         "const float confidence_interval_factor_mask_max")

        self.cu_er = CU_ElK(name='cu_er', operation="ER(i, obj, support)",
                            preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_elw.cu'),
                            options=self.cu_options,
                            arguments="pycuda::complex<float> *obj, signed char *support")

        self.cu_er_real = CU_ElK(name='cu_er', operation="ER_real_pos(i, obj, support)",
                                 preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_elw.cu'),
                                 options=self.cu_options,
                                 arguments="pycuda::complex<float> *obj, signed char *support")

        self.cu_hio = CU_ElK(name='cu_hio', operation="HIO(i, obj, obj_previous, support, beta)",
                             preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_elw.cu'),
                             options=self.cu_options,
                             arguments="pycuda::complex<float> *obj, pycuda::complex<float> *obj_previous, signed char *support, float beta")

        self.cu_hio_real = CU_ElK(name='cu_hio_real', operation="HIO_real_pos(i, obj, obj_previous, support, beta)",
                                  preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_elw.cu'),
                                  options=self.cu_options,
                                  arguments="pycuda::complex<float> *obj, pycuda::complex<float> *obj_previous, signed char *support, float beta")

        self.cu_cf = CU_ElK(name='cu_cf', operation="CF(i, obj, support)",
                            preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_elw.cu'),
                            options=self.cu_options,
                            arguments="pycuda::complex<float> *obj, signed char *support")

        self.cu_cf_real = CU_ElK(name='cu_cf_real', operation="CF_real_pos(i, obj, support)",
                                 preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_elw.cu'),
                                 options=self.cu_options,
                                 arguments="pycuda::complex<float> *obj, signed char *support")

        self.cu_raar = CU_ElK(name='cu_raar', operation="RAAR(i, obj, obj_previous, support, beta)",
                              preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_elw.cu'),
                              options=self.cu_options,
                              arguments="pycuda::complex<float> *obj, pycuda::complex<float> *obj_previous, signed char *support, float beta")

        self.cu_raar_real = CU_ElK(name='cu_raar_real', operation="RAAR_real_pos(i, obj, obj_previous, support, beta)",
                                   preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_elw.cu'),
                                   options=self.cu_options,
                                   arguments="pycuda::complex<float> *obj, pycuda::complex<float> *obj_previous, signed char *support, float beta")

        self.cu_ml_poisson_psi_gradient = CU_ElK(name='cu_ml_poisson_psi_gradient',
                                                 operation="PsiGradient(i, psi, dpsi, iobs, nx, ny, nz)",
                                                 preamble=getks('cuda/complex.cu') + getks(
                                                     'cdi/cuda/cdi_ml_poisson_elw.cu'),
                                                 options=self.cu_options,
                                                 arguments="pycuda::complex<float>* psi, pycuda::complex<float>* dpsi,"
                                                           "float* iobs, const int nx, const int ny, const int nz")

        self.cu_ml_poisson_reg_support_gradient = CU_ElK(name='cu_ml_poisson_psi_gradient',
                                                         operation="RegSupportGradient(i, obj, objgrad, support, reg_fac)",
                                                         preamble=getks('cuda/complex.cu') + getks(
                                                             'cdi/cuda/cdi_ml_poisson_elw.cu'),
                                                         options=self.cu_options,
                                                         arguments="pycuda::complex<float>* obj, pycuda::complex<float>* objgrad, signed char* support, const float reg_fac")

        self.cu_ml_poisson_cg_linear = CU_ElK(name='cu_ml_poisson_psi_gradient', operation="A[i] = a*A[i] + b*B[i]",
                                              preamble=getks('cuda/complex.cu'), options=self.cu_options,
                                              arguments="const float a, pycuda::complex<float> *A, const float b, pycuda::complex<float> *B")

        self.cu_gps1 = CU_ElK(name='cu_gps1',
                              operation="GPS1(i, obj, z, t, sigma_o, nx, ny, nz)",
                              preamble=getks('cuda/complex.cu') + getks('cdi/cuda/gps_elw.cu'),
                              options=self.cu_options,
                              arguments="pycuda::complex<float>* obj, pycuda::complex<float>* z,"
                                        "const float t, const float sigma_o, const int nx, const int ny, const int nz")

        self.cu_gps2 = CU_ElK(name='cu_gps2',
                              operation="GPS2(i, obj, z, epsilon)",
                              preamble=getks('cuda/complex.cu') + getks('cdi/cuda/gps_elw.cu'),
                              options=self.cu_options,
                              arguments="pycuda::complex<float>* obj, pycuda::complex<float>* z,"
                                        "const float epsilon")

        self.cu_gps3 = CU_ElK(name='cu_gps3',
                              operation="GPS3(i, obj, z)",
                              preamble=getks('cuda/complex.cu') + getks('cdi/cuda/gps_elw.cu'),
                              options=self.cu_options,
                              arguments="pycuda::complex<float>* obj, pycuda::complex<float>* z")

        self.cu_gps4 = CU_ElK(name='cu_gps4',
                              operation="GPS4(i, obj, y, support, s, sigma_f, positivity, nx, ny, nz)",
                              preamble=getks('cuda/complex.cu') + getks('cdi/cuda/gps_elw.cu'),
                              options=self.cu_options,
                              arguments="pycuda::complex<float>* obj, pycuda::complex<float>* y, signed char *support,"
                                        "const float s, const float sigma_f, signed char positivity,"
                                        "const int nx, const int ny, const int nz")

        self.cu_mask_interp_dist = CU_ElK(name='cu_mask_interp_dist',
                                          operation="mask_interp_dist(i, iobs, k, dist_n, nx, ny, nz)",
                                          preamble=getks('cuda/mask_interp_dist.cu'),
                                          options=self.cu_options,
                                          arguments="float *iobs, const int k, const int dist_n,"
                                                    "const int nx, const int ny, const int nz")
        # Reduction kernels
        self.cu_nb_point_support = CU_RedK(np.int32, neutral="0", reduce_expr="a+b",
                                           map_expr="support[i]",
                                           options=self.cu_options,
                                           arguments="signed char *support")

        self.cu_llk_red = CU_RedK(my_float8, neutral="0",
                                  reduce_expr="a+b",
                                  map_expr="LLKAll(i, iobs, psi, scale)",
                                  preamble=getks('cuda/complex.cu') + getks('cuda/float_n.cu')
                                           + getks('cdi/cuda/llk_red.cu'),
                                  options=self.cu_options,
                                  arguments="float *iobs, pycuda::complex<float> *psi, const float scale")

        self.cu_llk_icalc_red = CU_RedK(my_float8, neutral="0",
                                        reduce_expr="a+b",
                                        map_expr="LLKAllIcalc(i, iobs, icalc, scale)",
                                        preamble=getks('cuda/complex.cu') + getks('cuda/float_n.cu')
                                                 + getks('cdi/cuda/llk_red.cu'),
                                        options=self.cu_options,
                                        arguments="float *iobs, float *icalc, const float scale")

        self.cu_llk_reg_support_red = CU_RedK(np.float32, neutral="0", reduce_expr="a+b",
                                              map_expr="LLKRegSupport(obj[i], support[i])",
                                              preamble=getks('cuda/complex.cu') + getks(
                                                  'cdi/cuda/cdi_llk_reg_support_red.cu'),
                                              options=self.cu_options,
                                              arguments="pycuda::complex<float> *obj, signed char *support")

        # Polak-Ribière CG coefficient
        self.cu_cg_polak_ribiere_red = CU_RedK(np.complex64, neutral="complexf(0,0)",
                                               reduce_expr="a+b",
                                               map_expr="PolakRibiereComplex(grad[i], lastgrad[i])",
                                               preamble=getks('cuda/complex.cu') + getks(
                                                   'cuda/cg_polak_ribiere_red.cu'),
                                               options=self.cu_options,
                                               arguments="pycuda::complex<float> *grad, pycuda::complex<float> *lastgrad")
        # Line minimization factor for CG
        self.cdi_ml_poisson_gamma_red = CU_RedK(np.complex64, neutral="complexf(0,0)",
                                                reduce_expr="a+b",
                                                map_expr="Gamma(obs, psi, dpsi, i)",
                                                preamble=getks('cuda/complex.cu') + getks(
                                                    'cdi/cuda/cdi_ml_poisson_red.cu'),
                                                options=self.cu_options,
                                                arguments="float *obs, pycuda::complex<float> *psi, pycuda::complex<float> *dpsi")

        self.cdi_ml_poisson_gamma_support_red = CU_RedK(np.complex64, neutral="complexf(0,0)",
                                                        reduce_expr="a+b",
                                                        map_expr="GammaSupport(obs, psi, dpsi, obj, dobj, support, reg_fac, i)",
                                                        preamble=getks('cuda/complex.cu') + getks(
                                                            'cdi/cuda/cdi_ml_poisson_red.cu'),
                                                        options=self.cu_options,
                                                        arguments="float *obs, pycuda::complex<float> *psi, pycuda::complex<float> *dpsi,"
                                                                  "pycuda::complex<float> *obj, pycuda::complex<float> *dobj, signed char *support, "
                                                                  "const float reg_fac")

        self.cu_support_update = CU_RedK(np.int32, neutral="0", reduce_expr="a+b",
                                         map_expr="SupportUpdate(i, d, support, threshold, force_shrink)",
                                         preamble=getks('cuda/complex.cu') + getks(
                                             'cdi/cuda/cdi_support_update_red.cu'),
                                         options=self.cu_options,
                                         arguments="float *d, signed char *support, const float threshold, const bool force_shrink")

        self.cu_support_update_border = CU_RedK(np.int32, neutral="0", reduce_expr="a+b",
                                                map_expr="SupportUpdateBorder(i, d, support, threshold, force_shrink)",
                                                preamble=getks('cuda/complex.cu') + getks(
                                                    'cdi/cuda/cdi_support_update_red.cu'),
                                                options=self.cu_options,
                                                arguments="float *d, signed char *support, const float threshold,"
                                                          "const bool force_shrink")

        # Init support from autocorrelation array
        self.cu_support_init = CU_RedK(np.int32, neutral="0", reduce_expr="a+b",
                                       map_expr="SupportInit(i, d, support, threshold)",
                                       preamble=getks('cuda/complex.cu') + getks('cdi/cuda/cdi_support_update_red.cu'),
                                       options=self.cu_options,
                                       arguments="pycuda::complex<float> *d, signed char *support, const float threshold")

        # Calculate the average amplitude and maximum intensity in the support (complex object)
        # TODO: avoid using direct access to _M_re and _M_im - unfortunately trying to use real() and imag()
        # returns a compilation error, because of the volatile keyword used for reduction.
        self.cu_average_max_red = CU_RedK(np.complex64, neutral="complexf(0,0)",
                                          reduce_expr="complexf(a._M_re+b._M_re, fmaxf(a._M_im,b._M_im))",
                                          map_expr="complexf(sqrtf(dot(obj[i], obj[i])), dot(obj[i], obj[i]))"
                                                   "* (float)(support[i])",
                                          options=self.cu_options, preamble=getks('cuda/complex.cu'),
                                          arguments="pycuda::complex<float> *obj, signed char *support")

        # Calculate the average amplitude and maximum intensity in the support (object amplitude)
        self.cu_average_max_abs_red = CU_RedK(np.complex64, neutral="complexf(0,0)",
                                              reduce_expr="complexf(a._M_re+b._M_re, fmaxf(a._M_im,b._M_im))",
                                              map_expr="complexf(obj[i], obj[i] * obj[i]) * (float)(support[i])",
                                              options=self.cu_options, preamble=getks('cuda/complex.cu'),
                                              arguments="float *obj, signed char *support")

        # Calculate the root mean square and maximum intensity in the support (object amplitude)
        self.cu_rms_max_abs_red = CU_RedK(np.complex64, neutral="complexf(0,0)",
                                          reduce_expr="complexf(a._M_re+b._M_re, fmaxf(a._M_im,b._M_im))",
                                          map_expr="complexf(obj[i] * obj[i], obj[i] * obj[i]) * (float)(support[i])",
                                          options=self.cu_options, preamble=getks('cuda/complex.cu'),
                                          arguments="float *obj, signed char *support")

        self.cu_scale_amplitude = CU_RedK(np.complex64, neutral="complexf(0,0)", reduce_expr="a+b",
                                          map_expr="ScaleAmplitude(i, iobs, calc)",
                                          preamble=getks('cuda/complex.cu') + getks('cdi/cuda/scale_obs_calc_red.cu'),
                                          options=self.cu_options,
                                          arguments="float * iobs, pycuda::complex<float> *calc")

        self.cu_scale_intensity = CU_RedK(np.complex64, neutral="complexf(0,0)", reduce_expr="a+b",
                                          map_expr="ScaleIntensity(i, iobs, calc)",
                                          preamble=getks('cuda/complex.cu') + getks('cdi/cuda/scale_obs_calc_red.cu'),
                                          options=self.cu_options,
                                          arguments="float * iobs, pycuda::complex<float> *calc")

        self.cu_scale_intensity_poisson = CU_RedK(np.complex64, neutral="complexf(0,0)", reduce_expr="a+b",
                                                  map_expr="ScaleIntensityPoisson(i, iobs, calc)",
                                                  preamble=getks('cuda/complex.cu') + getks(
                                                      'cdi/cuda/scale_obs_calc_red.cu'),
                                                  options=self.cu_options,
                                                  arguments="float * iobs, pycuda::complex<float> *calc")

        self.cu_scale_weighted_intensity = CU_RedK(np.complex64, neutral="complexf(0,0)", reduce_expr="a+b",
                                                   map_expr="ScaleWeightedIntensity(i, iobs, calc)",
                                                   preamble=getks('cuda/complex.cu') + getks(
                                                       'cdi/cuda/scale_obs_calc_red.cu'),
                                                   options=self.cu_options,
                                                   arguments="float * iobs, pycuda::complex<float> *calc")

        # Absolute maximum of complex array
        self.cu_max_red = CU_RedK(np.float32, neutral="0", reduce_expr="a > b ? a : b", map_expr="abs(d[i])",
                                  options=self.cu_options, preamble=getks('cuda/complex.cu'),
                                  arguments="pycuda::complex<float> *d")

        # Convolution kernels for support update (Gaussian)
        conv16c2f_mod = SourceModule(getks('cuda/complex.cu') + getks('cuda/convolution16c2f.cu'),
                                     options=self.cu_options)
        self.abs_gauss_convol_16x = conv16c2f_mod.get_function("gauss_convolc2f_16x")
        conv16f_mod = SourceModule(getks('cuda/complex.cu') + getks('cuda/convolution16f.cu'), options=self.cu_options)
        self.gauss_convol_16y = conv16f_mod.get_function("gauss_convolf_16y")
        self.gauss_convol_16z = conv16f_mod.get_function("gauss_convolf_16z")

        # Same using a binary window
        conv16b_mod = SourceModule(getks('cuda/convolution16b.cu'), options=self.cu_options)
        self.binary_window_convol_16x = conv16b_mod.get_function("binary_window_convol_16x")
        self.binary_window_convol_16y = conv16b_mod.get_function("binary_window_convol_16y")
        self.binary_window_convol_16z = conv16b_mod.get_function("binary_window_convol_16z")
        self.binary_window_convol_16x_mask = conv16b_mod.get_function("binary_window_convol_16x_mask")
        self.binary_window_convol_16y_mask = conv16b_mod.get_function("binary_window_convol_16y_mask")
        self.binary_window_convol_16z_mask = conv16b_mod.get_function("binary_window_convol_16z_mask")


"""
The default processing unit 
"""
default_processing_unit = CUProcessingUnitCDI()


class CUOperatorCDI(OperatorCDI):
    """
    Base class for a operators on CDI objects using OpenCL
    """

    def __init__(self, processing_unit=None):
        super(CUOperatorCDI, self).__init__()

        self.Operator = CUOperatorCDI
        self.OperatorSum = CUOperatorCDISum
        self.OperatorPower = CUOperatorCDIPower

        if processing_unit is None:
            self.processing_unit = default_processing_unit
        else:
            self.processing_unit = processing_unit
        if self.processing_unit.cu_ctx is None:
            # CUDA kernels have not been prepared yet, use a default initialization
            if main_default_processing_unit.cu_device is None:
                main_default_processing_unit.select_gpu(language='cuda')
            self.processing_unit.init_cuda(cu_device=main_default_processing_unit.cu_device,
                                           test_fft=False, verbose=False)

    def apply_ops_mul(self, cdi):
        """
        Apply the series of operators stored in self.ops to a CDI object.
        In this version the operators are applied one after the other to the same CDI object (multiplication)

        :param w: the wavefront to which the operators will be applied.
        :return: the wavefront, after application of all the operators in sequence
        """
        return super(CUOperatorCDI, self).apply_ops_mul(cdi)

    def prepare_data(self, cdi):
        # Make sure data is already in CUDA space, otherwise transfer it
        if cdi._timestamp_counter > cdi._cu_timestamp_counter:
            cdi._cu_obj = cua.to_gpu(cdi._obj)
            cdi._cu_support = cua.to_gpu(cdi._support)
            cdi._cu_iobs = cua.to_gpu(cdi.iobs)
            if cdi._k_psf is None:
                cdi._cu_k_psf_f = None
            else:
                cdi._cu_k_psf = cua.to_gpu(cdi._k_psf)
                # We keep the Fourier Transform of the PSF convolution kernel in GPU memory (half-Hermitian array)
                if cdi._k_psf.ndim == 2:
                    ny, nx = cdi._k_psf.shape
                    shape2 = (ny, nx // 2 + 1)
                    axes = (-1, -2)
                else:
                    nz, ny, nx = cdi._k_psf.shape
                    shape2 = (nz, ny, nx // 2 + 1)
                    axes = (-1, -2, -3)

                cu_k_psf = cua.to_gpu(cdi._k_psf)
                cdi._cu_k_psf_f = cua.empty(shape2, np.complex64)

                # With cuFFT, the shape for r2c and c2r plans is the one of the *real* array
                pu = self.processing_unit
                # TODO: do we really need two plans ?
                cdi._cu_plan_r2c = pu.cu_fft_get_plan(cu_k_psf.shape, batch=False,
                                                      dtype_src=np.float32, dtype_dest=np.complex64)
                cdi._cu_plan_c2r = pu.cu_fft_get_plan(cu_k_psf.shape, batch=False,
                                                      dtype_src=np.complex64, dtype_dest=np.float32)

                cu_fft.fft(cu_k_psf, cdi._cu_k_psf_f, cdi._cu_plan_r2c, scale=True)

            cdi._cu_timestamp_counter = cdi._timestamp_counter
        if has_attr_not_none(cdi, '_cu_obj_view') is False:
            cdi._cu_obj_view = {}

    def timestamp_increment(self, cdi):
        cdi._cu_timestamp_counter += 1

    def view_register(self, obj):
        """
        Creates a new unique view key in an object. When finished with this view, it should be de-registered
        using view_purge. Note that it only reserves the key, but does not create the view.
        :return: an integer value, which corresponds to yet-unused key in the object's view.
        """
        i = 1
        while i in obj._cu_obj_view:
            i += 1
        obj._cu_obj_view[i] = None
        return i

    def view_copy(self, obj, i_source, i_dest):
        if i_source == 0:
            src = obj._cu_obj
        else:
            src = obj._cu_obj_view[i_source]
        if i_dest == 0:
            obj._cu_obj = cua.empty_like(src)
            dest = obj._cu_obj
        else:
            obj._cu_obj_view[i_dest] = cua.empty_like(src)
            dest = obj._cu_obj_view[i_dest]
        cu_drv.memcpy_dtod(dest=dest.gpudata, src=src.gpudata, size=dest.nbytes)

    def view_swap(self, obj, i1, i2):
        if i1 != 0:
            if i1 not in obj._cu_obj_view:
                # Create dummy value, assume a copy will be made later
                obj._cu_obj_view[i1] = None
        if i2 != 0:
            if i2 not in obj._cu_obj_view:
                # Create dummy value, assume a copy will be made later
                obj._cu_obj_view[i2] = None
        if i1 == 0:
            obj._cu_obj, obj._cu_obj_view[i2] = obj._cu_obj_view[i2], obj._cu_obj
        elif i2 == 0:
            obj._cu_obj, obj._cu_obj_view[i1] = obj._cu_obj_view[i1], obj._cu_obj
        else:
            obj._cu_obj_view[i1], obj._cu_obj_view[i2] = obj._cu_obj_view[i2], obj._cu_obj_view[i1]

    def view_sum(self, obj, i_source, i_dest):
        if i_source == 0:
            src = obj._cu_obj
        else:
            src = obj._cu_obj_view[i_source]
        if i_dest == 0:
            dest = obj._cu_obj
        else:
            dest = obj._cu_obj_view[i_dest]
        self.processing_unit.cu_sum(src, dest)

    def view_purge(self, obj, i):
        if i is not None:
            del obj._cu_obj_view[i]
        elif has_attr_not_none(obj, '_cu_obj_view'):
            del obj._cu_obj_view
            self.processing_unit.synchronize()  # is this useful ?


# The only purpose of this class is to make sure it inherits from CUOperatorCDI and has a processing unit
class CUOperatorCDISum(OperatorSum, CUOperatorCDI):
    def __init__(self, op1, op2):
        if np.isscalar(op1):
            op1 = Scale(op1)
        if np.isscalar(op2):
            op2 = Scale(op2)
        if isinstance(op1, CUOperatorCDI) is False or isinstance(op2, CUOperatorCDI) is False:
            raise OperatorException(
                "ERROR: cannot add a CUOperatorCDI with a non-CUOperatorCDI: %s + %s" % (str(op1), str(op2)))
        # We can only have a sum of two CLOperatorWavefront, so they must have a processing_unit attribute.
        CUOperatorCDI.__init__(self, op1.processing_unit)
        OperatorSum.__init__(self, op1, op2)

        # We need to cherry-pick some functions & attributes doubly inherited
        self.Operator = CUOperatorCDI
        self.OperatorSum = CUOperatorCDISum
        self.OperatorPower = CUOperatorCDIPower
        self.prepare_data = types.MethodType(CUOperatorCDI.prepare_data, self)
        self.timestamp_increment = types.MethodType(CUOperatorCDI.timestamp_increment, self)
        self.view_copy = types.MethodType(CUOperatorCDI.view_copy, self)
        self.view_swap = types.MethodType(CUOperatorCDI.view_swap, self)
        self.view_sum = types.MethodType(CUOperatorCDI.view_sum, self)
        self.view_purge = types.MethodType(CUOperatorCDI.view_purge, self)


# The only purpose of this class is to make sure it inherits from CLOperatorWavefront and has a processing unit
class CUOperatorCDIPower(OperatorPower, CUOperatorCDI):
    def __init__(self, op, n):
        CUOperatorCDI.__init__(self, op.processing_unit)
        OperatorPower.__init__(self, op, n)

        # We need to cherry-pick some functions & attributes doubly inherited
        self.Operator = CUOperatorCDI
        self.OperatorSum = CUOperatorCDISum
        self.OperatorPower = CUOperatorCDIPower
        self.prepare_data = types.MethodType(CUOperatorCDI.prepare_data, self)
        self.timestamp_increment = types.MethodType(CUOperatorCDI.timestamp_increment, self)
        self.view_copy = types.MethodType(CUOperatorCDI.view_copy, self)
        self.view_swap = types.MethodType(CUOperatorCDI.view_swap, self)
        self.view_sum = types.MethodType(CUOperatorCDI.view_sum, self)
        self.view_purge = types.MethodType(CUOperatorCDI.view_purge, self)


class AutoCorrelationSupport(CUOperatorCDI):
    """
    Operator to calculate an initial support from the auto-correlation function of the observed intensity.
    """

    def __init__(self, threshold=0.2, verbose=False):
        """
        Operator initialization
        :param threshold: pixels above the autocorrelation maximimum multiplied by the threshold will be included
                          in the support
        """
        super(AutoCorrelationSupport, self).__init__()
        self.threshold = threshold
        self.verbose = verbose

    def op(self, cdi):
        tmp = cdi.get_iobs().copy()
        tmp[tmp < 0] = 0  # clear masked values
        tmp = cua.to_gpu(tmp.astype(np.complex64))
        plan = self.processing_unit.cu_fft_get_plan(tmp, batch=False)
        cu_fft.fft(tmp, tmp, plan, scale=False)
        thres = np.float32(self.processing_unit.cu_max_red(tmp).get() * self.threshold)
        cdi._cu_support = cua.zeros(cdi._cu_obj.shape, dtype=np.int8)
        cdi.nb_point_support = int(self.processing_unit.cu_support_init(tmp, cdi._cu_support, thres).get())
        tmp.gpudata.free()
        if self.verbose:
            print('AutoCorrelation: %d pixels in support (%6.2f%%), threshold = %f' %
                  (cdi.nb_point_support, cdi.nb_point_support * 100 / tmp.size, thres))
        return cdi


class CopyToPrevious(CUOperatorCDI):
    """
    Operator which will store a copy of the cdi object as cu_obj_previous. This is used for various algorithms, such
    as difference map or RAAR
    """

    def op(self, cdi):
        if has_attr_not_none(cdi, '_cu_obj_previous') is False:
            cdi._cu_obj_previous = cua.empty_like(cdi._cu_obj)
        if cdi._cu_obj_previous.shape != cdi._cu_obj.shape:
            cdi._cu_obj_previous = cua.empty_like(cdi._cu_obj)
        cu_drv.memcpy_dtod(dest=cdi._cu_obj_previous.gpudata, src=cdi._cu_obj.gpudata, size=cdi._cu_obj.nbytes)
        return cdi


class FromPU(CUOperatorCDI):
    """
    Operator copying back the CDI object and support data from the cuda device to numpy. The calculated complex
    amplitude is also retrieved by computing the Fourier transform of the current view of the object.
    """

    def op(self, cdi):
        warnings.warn("Use of ToPU() and FromPU() operators is now deprecated. Use get() and set() to access data.")
        # cdi._cu_obj.get(ary=cdi._obj)
        ## print("obj norm: ",(abs(cdi._obj)**2).sum())
        # cdi._cu_support.get(ary=cdi._support)
        ## TODO: find a more efficient way to access the calculated diffraction
        # cdi = FT() * cdi
        # cdi.calc = cdi._cu_obj.get()
        # cdi = IFT() * cdi
        return cdi


class ToPU(CUOperatorCDI):
    """
    Operator copying the wavefront data from numpy to the cuda device, as a complex64 array.
    """

    def op(self, cdi):
        warnings.warn("Use of ToPU() and FromPU() operators is now deprecated. Use get() and set() to access data.")
        # cdi._cu_obj = cua.to_gpu(cdi._obj)
        # cdi._cu_support = cua.to_gpu(cdi._support)
        # cdi._cu_iobs = cua.to_gpu(cdi.iobs)
        return cdi


class FreePU(CUOperatorCDI):
    """
    Operator freeing CUDA memory, removing any pycuda.gpuarray.Array attribute in the supplied CDI object.
    """

    def op(self, cdi):
        self.processing_unit.finish()
        self.processing_unit.cu_fft_free_plans()
        # Get back last object and support
        cdi.get_obj()
        # Purge all GPUarray data
        for o in dir(cdi):
            if isinstance(cdi.__getattribute__(o), cua.GPUArray):
                cdi.__setattr__(o, None)
        self.view_purge(cdi, None)
        return cdi

    def timestamp_increment(self, cdi):
        cdi._timestamp_counter += 1


class FreeFromPU(CUOperatorCDI):
    """
    Gets back data from OpenCL and removes all OpenCL arrays.
    
    DEPRECATED
    """

    def __new__(cls):
        return FreePU() * FromPU()


class Scale(CUOperatorCDI):
    """
    Multiply the object by a scalar (real or complex).
    """

    def __init__(self, x):
        """

        :param x: the scaling factor
        """
        super(Scale, self).__init__()
        self.x = x

    def op(self, cdi):
        if np.isreal(self.x):
            self.processing_unit.cu_scale(cdi._cu_obj, np.float32(self.x))
        else:
            self.processing_unit.cu_scale_complex(cdi._cu_obj, np.complex64(self.x))
        return cdi


class FT(CUOperatorCDI):
    """
    Forward Fourier transform.
    TODO: use a proper scaling, to keep the array norm through both forward and backward FT
    """

    def __init__(self, scale=True):
        """

        :param scale: if True, the Fourier transform will be normalised, so that the transformed array L2 norm will
                      remain constant (by dividing the output by the square root of the object's size).
                      If False or None, the array norm will not be changed. If a scalar is given, the output array
                      is multiplied by it.
        """
        super(FT, self).__init__()
        self.scale = scale

    def op(self, cdi):
        plan = self.processing_unit.cu_fft_get_plan(cdi._cu_obj, batch=False)
        cu_fft.fft(cdi._cu_obj, cdi._cu_obj, plan, scale=False)
        if self.scale is True:
            cdi = Scale(1 / np.sqrt(cdi.iobs.size)) * cdi
        elif (self.scale is not False) and (self.scale is not None):
            cdi = Scale(self.scale) * cdi
        cdi._is_in_object_space = False
        return cdi


class IFT(CUOperatorCDI):
    """
    Inverse Fourier transform
    TODO: use a proper scaling, to keep the array norm through both forward and backward FT
    """

    def __init__(self, scale=True):
        """

        :param scale: if True, the Fourier transform will be normalised, so that the transformed array L2 norm will
                      remain constant (by dividing the output by the square root of the object's size).
                      If False or None, the array norm will not be changed. If a scalar is given, the output array
                      is multiplied by it.
        """
        super(IFT, self).__init__()
        self.scale = scale

    def op(self, cdi):
        plan = self.processing_unit.cu_fft_get_plan(cdi._cu_obj, batch=False)
        cu_fft.ifft(cdi._cu_obj, cdi._cu_obj, plan, scale=False)
        if self.scale is True:
            cdi = Scale(1 / np.sqrt(cdi.iobs.size)) * cdi
        elif (self.scale is not False) and (self.scale is not None):
            cdi = Scale(self.scale) * cdi
        cdi._is_in_object_space = True
        return cdi


class Calc2Obs(CUOperatorCDI):
    """
    Copy the calculated intensities to the observed ones. Can be used for simulation.
    """

    def __init__(self):
        """

        """
        super(Calc2Obs, self).__init__()

    def op(self, cdi):
        if cdi.in_object_space():
            cdi = FT(scale=False) * cdi
            self.processing_unit.cu_square_modulus(cdi._cu_iobs, cdi._cu_obj)
            cdi = IFT(scale=False) * cdi
        else:
            self.processing_unit.cu_square_modulus(cdi._cu_iobs, cdi._cu_obj)
        return cdi


class ApplyAmplitude(CUOperatorCDI):
    """
    Apply the magnitude from an observed intensity, keep the phase. Optionally, calculate the log-likelihood before
    changing the amplitudes.
    """

    def __init__(self, calc_llk=False, zero_mask=False, scale_in=1, scale_out=1, confidence_interval_factor=0,
                 confidence_interval_factor_mask_min=0.5, confidence_interval_factor_mask_max=1.2):
        """

        :param calc_llk: if true, the log-likelihood will be calculated and stored in the object
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
            complex amplitude is kept with an optional scale factor.
        :param scale_in: a scale factor by which the input values should be multiplied, typically because of FFT
        :param scale_out: a scale factor by which the output values should be multiplied, typically because of FFT
        :param confidence_interval_factor: a relaxation factor, with the projection of calculated amplitude being done
           towards the limit of the poisson confidence interval. A value of 1
           corresponds to a 50% confidence interval, a value of 0 corresponds to a
           strict observed amplitude projection. [EXPERIMENTAL]
        :param confidence_interval_factor_mask_min, confidence_interval_factor_mask_max:
            For masked pixels where a value has been estimated (e.g. with InterpIobsMask()),
            a confidence interval can be given as a factor to be applied to the interpolated
            observed intensity. This corresponds to values stored between -1e19 and -1e38. [EXPERIMENTAL]
        """
        super(ApplyAmplitude, self).__init__()
        self.calc_llk = calc_llk
        self.scale_in = np.float32(scale_in)
        self.scale_out = np.float32(scale_out)
        self.zero_mask = np.int8(zero_mask)
        self.confidence_interval_factor = np.float32(confidence_interval_factor)
        self.confidence_interval_factor_mask_min = np.float32(confidence_interval_factor_mask_min)
        self.confidence_interval_factor_mask_max = np.float32(confidence_interval_factor_mask_max)

    def op(self, cdi):
        # TODO: use a single-pass reduction kernel to apply the amplitude and compute the LLK
        if cdi._k_psf is None:
            if self.calc_llk:
                cdi = LLK(scale=self.scale_in) * cdi
            self.processing_unit.cu_apply_amplitude(cdi._cu_iobs, cdi._cu_obj, self.scale_in, self.scale_out,
                                                    self.zero_mask, self.confidence_interval_factor,
                                                    self.confidence_interval_factor_mask_min,
                                                    self.confidence_interval_factor_mask_max)

        else:
            # FFT-based convolution, using half-Hermitian kernel and real->complex64 FFT
            cu_icalc = cua.empty_like(cdi._cu_iobs)  # float32
            cu_icalc_f = cua.empty_like(cdi._cu_k_psf_f)  # Complex64, half-Hermitian array

            self.processing_unit.cu_square_modulus(cu_icalc, cdi._cu_obj)
            # a = cua.sum(cu_icalc).get()
            cu_fft.fft(cu_icalc, cu_icalc_f, cdi._cu_plan_r2c, scale=False)
            self.processing_unit.cu_mult_complex(cdi._cu_k_psf_f, cu_icalc_f)
            cu_fft.ifft(cu_icalc_f, cu_icalc, cdi._cu_plan_c2r, scale=False)
            # b = cua.sum(cu_icalc).get()
            # print(a / b, b / a)
            if self.calc_llk:
                llk = self.processing_unit.cu_llk_icalc_red(cdi._cu_iobs, cu_icalc, self.scale_in ** 2).get()
                cdi.llk_poisson = llk['a']
                cdi.llk_gaussian = llk['b']
                cdi.llk_euclidian = llk['c']
                cdi.nb_photons_calc = llk['d']
                cdi.llk_poisson_free = llk['e']
                cdi.llk_gaussian_free = llk['f']
                cdi.llk_euclidian_free = llk['g']

            self.processing_unit.cu_apply_amplitude_icalc(cdi._cu_iobs, cdi._cu_obj, cu_icalc,
                                                          self.scale_in, self.scale_out, self.zero_mask,
                                                          self.confidence_interval_factor,
                                                          self.confidence_interval_factor_mask_min,
                                                          self.confidence_interval_factor_mask_max)
        return cdi


class FourierApplyAmplitude(CUOperatorCDI):
    """
    Fourier magnitude operator, performing a Fourier transform, the magnitude projection, and a backward FT.
    """

    def __init__(self, calc_llk=False, zero_mask=False, confidence_interval_factor=0,
                 confidence_interval_factor_mask_min=0.5, confidence_interval_factor_mask_max=1.2):
        """

        :param calc_llk: if True, the log-likelihood will be calculated while in diffraction space.
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
                          complex amplitude is kept with an optional scale factor.
        """
        super(FourierApplyAmplitude, self).__init__()
        self.calc_llk = calc_llk
        self.zero_mask = zero_mask
        self.confidence_interval_factor = confidence_interval_factor
        self.confidence_interval_factor_mask_min = confidence_interval_factor_mask_min
        self.confidence_interval_factor_mask_max = confidence_interval_factor_mask_max

    def op(self, cdi):
        # s = 1.0 / cdi._cu_iobs.size  # FFT scaling
        s = 1.0 / np.sqrt(cdi._cu_iobs.size)  # FFT scaling
        return IFT(scale=False) * ApplyAmplitude(calc_llk=self.calc_llk, zero_mask=self.zero_mask,
                                                 scale_in=s, scale_out=s,
                                                 confidence_interval_factor=self.confidence_interval_factor,
                                                 confidence_interval_factor_mask_min=0.5,
                                                 confidence_interval_factor_mask_max=1.2) * FT(scale=False) * cdi


class ERProj(CUOperatorCDI):
    """
    Error reduction.
    """

    def __init__(self, positivity=False):
        super(ERProj, self).__init__()
        self.positivity = positivity

    def op(self, cdi: CDI):
        if self.positivity:
            self.processing_unit.cu_er_real(cdi._cu_obj, cdi._cu_support)
        else:
            self.processing_unit.cu_er(cdi._cu_obj, cdi._cu_support)
        return cdi


class ER(CUOperatorCDI):
    """
    Error reduction cycle
    """

    def __init__(self, positivity=False, calc_llk=False, nb_cycle=1, show_cdi=False, fig_num=-1,
                 zero_mask=False, confidence_interval_factor=0,
                 confidence_interval_factor_mask_min=0.5, confidence_interval_factor_mask_max=1.2):
        """

        :param positivity: apply a positivity restraint
        :param calc_llk: if True, calculate llk while in Fourier space. If a positive integer is given, llk will be
                         calculated every calc_llk cycle
        :param nb_cycle: the number of cycles to perform
        :param show_cdi: if a positive integer number N, the object & probe will be displayed every N cycle.
                               By default 0 (no plot)
        :param fig_num: the number of the figure to plot the object intensity, as for ShowCDI()
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
                          complex amplitude is kept with an optional scale factor.
        :param confidence_interval_factor: a relaxation factor, with the projection of calculated amplitude being done
           towards the limit of the poisson confidence interval. A value of 1
           corresponds to a 50% confidence interval, a value of 0 corresponds to a
           strict observed amplitude projection. [EXPERIMENTAL]
        :param confidence_interval_factor_mask_min, confidence_interval_factor_mask_max:
            For masked pixels where a value has been estimated (e.g. with InterpIobsMask()),
            a confidence interval can be given as a factor to be applied to the interpolated
            observed intensity. This corresponds to values stored between -1e19 and -1e38. [EXPERIMENTAL]
        """
        super(ER, self).__init__()
        self.positivity = positivity
        self.calc_llk = calc_llk
        self.nb_cycle = nb_cycle
        self.show_cdi = show_cdi
        self.fig_num = fig_num
        self.zero_mask = zero_mask
        self.confidence_interval_factor = confidence_interval_factor
        self.confidence_interval_factor_mask_min = confidence_interval_factor_mask_min
        self.confidence_interval_factor_mask_max = confidence_interval_factor_mask_max

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new AP operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return ER(positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                  show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask, confidence_interval_factor=0,
                  confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                  confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max)

    def op(self, cdi: CDI):
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True
            cdi = ERProj(positivity=self.positivity) * \
                  FourierApplyAmplitude(calc_llk=calc_llk, zero_mask=self.zero_mask,
                                        confidence_interval_factor=self.confidence_interval_factor,
                                        confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                                        confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max) \
                  * cdi

            if calc_llk:
                cdi.update_history(mode='llk', algorithm='ER', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='ER')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(fig_num=self.fig_num) * cdi
            cdi.cycle += 1
        return cdi


class CFProj(CUOperatorCDI):
    """
    Charge Flipping.
    """

    def __init__(self, positivity=False):
        super(CFProj, self).__init__()
        self.positivity = positivity

    def op(self, cdi: CDI):
        if self.positivity:
            self.processing_unit.cu_cf_real(cdi._cu_obj, cdi._cu_support)
        else:
            self.processing_unit.cu_cf(cdi._cu_obj, cdi._cu_support)
        return cdi


class CF(CUOperatorCDI):
    """
    Charge flipping cycle
    """

    def __init__(self, positivity=False, calc_llk=False, nb_cycle=1, show_cdi=False, fig_num=-1, zero_mask=False,
                 confidence_interval_factor=0, confidence_interval_factor_mask_min=0.5,
                 confidence_interval_factor_mask_max=1.2):
        """

        :param positivity: apply a positivity restraint
        :param calc_llk: if True, calculate llk while in Fourier space. If a positive integer is given, llk will be
                         calculated every calc_llk cycle
        :param nb_cycle: the number of cycles to perform
        :param show_cdi: if a positive integer number N, the object & probe will be displayed every N cycle.
                               By default 0 (no plot)
        :param fig_num: the number of the figure to plot the object intensity, as for ShowCDI()
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
                          complex amplitude is kept with an optional scale factor.
        :param confidence_interval_factor: a relaxation factor, with the projection of calculated amplitude being done
           towards the limit of the poisson confidence interval. A value of 1
           corresponds to a 50% confidence interval, a value of 0 corresponds to a
           strict observed amplitude projection. [EXPERIMENTAL]
        :param confidence_interval_factor_mask_min, confidence_interval_factor_mask_max:
            For masked pixels where a value has been estimated (e.g. with InterpIobsMask()),
            a confidence interval can be given as a factor to be applied to the interpolated
            observed intensity. This corresponds to values stored between -1e19 and -1e38. [EXPERIMENTAL]
        """
        super(CF, self).__init__()
        self.positivity = positivity
        self.calc_llk = calc_llk
        self.nb_cycle = nb_cycle
        self.show_cdi = show_cdi
        self.fig_num = fig_num
        self.zero_mask = zero_mask
        self.confidence_interval_factor = confidence_interval_factor
        self.confidence_interval_factor_mask_min = confidence_interval_factor_mask_min
        self.confidence_interval_factor_mask_max = confidence_interval_factor_mask_max

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new CF operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return CF(positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                  show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask,
                  confidence_interval_factor=self.confidence_interval_factor,
                  confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                  confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max)

    def op(self, cdi: CDI):
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True
            cdi = CFProj(positivity=self.positivity) * \
                  FourierApplyAmplitude(calc_llk=calc_llk, zero_mask=self.zero_mask,
                                        confidence_interval_factor=self.confidence_interval_factor,
                                        confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                                        confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max) \
                  * cdi

            if calc_llk:
                cdi.update_history(mode='llk', algorithm='CF', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='CF')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(fig_num=self.fig_num) * cdi
            cdi.cycle += 1
        return cdi


class HIOProj(CUOperatorCDI):
    """
    Hybrid Input-Output.
    """

    def __init__(self, beta=0.9, positivity=False):
        super(HIOProj, self).__init__()
        self.beta = np.float32(beta)
        self.positivity = positivity

    def op(self, cdi):
        if self.positivity:
            self.processing_unit.cu_hio_real(cdi._cu_obj, cdi._cu_obj_previous, cdi._cu_support, self.beta)
        else:
            self.processing_unit.cu_hio(cdi._cu_obj, cdi._cu_obj_previous, cdi._cu_support, self.beta)
        return cdi


class HIO(CUOperatorCDI):
    """
    Hybrid Input-Output reduction cycle
    """

    def __init__(self, beta=0.9, positivity=False, calc_llk=False, nb_cycle=1, show_cdi=False, fig_num=-1,
                 zero_mask=False, confidence_interval_factor=0, confidence_interval_factor_mask_min=0.5,
                 confidence_interval_factor_mask_max=1.2):
        """

        :param positivity: apply a positivity restraint
        :param calc_llk: if True, calculate llk while in Fourier space. If a positive integer is given, llk will be
                         calculated every calc_llk cycle
        :param nb_cycle: the number of cycles to perform
        :param show_cdi: if a positive integer number N, the object & probe will be displayed every N cycle.
                               By default 0 (no plot)
        :param fig_num: the number of the figure to plot the object intensity, as for ShowCDI()
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
                          complex amplitude is kept with an optional scale factor.
        :param confidence_interval_factor: a relaxation factor, with the projection of calculated amplitude being done
           towards the limit of the poisson confidence interval. A value of 1
           corresponds to a 50% confidence interval, a value of 0 corresponds to a
           strict observed amplitude projection. [EXPERIMENTAL]
        :param confidence_interval_factor_mask_min, confidence_interval_factor_mask_max:
            For masked pixels where a value has been estimated (e.g. with InterpIobsMask()),
            a confidence interval can be given as a factor to be applied to the interpolated
            observed intensity. This corresponds to values stored between -1e19 and -1e38. [EXPERIMENTAL]
        """
        super(HIO, self).__init__()
        self.beta = beta
        self.positivity = positivity
        self.calc_llk = calc_llk
        self.nb_cycle = nb_cycle
        self.show_cdi = show_cdi
        self.fig_num = fig_num
        self.zero_mask = zero_mask
        self.confidence_interval_factor = confidence_interval_factor
        self.confidence_interval_factor_mask_min = confidence_interval_factor_mask_min
        self.confidence_interval_factor_mask_max = confidence_interval_factor_mask_max

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new HIO operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return HIO(beta=self.beta, positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                   show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask,
                   confidence_interval_factor=self.confidence_interval_factor,
                   confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                   confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max)

    def op(self, cdi: CDI):
        cdi = CopyToPrevious() * cdi
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True
            cdi = HIOProj(beta=self.beta, positivity=self.positivity) * \
                  FourierApplyAmplitude(calc_llk=calc_llk, zero_mask=self.zero_mask,
                                        confidence_interval_factor=self.confidence_interval_factor,
                                        confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                                        confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max) \
                  * cdi

            if calc_llk:
                cdi.update_history(mode='llk', algorithm='HIO', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='HIO')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(fig_num=self.fig_num) * cdi
            cdi.cycle += 1
        return cdi


class RAARProj(CUOperatorCDI):
    """
    RAAR.
    """

    def __init__(self, beta=0.9, positivity=False):
        super(RAARProj, self).__init__()
        self.beta = np.float32(beta)
        self.positivity = positivity

    def op(self, cdi):
        if self.positivity:
            self.processing_unit.cu_raar_real(cdi._cu_obj, cdi._cu_obj_previous, cdi._cu_support, self.beta)
        else:
            self.processing_unit.cu_raar(cdi._cu_obj, cdi._cu_obj_previous, cdi._cu_support, self.beta)
        return cdi


class RAAR(CUOperatorCDI):
    """
    RAAR cycle
    """

    def __init__(self, beta=0.9, positivity=False, calc_llk=False, nb_cycle=1, show_cdi=False, fig_num=-1,
                 zero_mask=False, confidence_interval_factor=0, confidence_interval_factor_mask_min=0.5,
                 confidence_interval_factor_mask_max=1.2):
        """

        :param positivity: apply a positivity restraint
        :param calc_llk: if True, calculate llk while in Fourier space. If a positive integer is given, llk will be
                         calculated every calc_llk cycle
        :param nb_cycle: the number of cycles to perform
        :param show_cdi: if a positive integer number N, the object & probe will be displayed every N cycle.
                               By default 0 (no plot)
        :param fig_num: the number of the figure to plot the object intensity, as for ShowCDI()
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
                          complex amplitude is kept with an optional scale factor.
        :param confidence_interval_factor: a relaxation factor, with the projection of calculated amplitude being done
           towards the limit of the poisson confidence interval. A value of 1
           corresponds to a 50% confidence interval, a value of 0 corresponds to a
           strict observed amplitude projection. [EXPERIMENTAL]
        :param confidence_interval_factor_mask_min, confidence_interval_factor_mask_max:
            For masked pixels where a value has been estimated (e.g. with InterpIobsMask()),
            a confidence interval can be given as a factor to be applied to the interpolated
            observed intensity. This corresponds to values stored between -1e19 and -1e38. [EXPERIMENTAL]
        """
        super(RAAR, self).__init__()
        self.beta = beta
        self.positivity = positivity
        self.calc_llk = calc_llk
        self.nb_cycle = nb_cycle
        self.show_cdi = show_cdi
        self.fig_num = fig_num
        self.zero_mask = zero_mask
        self.confidence_interval_factor = confidence_interval_factor
        self.confidence_interval_factor_mask_min = confidence_interval_factor_mask_min
        self.confidence_interval_factor_mask_max = confidence_interval_factor_mask_max

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new RAAR operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return RAAR(beta=self.beta, positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                    show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask,
                    confidence_interval_factor=self.confidence_interval_factor,
                    confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                    confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max)

    def op(self, cdi: CDI):
        cdi = CopyToPrevious() * cdi
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True

            cdi = RAARProj(self.beta, positivity=self.positivity) * \
                  FourierApplyAmplitude(calc_llk=calc_llk, zero_mask=self.zero_mask,
                                        confidence_interval_factor=self.confidence_interval_factor,
                                        confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                                        confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max) \
                  * cdi

            if calc_llk:
                cdi.update_history(mode='llk', algorithm='RAAR', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='RAAR')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(fig_num=self.fig_num) * cdi
            cdi.cycle += 1
        return cdi


class GPS(CUOperatorCDI):
    """
    GPS cycle, according to Pham et al [2019]
    """

    def __init__(self, inertia=0.05, t=1.0, s=0.9, sigma_f=0, sigma_o=0, positivity=False,
                 calc_llk=False, nb_cycle=1, show_cdi=False, fig_num=-1, zero_mask=False,
                 confidence_interval_factor=0, confidence_interval_factor_mask_min=0.5,
                 confidence_interval_factor_mask_max=1.2):
        """
        :param inertia: inertia parameter (sigma in original Pham2019 article)
        :param t: t parameter
        :param s: s parameter
        :param sigma_f: Fourier-space smoothing kernel width, in Fourier-space pixel units
        :param sigma_o: object-space smoothing kernel width, in object-space pixel units
        :param positivity: apply a positivity restraint
        :param calc_llk: if True, calculate llk while in Fourier space. If a positive integer is given, llk will be
                         calculated every calc_llk cycle
        :param nb_cycle: the number of cycles to perform
        :param show_cdi: if a positive integer number N, the object & probe will be displayed every N cycle.
                               By default 0 (no plot)
        :param fig_num: the number of the figure to plot the object intensity, as for ShowCDI()
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
                          complex amplitude is kept with an optional scale factor.
        :param confidence_interval_factor: a relaxation factor, with the projection of calculated amplitude being done
           towards the limit of the poisson confidence interval. A value of 1
           corresponds to a 50% confidence interval, a value of 0 corresponds to a
           strict observed amplitude projection. [EXPERIMENTAL]
        :param confidence_interval_factor_mask_min, confidence_interval_factor_mask_max:
            For masked pixels where a value has been estimated (e.g. with InterpIobsMask()),
            a confidence interval can be given as a factor to be applied to the interpolated
            observed intensity. This corresponds to values stored between -1e19 and -1e38. [EXPERIMENTAL]
        """
        super(GPS, self).__init__()
        self.inertia = np.float32(inertia)
        self.t = np.float32(t)
        self.s = np.float32(s)
        self.sigma_f = np.float32(sigma_f)
        self.sigma_o = np.float32(sigma_o)
        self.positivity = positivity
        self.calc_llk = calc_llk
        self.nb_cycle = nb_cycle
        self.show_cdi = show_cdi
        self.fig_num = fig_num
        self.zero_mask = zero_mask
        self.confidence_interval_factor = confidence_interval_factor
        self.confidence_interval_factor_mask_min = confidence_interval_factor_mask_min
        self.confidence_interval_factor_mask_max = confidence_interval_factor_mask_max

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new GPS operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return GPS(inertia=self.inertia, t=self.t, s=self.s, sigma_f=self.sigma_f, sigma_o=self.sigma_o,
                   positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                   show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask,
                   confidence_interval_factor=self.confidence_interval_factor,
                   confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                   confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max)

    def op(self, cdi: CDI):
        s = 1.0 / np.sqrt(cdi._cu_iobs.size)  # FFT scaling np.float32(1)

        epsilon = np.float32(self.inertia / (self.inertia + self.t))

        ny, nx = np.int32(cdi._obj.shape[-2]), np.int32(cdi._obj.shape[-1])
        if cdi._obj.ndim == 3:
            nz = np.int32(cdi._obj.shape[0])
        else:
            nz = np.int32(1)

        # Make sure we have tmp copy arrays available
        if has_attr_not_none(cdi, '_cu_z') is False:
            cdi._cu_z = cua.empty_like(cdi._cu_obj)
        elif cdi._cu_z.shape != cdi._cu_obj.shape:
            cdi._cu_z = cua.empty_like(cdi._cu_obj)

        if has_attr_not_none(cdi, '_cu_y') is False:
            cdi._cu_y = cua.empty_like(cdi._cu_obj)
        elif cdi._cu_y.shape != cdi._cu_obj.shape:
            cdi._cu_y = cua.empty_like(cdi._cu_obj)

        # We start in Fourier space (obj = z_0)
        cdi = FT(scale=True) * cdi

        # z_0 = FT(obj)
        cu_drv.memcpy_dtod(dest=cdi._cu_z.gpudata, src=cdi._cu_obj.gpudata, size=cdi._cu_obj.nbytes)

        # Start with obj = y_0 = 0
        cdi._cu_obj.fill(np.complex64(0))

        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True

            # keep y copy
            cu_drv.memcpy_dtod(dest=cdi._cu_y.gpudata, src=cdi._cu_obj.gpudata, size=cdi._cu_obj.nbytes)

            cdi = FT(scale=False) * cdi

            # ^z = z_k - t F(y_k)
            self.processing_unit.cu_gps1(cdi._cu_obj, cdi._cu_z, self.t * s, self.sigma_o, nx, ny, nz)

            cdi = ApplyAmplitude(calc_llk=calc_llk, zero_mask=self.zero_mask,
                                 confidence_interval_factor=self.confidence_interval_factor,
                                 confidence_interval_factor_mask_min=self.confidence_interval_factor_mask_min,
                                 confidence_interval_factor_mask_max=self.confidence_interval_factor_mask_max) * cdi

            # obj = z_k+1 = (1 - epsilon) * sqrt(iobs) * exp(i * arg(^z)) + epsilon * z_k
            self.processing_unit.cu_gps2(cdi._cu_obj, cdi._cu_z, epsilon)

            if calc_llk:
                cdi.update_history(mode='llk', algorithm='GPS', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='GPS')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = IFT(scale=True) * cdi
                    cdi = ShowCDI(fig_num=self.fig_num) * cdi
                    cdi = FT(scale=True) * cdi
            cdi.cycle += 1

            if ic < self.nb_cycle - 1:
                # obj = 2 * z_k+1 - z_k  & store z_k+1 in z
                self.processing_unit.cu_gps3(cdi._cu_obj, cdi._cu_z)

                cdi = IFT(scale=False) * cdi

                # obj = ^y = proj_support[y_k + s * obj] * G_sigma_f
                self.processing_unit.cu_gps4(cdi._cu_obj, cdi._cu_y, cdi._cu_support, self.s * s, self.sigma_f,
                                             self.positivity, nx, ny, nz)
            else:
                self.processing_unit.cu_scale(cdi._cu_obj, s)

        # Free memory
        cdi._cu_y.gpudata.free()
        cdi._cu_z.gpudata.free()
        del cdi._cu_y, cdi._cu_z

        # Back to object space
        cdi = IFT(scale=False) * cdi

        return cdi


class ML(CUOperatorCDI):
    """
    Maximum likelihood conjugate gradient minimization
    """

    def __init__(self, reg_fac=1e2, nb_cycle=1, calc_llk=False, show_cdi=False, fig_num=-1):
        """

        :param reg_fac:
        :param nb_cycle: the number of cycles to perform
        :param calc_llk: if True, calculate llk while in Fourier space. If a positive integer is given, llk will be
                         calculated every calc_llk cycle
        :param show_cdi: if a positive integer number N, the object & probe will be displayed every N cycle.
                               By default 0 (no plot)
        :param fig_num: the number of the figure to plot the object intensity, as for ShowCDI()
        """
        super(ML, self).__init__()
        self.need_init = True
        self.reg_fac = reg_fac
        self.nb_cycle = nb_cycle
        self.calc_llk = calc_llk
        self.show_cdi = show_cdi
        self.fig_num = fig_num

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new ML operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return ML(reg_fac=self.reg_fac, nb_cycle=self.nb_cycle * n, calc_llk=self.calc_llk, show_cdi=self.show_cdi,
                  fig_num=self.fig_num)

    def op(self, cdi: CDI):
        if self.need_init is False:
            if (has_attr_not_none(cdi, '_cu_obj_dir') is False) \
                    or (has_attr_not_none(cdi, '_cu_dpsi') is False) \
                    or (has_attr_not_none(cdi, '_cu_obj_grad') is False) \
                    or (has_attr_not_none(cdi, '_cu_obj_grad_last') is False) \
                    or (has_attr_not_none(cdi, 'llk_support_reg_fac') is False):
                self.need_init = True

        if self.need_init:
            # Take into account support in regularization
            N = cdi._obj.size
            # Total number of photons
            Nph = cdi.iobs_sum
            cdi.llk_support_reg_fac = np.float32(self.reg_fac / (8 * N / Nph))
            # if cdi.llk_support_reg_fac > 0:
            #    print("Regularization factor for support:", cdi.llk_support_reg_fac)

            cdi._cu_obj_dir = cua.empty(cdi._obj.shape, np.complex64)
            cdi._cu_psi = cua.empty(cdi._obj.shape, np.complex64)
            # cdi._cu_dpsi = cua.empty(cdi._obj.shape, np.complex64)
            cdi._cu_obj_grad = cua.empty(cdi._obj.shape, np.complex64)
            cdi._cu_obj_gradlast = cua.empty(cdi._obj.shape, np.complex64)
            self.need_init = False

        ny, nx = np.int32(cdi._obj.shape[-2]), np.int32(cdi._obj.shape[-1])
        if cdi._obj.ndim == 3:
            nz = np.int32(cdi._obj.shape[0])
        else:
            nz = np.int32(1)

        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True

            cu_drv.memcpy_dtod(dest=cdi._cu_psi.gpudata, src=cdi._cu_obj.gpudata, size=cdi._cu_psi.nbytes)

            plan = self.processing_unit.cu_fft_get_plan(cdi._cu_psi, batch=False)
            cu_fft.fft(cdi._cu_psi, cdi._cu_psi, plan)
            # TODO: avoid this separate normalization step !
            cdi._cu_psi *= 1 / np.sqrt(cdi.iobs.size)

            if calc_llk:
                cdi._cu_psi, cdi._cu_obj = cdi._cu_obj, cdi._cu_psi
                cdi._is_in_object_space = False
                cdi = LLK() * cdi
                cdi._cu_psi, cdi._cu_obj = cdi._cu_obj, cdi._cu_psi
                cdi._is_in_object_space = True

            # This calculates the conjugate of [(1 - iobs/icalc) * psi]
            self.processing_unit.cu_ml_poisson_psi_gradient(cdi._cu_psi, cdi._cu_obj_grad, cdi._cu_iobs, nx, ny, nz)

            plan = self.processing_unit.cu_fft_get_plan(cdi._cu_obj_grad, batch=False)
            cu_fft.fft(cdi._cu_obj_grad, cdi._cu_obj_grad, plan)
            # TODO: avoid this separate normalization step
            cdi._cu_obj_grad *= 1 / np.sqrt(cdi.iobs.size)

            if cdi.llk_support_reg_fac > 0:
                self.processing_unit.cu_ml_poisson_reg_support_gradient(cdi._cu_obj, cdi._cu_obj_grad, cdi._cu_support,
                                                                        cdi.llk_support_reg_fac)

            if ic == 0:
                beta = np.float32(0)
                cu_drv.memcpy_dtod(dest=cdi._cu_obj_dir.gpudata, src=cdi._cu_obj_grad.gpudata,
                                   size=cdi._cu_obj_grad.nbytes)
            else:
                # Polak-Ribière CG coefficient
                tmp = self.processing_unit.cu_cg_polak_ribiere_red(cdi._cu_obj_grad, cdi._cu_obj_gradlast).get()
                if False:
                    g1 = cdi._cu_obj_grad.get()
                    g0 = cdi._cu_obj_gradlast.get()
                    A, B = (g1.real * (g1.real - g0.real) + g1.imag * (g1.imag - g0.imag)).sum(), (
                            g0.real * g0.real + g0.imag * g0.imag).sum()
                    cpubeta = A / B
                    print("betaPR: (GPU)=%8.4e  , (CPU)=%8.4e [%8.4e/%8.4e], dot(g0.g1)=%8e [%8e]" %
                          (tmp.real / tmp.imag, cpubeta, A, B, (g0 * g1).sum().real, (abs(g0) ** 2).sum().real))
                # Reset direction if beta<0 => beta=0
                beta = np.float32(max(0, tmp.real / max(1e-20, tmp.imag)))

                self.processing_unit.cu_ml_poisson_cg_linear(beta, cdi._cu_obj_dir, np.float32(-1), cdi._cu_obj_grad)

            # For next cycle
            cdi._cu_obj_grad, cdi._cu_obj_gradlast = cdi._cu_obj_gradlast, cdi._cu_obj_grad

            # Avoid using two memory allocations for obj_grad and dpsi
            cdi._cu_dpsi = cdi._cu_obj_grad

            cu_drv.memcpy_dtod(dest=cdi._cu_dpsi.gpudata, src=cdi._cu_obj_dir.gpudata, size=cdi._cu_dpsi.nbytes)

            plan = self.processing_unit.cu_fft_get_plan(cdi._cu_dpsi, batch=False)
            cu_fft.fft(cdi._cu_dpsi, cdi._cu_dpsi, plan)
            # TODO: avoid this separate normalization step
            cdi._cu_dpsi *= 1 / np.sqrt(cdi.iobs.size)

            if cdi.llk_support_reg_fac > 0:
                tmp = self.processing_unit.cdi_ml_poisson_gamma_support_red(cdi._cu_iobs, cdi._cu_psi, cdi._cu_dpsi,
                                                                            cdi._cu_obj, cdi._cu_obj_dir,
                                                                            cdi._cu_support,
                                                                            cdi.llk_support_reg_fac).get()
                gamma_n, gamma_d = tmp.real, tmp.imag
                gamma = np.float32(gamma_n / gamma_d)
            else:
                tmp = self.processing_unit.cdi_ml_poisson_gamma_red(cdi._cu_iobs, cdi._cu_psi, cdi._cu_dpsi).get()
                gamma_n, gamma_d = tmp.real, tmp.imag
                gamma = np.float32(gamma_n / gamma_d)

            self.processing_unit.cu_ml_poisson_cg_linear(np.float32(1), cdi._cu_obj, gamma, cdi._cu_obj_dir)

            if calc_llk:
                cdi.update_history(mode='llk', algorithm='ML', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='ML')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(fig_num=self.fig_num) * cdi

            cdi.cycle += 1

        return cdi


class SupportUpdate(CUOperatorCDI):
    """
    Update the support
    """

    def __init__(self, threshold_relative=0.2, smooth_width=3, force_shrink=False, method='rms',
                 post_expand=None, verbose=False, update_border_n=0):
        """
        Update support.

        Args:
            threshold_relative: must be between 0 and 1. Only points with object amplitude above a value equal to 
                relative_threshold * reference_value are kept in the support.
                reference_value can either:
                - use the fact that when converged, the square norm of the object is equal to the number of 
                recorded photons (normalized Fourier Transform). Then:
                  reference_value = sqrt((abs(obj)**2).sum()/nb_points_support)
                - or use threshold_percentile (see below, very slow, deprecated)
            smooth_width: smooth the object amplitude using a gaussian of this width before calculating new support
                          If this is a scalar, the smooth width is fixed to this value.
                          If this is a 3-value tuple (or list or array), i.e. 'smooth_width=2,0.5,600', the smooth width
                          will vary with the number of cycles recorded in the CDI object (as cdi.cycle), varying
                          exponentially from the first to the second value over the number of cycles specified by the
                          last value.
                          With 'smooth_width=a,b,nb':
                               smooth_width = a * exp(-cdi.cycle/nb*log(b/a)) if cdi.cycle < nb
                               smooth_width = b if cdi.cycle >= nb
            force_shrink: if True, the support can only shrink
            method: either 'max' or 'average' or 'rms' (default), the threshold will be relative to either the maximum
                    amplitude in the object, or the average or root-mean-square amplitude (computed inside support)
            post_expand=1: after the new support has been calculated, it can be processed using the SupportExpand
                           operator, either one or multiple times, in order to 'clean' the support:
                           - 'post_expand=1' will expand the support by 1 pixel
                           - 'post_expand=-1' will shrink the support by 1 pixel
                           - 'post_expand=(-1,1)' will shrink and then expand the support by 1 pixel
                           - 'post_expand=(-2,3)' will shrink and then expand the support by respectively 2 and 3 pixels
            verbose: if True, print number of points in support
            update_border_n: if > 0, the only pixels affected by the support updated lie within +/- N pixels around the
                             outer border of the support.
        Returns:
            Nothing. self._support is updated
        """
        super(SupportUpdate, self).__init__()
        self.smooth_width = smooth_width
        self.threshold_relative = threshold_relative
        self.force_shrink = np.bool(force_shrink)
        self.method = method
        self.verbose = verbose
        if isinstance(post_expand, int) or isinstance(post_expand, np.integer):
            self.post_expand = (post_expand,)
        else:
            self.post_expand = post_expand
        self.update_border_n = update_border_n

    def op(self, cdi):
        if np.isscalar(self.smooth_width):
            smooth_width = self.smooth_width
        else:
            a, b, nb = self.smooth_width
            i = cdi.cycle
            if i < nb:
                smooth_width = a * np.exp(-i / nb * np.log(a / b))
            else:
                smooth_width = b
        # Convolve the absolute value of the object
        cdi = ObjConvolve(sigma=smooth_width) * cdi

        # Get average amplitude and maximum intensity for the object in the support (unsmoothed)
        tmp = self.processing_unit.cu_average_max_red(cdi._cu_obj, cdi._cu_support).get()
        cdi._obj_max = np.sqrt(tmp.imag)

        # Actual threshold is computed on the convolved object
        if self.method == 'max':
            tmp = self.processing_unit.cu_rms_max_abs_red(cdi._cu_obj_abs, cdi._cu_support).get()
            thr = self.threshold_relative * np.float32(np.sqrt(tmp.imag))
        elif self.method == 'rms':
            tmp = self.processing_unit.cu_rms_max_abs_red(cdi._cu_obj_abs, cdi._cu_support).get()
            thr = self.threshold_relative * np.sqrt(np.float32(tmp.real / cdi.nb_point_support))
        else:
            tmp = self.processing_unit.cu_average_max_abs_red(cdi._cu_obj_abs, cdi._cu_support).get()
            thr = self.threshold_relative * np.float32(tmp.real / cdi.nb_point_support)

        # Update support and compute the new number of points in the support

        if self.update_border_n > 0:
            # First compute the border of the support
            nx, ny = np.int32(cdi._obj.shape[-1]), np.int32(cdi._obj.shape[-2])
            if cdi._obj.ndim == 3:
                nz = np.int32(cdi._obj.shape[0])
            else:
                nz = np.int32(1)

            m1 = np.int8(2)  # Bitwise mask for expanded support
            m2 = np.int8(4)  # Bitwise mask for shrunk support

            # Convolution kernel width cannot exceed 7, so loop for larger convolutions
            for i in range(0, self.update_border_n, 7):
                # Expanded support
                m0 = m1 if i > 0 else np.int8(1)
                n = np.int32(self.update_border_n - i) if (self.update_border_n - i) <= 7 else np.int32(7)

                self.processing_unit.binary_window_convol_16x_mask(cdi._cu_support, n, nx, ny, nz, m0, m1,
                                                                   block=(16, 1, 1), grid=(1, int(ny), int(nz)))
                self.processing_unit.binary_window_convol_16y_mask(cdi._cu_support, n, nx, ny, nz, m1, m1,
                                                                   block=(1, 16, 1), grid=(int(nx), 1, int(nz)))
                if cdi._obj.ndim == 3:
                    self.processing_unit.binary_window_convol_16z_mask(cdi._cu_support, n, nx, ny, nz, m1, m1,
                                                                       block=(1, 1, 16), grid=(int(nx), int(ny), 1))

                # Shrunk support
                m0 = m2 if i > 0 else np.int8(1)
                self.processing_unit.binary_window_convol_16x_mask(cdi._cu_support, -n, nx, ny, nz, m0, m2,
                                                                   block=(16, 1, 1), grid=(1, int(ny), int(nz)))
                self.processing_unit.binary_window_convol_16y_mask(cdi._cu_support, -n, nx, ny, nz, m2, m2,
                                                                   block=(1, 16, 1), grid=(int(nx), 1, int(nz)))
                if cdi._obj.ndim == 3:
                    self.processing_unit.binary_window_convol_16z_mask(cdi._cu_support, -n, nx, ny, nz, m2, m2,
                                                                       block=(1, 1, 16), grid=(int(nx), int(ny), 1))

            nb = int(self.processing_unit.cu_support_update_border(cdi._cu_obj_abs, cdi._cu_support, thr,
                                                                   self.force_shrink).get())
        else:
            nb = int(self.processing_unit.cu_support_update(cdi._cu_obj_abs, cdi._cu_support, thr,
                                                            self.force_shrink).get())

        if self.post_expand is not None:
            for n in self.post_expand:
                cdi = SupportExpand(n=n, update_nb_points_support=False) * cdi
            nb = int(self.processing_unit.cu_nb_point_support(cdi._cu_support).get())

        if self.verbose:
            print("Nb points in support: %d (%6.3f%%), threshold=%8f (%6.3f), nb photons=%10e"
                  % (nb, nb / cdi._obj.size * 100, thr, self.threshold_relative, tmp.real))
        cdi._cu_obj_abs = None  # Free memory
        cdi.nb_point_support = nb
        cdi.history.insert(cdi.cycle, support_size=cdi.nb_point_support, obj_max=cdi._obj_max,
                           obj_average=np.sqrt(cdi.nb_photons_calc / cdi.nb_point_support),
                           support_update_threshold=thr)
        return cdi


class ScaleObj(CUOperatorCDI):
    """
    Scale the object according to the observed intensity. The scaling is either made against the amplitudes,
    the intensities, or the weighted intensities.
    This is only useful if a mask is used - the scale factor effectively only applies to masked intensities.
    :param method: 'I' (intensities), 'F' (amplitudes), 'wI' (weighted intensities), 'P' Poisson
    :return: nothing. The object is scaled to best match the intensities.
    """

    def __init__(self, method='I', verbose=False):
        """
        :param method: 'I' (intensities), 'F' (amplitudes), 'wI' (weighted intensities), 'P' (Poisson)
        :param verbose: if True, print the scale factor
        """
        super(ScaleObj, self).__init__()
        self.method = method
        self.verbose = verbose

    def op(self, cdi):
        cdi = FT(scale=True) * cdi
        if self.method.lower() == 'f':
            # Scale the object to match Fourier amplitudes
            tmp = self.processing_unit.cu_scale_amplitude(cdi._cu_iobs, cdi._cu_obj).get()
            scale = tmp.real / tmp.imag
            if False:
                tmpcalc = np.abs(cdi.get_obj()) * (cdi.iobs >= 0)
                tmpobs = np.sqrt(np.abs(cdi.iobs))
                scale_cpu = (tmpcalc * tmpobs).sum() / (tmpcalc ** 2).sum()
                print("Scaling F: scale_cpu= %8.4f, scale_gpu= %8.4f" % (scale_cpu, scale))
        elif self.method.lower() == 'i':
            # Scale object to match Fourier intensities
            tmp = self.processing_unit.cu_scale_intensity(cdi._cu_iobs, cdi._cu_obj).get()
            scale = np.sqrt(tmp.real / tmp.imag)
            if False:
                tmpcalc = np.abs(cdi.get_obj()) ** 2 * (cdi.iobs >= 0)
                scale_cpu = np.sqrt((tmpcalc * cdi.iobs).sum() / (tmpcalc ** 2).sum())
                print("Scaling I: scale_cpu= %8.4f, scale_gpu= %8.4f" % (scale_cpu, scale))
        elif self.method.lower() == 'p':
            # Scale object to match intensities with Poisson noise
            tmp = self.processing_unit.cu_scale_intensity_poisson(cdi._cu_iobs, cdi._cu_obj).get()
            scale = tmp.real / tmp.imag
            if False:
                tmpcalc = (np.abs(cdi.get_obj()) ** 2 * (cdi.iobs >= 0)).sum()
                tmpobs = (cdi.iobs * (cdi.iobs >= 0)).sum()
                scale_cpu = tmpobs / tmpcalc
                print("Scaling P: scale_cpu= %8.4f, scale_gpu= %8.4f" % (scale_cpu, scale))
        else:
            # Scale object to match weighted intensities
            # Weight: 1 for null intensities, zero for masked pixels
            tmp = self.processing_unit.cu_scale_weighted_intensity(cdi._cu_iobs, cdi._cu_obj).get()
            scale = np.sqrt(tmp.real / tmp.imag)
            if False:
                w = (1 / (np.abs(cdi.iobs) + 1e-6) * (cdi.iobs > 1e-6) + (cdi.iobs <= 1e-6)) * (cdi.iobs >= 0)
                tmpcalc = np.abs(cdi.get_obj()) ** 2
                scale_cpu = np.sqrt((w * tmpcalc * cdi.iobs).sum() / (w * tmpcalc ** 2).sum())
                print("Scaling W: scale_cpu= %8.4f, scale_gpu= %8.4f" % (scale_cpu, scale))
        cdi = IFT(scale=scale / np.sqrt(cdi._obj.size)) * cdi
        if self.verbose:
            print("Scaled object by: %f [%s]" % (scale, self.method))
        return cdi


class LLK(CUOperatorCDI):
    """
    Log-likelihood reduction kernel. This is a reduction operator - it will write llk as an argument in the cdi object.
    If it is applied to a CDI instance in object space, a FT() and IFT() will be applied  to perform the calculation
    in diffraction space.
    This collect log-likelihood for Poisson, Gaussian and Euclidian noise models, and also computes the
    total calculated intensity (including in masked pixels).
    """

    def __init__(self, scale=1.0):
        """

        :param scale: the scale factor to be applied to the calculated amplitude before evaluating the
                      log-likelihood. The calculated amplitudes are left unmodified.
        """
        super(LLK, self).__init__()
        self.scale = np.float32(scale ** 2)

    def op(self, cdi):
        need_ft = cdi.in_object_space()

        if need_ft:
            cdi = FT() * cdi

        if cdi._k_psf is None:
            llk = self.processing_unit.cu_llk_red(cdi._cu_iobs, cdi._cu_obj, self.scale).get()
        else:
            # FFT-based convolution, using half-Hermitian kernel and real->complex64 FFT
            cu_icalc = cua.empty_like(cdi._cu_iobs)  # float32
            cu_icalc_f = cua.empty_like(cdi._cu_k_psf_f)  # Complex64, half-Hermitian array

            self.processing_unit.cu_square_modulus(cu_icalc, cdi._cu_obj)
            cu_fft.fft(cu_icalc, cu_icalc_f, cdi._cu_plan_r2c, scale=False)
            self.processing_unit.cu_mult_complex(cdi._cu_k_psf_f, cu_icalc_f)
            cu_fft.ifft(cu_icalc_f, cu_icalc, cdi._cu_plan_c2r, scale=False)
            llk = self.processing_unit.cu_llk_icalc_red(cdi._cu_iobs, cu_icalc, self.scale).get()

        cdi.llk_poisson = llk['a']
        cdi.llk_gaussian = llk['b']
        cdi.llk_euclidian = llk['c']
        cdi.nb_photons_calc = llk['d']
        cdi.llk_poisson_free = llk['e']
        cdi.llk_gaussian_free = llk['f']
        cdi.llk_euclidian_free = llk['g']

        if need_ft:
            cdi = IFT() * cdi

        return cdi


class LLKSupport(CUOperatorCDI):
    """
    Support log-likelihood reduction kernel. Can only be used when cdi instance is object space.
    This is a reduction operator - it will write llk_support as an argument in the cdi object, and return cdi.
    """

    def op(self, cdi):
        llk = float(self.processing_unit.cu_llk_reg_support_red(cdi._cu_obj, cdi._cu_support).get())
        cdi.llk_support = llk * cdi.llk_support_reg_fac
        return cdi


class DetwinSupport(CUOperatorCDI):
    """
    This operator can be used to halve the support (or restore the full support), in order to obtain an
    asymmetrical support function to favor one twin.
    """

    def __init__(self, restore=False, axis=0):
        """
        Constructor for the detwinning 
        :param restore: if True, the original support (stored in main memory) is copied back to the GPU
        :param axis: remove the half of the support along the given axis (default=0)
        """
        super(DetwinSupport, self).__init__()
        self.restore = restore
        self.axis = axis

    def op(self, cdi):
        if self.restore:
            cdi._cu_support = cua.to_gpu(cdi._support)
        else:
            # Get current support
            tmp = fftshift(cdi._cu_support.get())
            # Use center of mass to cut near middle
            c = center_of_mass(tmp)
            if self.axis == 0:
                tmp[int(round(c[0])):] = 0
            elif self.axis == 1 or tmp.ndim == 2:
                tmp[:, int(round(c[1])):] = 0
            else:
                tmp[:, :, int(round(c[2])):] = 0
            cdi._cu_support = cua.to_gpu(fftshift(tmp))
        return cdi


class DetwinHIO(CUOperatorCDI):
    """
    HIO cycles with a temporary halved support
    """

    # using __new__ works but prevents returning a simple 'DetwinHIO' translation using the __str__ function
    # def __new__(cls, detwin_axis=0, nb_cycle=10, beta=0.9, positivity=False):
    #    return DetwinSupport(restore=True) * HIO(beta=beta, positivity=positivity) ** nb_cycle \
    #           * DetwinSupport(axis=detwin_axis)
    def __init__(self, detwin_axis=0, nb_cycle=10, beta=0.9, positivity=False, zero_mask=False):
        """
        Constructor for the DetwinHIO operator
        :param detwin_axis: axis along which the detwinning will be performed. If None, a random axis is chosen
        :param nb_cycle: number of cycles to perform while using a halved support
        :param beta: the beta value for the HIO operator
        :param positivity: True or False
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
                          complex amplitude is kept with an optional scale factor.
        """
        super(DetwinHIO, self).__init__()
        self.detwin_axis = detwin_axis
        self.nb_cycle = nb_cycle
        self.beta = beta
        self.positivity = positivity
        self.zero_mask = zero_mask

    def op(self, cdi: CDI):
        # print('Detwinning with %d HIO cycles and a half-support' % self.nb_cycle)
        if self.detwin_axis is None:
            self.detwin_axis = randint(0, cdi.iobs.ndim)
        return DetwinSupport(restore=True) * HIO(beta=self.beta, positivity=self.positivity,
                                                 zero_mask=self.zero_mask) ** self.nb_cycle \
               * DetwinSupport(axis=self.detwin_axis) * cdi


class DetwinRAAR(CUOperatorCDI):
    """
    RAAR cycles with a temporary halved support
    """

    # using __new__ works but prevents returning a simple 'DetwinRAAR' translation using the __str__ function
    # def __new__(cls, detwin_axis=0, nb_cycle=10, beta=0.9, positivity=False):
    #    return DetwinSupport(restore=True) * RAAR(beta=beta, positivity=positivity) ** nb_cycle \
    #           * DetwinSupport(axis=detwin_axis)
    def __init__(self, detwin_axis=0, nb_cycle=10, beta=0.9, positivity=False, zero_mask=False):
        """
        Constructor for the DetwinRAAR operator
        :param detwin_axis: axis along which the detwinning will be performed. If None, a random axis is chosen
        :param nb_cycle: number of cycles to perform while using a halved support
        :param beta: the beta value for the HIO operator
        :param positivity: True or False
        :param zero_mask: if True, masked pixels (iobs<-1e19) are forced to zero, otherwise the calculated
                          complex amplitude is kept with an optional scale factor.
        """
        super(DetwinRAAR, self).__init__()
        self.detwin_axis = detwin_axis
        self.nb_cycle = nb_cycle
        self.beta = beta
        self.positivity = positivity
        self.zero_mask = zero_mask

    def op(self, cdi: CDI):
        # print('Detwinning with %d RAAR cycles and a half-support' % self.nb_cycle)
        if self.detwin_axis is None:
            self.detwin_axis = randint(0, cdi.iobs.ndim)
        return DetwinSupport(restore=True) * RAAR(beta=self.beta, positivity=self.positivity,
                                                  zero_mask=self.zero_mask) ** self.nb_cycle \
               * DetwinSupport(axis=self.detwin_axis) * cdi


class SupportExpand(CUOperatorCDI):
    """
    Expand (or shrink) the support using a binary window convolution.
    """

    def __init__(self, n=1, update_nb_points_support=True):
        """

        :param n: number of pixels to broaden the support, which will be done by a binary convolution with a
                  window size equal to 2*n+1 along all dimensions. if n is negative, the support is instead shrunk,
                  by performing the binary convolution and test on 1-support.
        :param update_nb_points_support: if True (the default), the number of points in the support will be calculated
                                         and stored in the object
        """
        super(SupportExpand, self).__init__()
        self.n = np.int32(n)
        self.update_nb_points_support = update_nb_points_support

    def op(self, cdi):
        if self.n == 0:
            return cdi
        nx, ny = np.int32(cdi._obj.shape[-1]), np.int32(cdi._obj.shape[-2])
        if cdi._obj.ndim == 3:
            nz = np.int32(cdi._obj.shape[0])
        else:
            nz = np.int32(1)
        self.processing_unit.binary_window_convol_16x(cdi._cu_support, self.n, nx, ny, nz,
                                                      block=(16, 1, 1), grid=(1, int(ny), int(nz)))
        self.processing_unit.binary_window_convol_16y(cdi._cu_support, self.n, nx, ny, nz,
                                                      block=(1, 16, 1), grid=(int(nx), 1, int(nz)))
        if cdi._obj.ndim == 3:
            self.processing_unit.binary_window_convol_16z(cdi._cu_support, self.n, nx, ny, nz,
                                                          block=(1, 1, 16), grid=(int(nx), int(ny), 1))
        if self.update_nb_points_support:
            cdi.nb_point_support = int(self.processing_unit.cu_nb_point_support(cdi._cu_support).get())
        return cdi


class ObjConvolve(CUOperatorCDI):
    """
    3D Gaussian convolution of the object, produces a new array with the convoluted amplitude of the object.
    """

    def __init__(self, sigma=1):
        super(ObjConvolve, self).__init__()
        self.sigma = np.float32(sigma)

    def op(self, cdi):
        cdi._cu_obj_abs = cua.zeros(cdi._cu_obj.shape, dtype=np.float32)
        nx, ny = np.int32(cdi._obj.shape[-1]), np.int32(cdi._obj.shape[-2])
        if cdi._obj.ndim == 3:
            nz = np.int32(cdi._obj.shape[0])
        else:
            nz = np.int32(1)
        self.processing_unit.abs_gauss_convol_16x(cdi._cu_obj, cdi._cu_obj_abs, self.sigma, nx, ny, nz,
                                                  block=(16, 1, 1), grid=(1, int(ny), int(nz)))
        self.processing_unit.gauss_convol_16y(cdi._cu_obj_abs, self.sigma, nx, ny, nz,
                                              block=(1, 16, 1), grid=(int(nx), 1, int(nz)))
        if cdi._obj.ndim == 3:
            self.processing_unit.gauss_convol_16z(cdi._cu_obj_abs, self.sigma, nx, ny, nz,
                                                  block=(1, 1, 16), grid=(int(nx), int(ny), 1))
        return cdi


class ShowCDI(ShowCDICPU):
    def __init__(self, fig_num=None, i=None):
        """

        :param fig_num: the matplotlib figure number. if None, a new figure will be created each time.
        :param i: if the object is 3D, display the ith plane (default: the center one)
        """
        super(ShowCDI, self).__init__(fig_num=fig_num, i=i)

    @staticmethod
    def get_icalc(cdi, i=None):
        if cdi.in_object_space():
            cdi = FT(scale=True) * cdi
            icalc = abs(cdi.get_obj()) ** 2
            cdi = IFT(scale=True) * cdi
        else:
            icalc = abs(cdi.get_obj()) ** 2
        if icalc.ndim == 3 and i is not None:
            return icalc[i]
        return icalc


class EstimatePSF(CUOperatorCDI):
    """
    Estimate the Point Spread Function. Applies to a CDI object in reciprocal space.
    The PSF is liklely due to partial coherence and thus related to the Mutual Coherence Function.
    """

    def __init__(self, nb_cycle=1, calc_llk=False):
        super(EstimatePSF, self).__init__()
        self.need_init = True
        self.nb_cycle = nb_cycle

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new EstimatePSF operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return EstimatePSF(nb_cycle=self.nb_cycle * n)

    def op(self, cdi: CDI):
        need_ft = cdi.in_object_space()

        if need_ft:
            cdi = FT(scale=True) * cdi

        # GPU-fft deconvolution
        # We keep 3 arrays in memory (not counting iobs and mask):
        # - keep the original FT of the object (psi)
        # - the current iteration of the point-spread kernel (cdi._cu_k_psf_f).
        #   This array is FT'd at each cycle, to avoid requiring another array.
        #   In the end it is stored as a real array for PSF convolution
        # - a temporary kernel to hold the FT if the calculated intensity (u)

        # Keep old values for the psf and LLK to see if something goes wrong
        k_psf_old = cdi._k_psf
        cdi = LLK() * cdi
        llk_poisson_old = cdi.get_llk()[0]
        if k_psf_old is not None:
            k_psf_f_old = cdi._cu_k_psf_f.get()

        # TODO: Get the iobs and icalc array slices on the GPU
        iobs = cdi.get_iobs(shift=True)
        icalc = fftshift(np.abs(cdi._cu_obj.get()) ** 2).astype(np.float32)
        iobs[iobs < 0] = icalc[iobs < 0]

        # Find adequate half-size for Richardson-Lucy array, use a power of 2 for simplicity
        n2 = 2 ** int(np.log2(min(cdi._obj.shape)) - 1)
        if n2 > 64:
            n2 = 64
        n3 = n2 // 2

        # TODO: start from previous psf kernel ?
        if cdi._obj.ndim == 2:
            ny, nx = cdi._obj.shape
            nz = 1
            icalc = icalc[ny // 2 - n2:ny // 2 + n2, nx // 2 - n2:nx // 2 + n2]
            iobs = iobs[ny // 2 - n2:ny // 2 + n2, nx // 2 - n2:nx // 2 + n2]
            ny, nx = iobs.shape
            psf = np.zeros((ny, nx), dtype=np.float32)
            psf[ny // 2 - n3:ny // 2 + n3, nx // 2 - n3:nx // 2 + n3] = 1
        else:
            nz, ny, nx = cdi._obj.shape
            icalc = icalc[nz // 2 - n2:nz // 2 + n2, ny // 2 - n2:ny // 2 + n2, nx // 2 - n2:nx // 2 + n2]
            iobs = iobs[nz // 2 - n2:nz // 2 + n2, ny // 2 - n2:ny // 2 + n2, nx // 2 - n2:nx // 2 + n2]
            nz, ny, nx = iobs.shape
            psf = np.zeros((nz, ny, nx), dtype=np.float32)
            psf[nz // 2 - n3:nz // 2 + n3, nz // 2 - n3:ny // 2 + n3, nx // 2 - n3:nx // 2 + n3] = 1

        nx = np.int32(nx)
        nx2 = np.int32(nx // 2 + 1)  # Size of the real->complex transform
        ny = np.int32(ny)
        nz = np.int32(nz)

        cu_icalc = cua.to_gpu(fftshift(icalc))
        if cdi._obj.ndim == 2:
            cu_icalc_m = cua.to_gpu(fftshift(icalc[::-1, ::-1]))
        else:
            cu_icalc_m = cua.to_gpu(fftshift(icalc[::-1, ::-1, ::-1]))

        cu_iobs = cua.to_gpu(fftshift(iobs))
        cu_psf = cua.to_gpu(fftshift(psf))
        del iobs, icalc, psf
        gc.collect()

        # real<->complex out-of-place FFT plans
        if cu_iobs.ndim == 2:
            shape = (ny, nx)
            shape2 = (ny, nx2)
        else:
            shape = (nz, ny, nx)
            shape2 = (nz, ny, nx2)

        cu_icalc_f = cua.empty(shape2, dtype=np.complex64)
        cu_icalc_m_f = cua.empty(shape2, dtype=np.complex64)

        # With cuFFT, the shape for r2c and c2r plans is the one of the *real* array
        pu = self.processing_unit
        # TODO: do we really need two plans ?
        plan_r2c = pu.cu_fft_get_plan(cu_icalc.shape, batch=False,
                                              dtype_src=np.float32, dtype_dest=np.complex64)
        plan_c2r = pu.cu_fft_get_plan(cu_icalc.shape, batch=False,
                                              dtype_src=np.complex64, dtype_dest=np.float32)

        # First compute FT of Iobs and Icalc
        cu_fft.fft(cu_icalc, cu_icalc_f, plan_r2c, scale=False)
        cu_fft.fft(cu_icalc_m, cu_icalc_m_f, plan_r2c, scale=False)

        cu_icalc.gpudata.free()
        cu_icalc_m.gpudata.free()
        del cu_icalc, cu_icalc_m

        cu_psi = cua.empty(shape, dtype=np.float32)
        cu_psi_f = cua.empty(shape2, dtype=np.complex64)

        for i in range(self.nb_cycle):
            # convolve(icalc, psf)
            cu_fft.fft(cu_psf, cu_psi_f, plan_r2c, scale=False)
            self.processing_unit.cu_mult_complex(cu_icalc_f, cu_psi_f)
            cu_fft.ifft(cu_psi_f, cu_psi, plan_c2r, scale=False)

            # iobs / convolve(icalc,psf)
            self.processing_unit.cu_div_float(cu_iobs, cu_psi)

            # convolve(iobs / convolve(icalc,psf), icalc_mirror)
            cu_fft.fft(cu_psi, cu_psi_f, plan_r2c, scale=False)
            self.processing_unit.cu_mult_complex(cu_icalc_m_f, cu_psi_f)
            # self.processing_unit.cu_mult_mirror(cu_icalc_f, cu_psi_f, nx, ny, nz)]
            cu_fft.ifft(cu_psi_f, cu_psi, plan_c2r, scale=False)

            # psf *= conv(iobs / conv(icalc,psf), icalc)
            # cu_psf *= cu_psi
            self.processing_unit.cu_mult(cu_psi, cu_psf)
        # print(cua.sum(cu_psf).get(), cua.min(cu_psf.real).get(), cla.max(cu_psf.real).get())

        # Free GPU memory
        cu_psi.gpudata.free()
        cu_psi_f.gpudata.free()
        cu_icalc_f.gpudata.free()
        cu_icalc_m_f.gpudata.free()
        cu_iobs.gpudata.free()
        del cu_psi, cu_psi_f, cu_icalc_f, cu_icalc_m_f, cu_iobs
        gc.collect()

        # Center kernel
        psf = np.abs(fftshift(cu_psf.get()))
        cu_psf.gpudata.free()
        del cu_psf

        imax = np.argmax(psf)
        if psf.ndim == 2:
            ix = imax % nx - nx // 2
            iy = imax // nx - ny // 2
            psf = np.roll(psf, (-iy, -ix), axis=(0, 1))
        else:
            ix = imax % nx - nx // 2
            iz = imax // (nx * ny) - nz // 2
            iy = (imax - iz * nx * ny) // nx - ny // 2
            psf = np.roll(psf, (-iz, -iy, -ix), axis=(0, 1, 2))

        # Symmetrize & normalize kernel
        if psf.ndim == 2:
            psf[1:, 1:] += psf[1:, 1:][::-1, ::-1]
        else:
            psf[1:, 1:, 1:] += psf[1:, 1:, 1:][::-1, ::-1, ::-1]
        psf /= psf.sum()

        # Store complete convolution kernel
        cdi._k_psf = np.zeros(cdi._cu_obj.shape, dtype=np.float32)

        if cdi._obj.ndim == 2:
            ny, nx = cdi._k_psf.shape
            cdi._k_psf[ny // 2 - n2:ny // 2 + n2, nx // 2 - n2:nx // 2 + n2] = psf
            shape2 = (ny, nx // 2 + 1)
        else:
            nz, ny, nx = cdi._k_psf.shape
            cdi._k_psf[nz // 2 - n2:nz // 2 + n2, ny // 2 - n2:ny // 2 + n2, nx // 2 - n2:nx // 2 + n2] = psf
            shape2 = (nz, ny, nx // 2 + 1)

        del psf
        gc.collect()

        cdi._k_psf = fftshift(cdi._k_psf)  # float32 convolution kernel

        # Keep FT'd kernel for convolution of calculated intensity, as a half-Hermitian array from real->complex FFT
        # Also store the FFT plans in the CDI object
        cu_k_psf = cua.to_gpu(cdi._k_psf)
        cdi._cu_k_psf_f = cua.empty(shape2, np.complex64)
        # print("cdi._cu_k_psf_f", cdi._cu_k_psf_f.shape, cdi._cu_k_psf_f.dtype)

        # With cuFFT, the shape for r2c and c2r plans is the one of the *real* array
        # TODO: do we really need two plans ?
        cdi._cu_plan_r2c = pu.cu_fft_get_plan(cu_k_psf.shape, batch=False,
                                              dtype_src=np.float32, dtype_dest=np.complex64)
        cdi._cu_plan_c2r = pu.cu_fft_get_plan(cu_k_psf.shape, batch=False,
                                              dtype_src=np.complex64, dtype_dest=np.float32)

        cu_fft.fft(cu_k_psf, cdi._cu_k_psf_f, cdi._cu_plan_r2c, scale=False)
        cdi._cu_k_psf_f /= cu_k_psf.size

        # Check with old LLK values to see if something went wrong
        cdi = LLK() * cdi
        llk_poisson_new = cdi.get_llk()[0]
        if llk_poisson_new > 10 * llk_poisson_old + 10:
            s = "LLK: %8.2f -> %8.2f" % (llk_poisson_old, llk_poisson_new)
            print("Oups: something went wrong with the PSF estimation [%s]. Not updating PSF." % s)
            cdi._k_psf = k_psf_old
            if k_psf_old is not None:
                cdi._cu_k_psf_f = cua.to_gpu(k_psf_f_old)
            else:
                cdi._cu_k_psf_f = None
                cdi._cu_plan_r2c = None
                cdi._cu_plan_c2r = None
        # else:
        #    s = "LLK: %8.2f -> %8.2f" % (llk_poisson_old, llk_poisson_new)
        #    print("PSF estimation: %s" % s)

        if need_ft:
            cdi = IFT(True) * cdi

        return cdi


class PRTF(CUOperatorCDI):
    """Operator to compute the Phase Retrieval Transfer Function.
    When applied to a CDI object, it stores the result in it as
    cdi.prtf, cdi.prtf_freq, cdi.prtf_nyquist, cdi.prtf_nb
    """

    def __init__(self, fig_num=None, file_name=None, nb_shell=None, fig_title=None):
        """

        :param fig_num: the figure number to display the PRTF.
        :param file_name: if given, the PRTF figure will be saved to this file (should end in .png or .pdf).
        :param nb_shell: the number of shell in which to compute the PRTF. By default the shell thickness is 2 pixels
        :param fig_title: the figure title
        """
        super(PRTF, self).__init__()
        self.fig_num = fig_num
        self.file_name = file_name
        self.nb_shell = nb_shell
        self.fig_title = fig_title

    def op(self, cdi: CDI):
        pu = self.processing_unit
        need_ft = cdi.in_object_space()

        if need_ft:
            cdi = FT() * cdi

        sh = cdi.get_iobs().shape
        f_nyquist = np.int32(np.max(sh) / 2)
        if self.nb_shell is None:
            nb_shell = np.int32(f_nyquist / 2)
        else:
            nb_shell = np.int32(self.nb_shell)
        # print("PRTF: fnyquist=%5f  nb_shell=%4d" % (f_nyquist, nb_shell))
        ny, nx = sh[-2:]
        if len(sh) == 3:
            nz = sh[0]
        else:
            nz = 1
        cu_shell_obs = cua.zeros(nb_shell, np.float32)
        cu_shell_calc = cua.zeros(nb_shell, np.float32)
        cu_shell_nb = cua.zeros(nb_shell, np.int32)
        cdi.prtf_freq = np.linspace(0, f_nyquist * (nb_shell - 1) / nb_shell, nb_shell) + f_nyquist / nb_shell / 2
        cdi.prtf_fnyquist = f_nyquist
        cu_prtf_k = CU_ElK(name='cu_prtf',
                           operation="prtf(i, obj, iobs, shell_calc, shell_obs, shell_nb, nb_shell, f_nyquist, nx, ny,"
                                     "nz)",
                           preamble=getks('cuda/complex.cu') + getks('cuda/float_n.cu') + getks('cdi/cuda/prtf.cu'),
                           options=self.processing_unit.cu_options,
                           arguments="pycuda::complex<float>* obj, float* iobs, float* shell_calc,"
                                     "float* shell_obs, int *shell_nb, const int nb_shell,"
                                     "const int f_nyquist, const int nx, const int ny, const int nz")
        cu_prtf_k(cdi._cu_obj, cdi._cu_iobs, cu_shell_calc, cu_shell_obs, cu_shell_nb, nb_shell, f_nyquist, nx, ny, nz)
        prtf = cu_shell_calc.get() / cu_shell_obs.get()
        nb = cu_shell_nb.get()
        prtf /= np.nanpercentile(prtf[nb > 0], 100)
        cdi.prtf = np.ma.masked_array(prtf, mask=nb == 0)
        cdi.prtf_nb = nb
        cdi.prtf_iobs = cu_shell_obs.get()

        plot_prtf(cdi.prtf_freq, f_nyquist, cdi.prtf, iobs_shell=cdi.prtf_iobs, nbiobs_shell=nb,
                  file_name=self.file_name, title=self.fig_title)

        if need_ft:
            cdi = IFT() * cdi

        return cdi


class InterpIobsMask(CUOperatorCDI):
    """
    Interpolate masked pixels observed intensity using inverse distance weighting
    """

    def __init__(self, d=8, n=4):
        """

        :param d: the half-distance of the interpolation, which will be done
            for pixel i from i-d to i+d along each dimension
        :param n: the weighting will be calculated as 1/d**n
        """
        super(InterpIobsMask, self).__init__()
        self.d = np.int32(d)
        self.n = np.int32(n)

    def op(self, cdi: CDI):
        nx, ny = np.int32(cdi._cu_iobs.shape[-1]), np.int32(cdi._cu_iobs.shape[-2])
        if cdi._obj.ndim == 3:
            nz = np.int32(cdi._cu_iobs.shape[0])
        else:
            nz = np.int32(1)
        self.processing_unit.cu_mask_interp_dist(cdi._cu_iobs, self.d, self.n, nx, ny, nz)
        cdi.iobs = cdi._cu_iobs.get()
        return cdi
