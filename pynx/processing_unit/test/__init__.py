#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2018-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

"""
This package provides access to the processing unit test suite.
"""

import unittest

from pynx.processing_unit.test.test_opencl import suite as test_opencl_suite
from pynx.processing_unit.test.test_cuda import suite as test_cuda_suite


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(test_opencl_suite())
    test_suite.addTest(test_cuda_suite())
    return test_suite
