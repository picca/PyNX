# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import warnings

warnings.filterwarnings("ignore", message="creating CUBLAS context to get version number")
import numpy as np

try:
    import pycuda.driver as cu_drv

    cu_drv.init()
    import pycuda.gpuarray as gpuarray
    from pycuda.tools import make_default_context
    from pycuda.driver import Context
    import skcuda.fft as cu_fft

    has_cuda = True
except ImportError:
    pass

from . import ProcessingUnit
from .cu_resources import cu_resources

from pynx.utils.math import test_smaller_primes


class CUProcessingUnit(ProcessingUnit):
    """
    Processing unit in CUDA space.

    Handles initializing the context and fft plan. Kernel initialization must be done in derived classes.
    """

    def __init__(self):
        super(CUProcessingUnit, self).__init__()
        self.cu_ctx = None  # CUDA context
        self.cu_options = None  # CUDA compile options
        self.cu_arch = None
        self.profiling = False
        self._cufft_plan_v = {}

    def init_cuda(self, cu_ctx=None, cu_device=None, fft_size=(1, 1024, 1024), batch=True, gpu_name=None, test_fft=True,
                  verbose=True):
        """
        Initialize the OpenCL context and creates an associated command queue

        :param cu_ctx: pycuda.driver.Context. If none, a default context will be created
        :param cu_device: pycuda.driver.Device. If none, and no context is given, the fastest GPu will be used.
        :param fft_size: the fft size to be used, for benchmark purposes when selecting GPU. different fft sizes
                         can be used afterwards?
        :param batch: if True, will benchmark using a batch 2D FFT
        :param gpu_name: a (sub)string matching the name of the gpu to be used
        :param test_fft: if True, will benchmark the GPU(s)
        :param verbose: report the GPU found and their speed
        :return: nothing
        """
        self.set_benchmark_fft_parameters(fft_size=fft_size, batch=batch)
        self.use_cuda(gpu_name=gpu_name, cu_ctx=cu_ctx, cu_device=cu_device, test_fft=test_fft, verbose=verbose)

        assert test_smaller_primes(fft_size[-1], self.max_prime_fft, required_dividers=(2,)) \
               and test_smaller_primes(fft_size[-2], self.max_prime_fft, required_dividers=(2,))

        if self.cu_ctx is None:
            self.cu_ctx = CUProcessingUnit.get_context(self.cu_device)

        self.cu_options = ["-use_fast_math"]
        # TODO: KLUDGE. Add a workaround if the card (Pascal) is more recent than the driver...
        # if cu_drv.get_version() == (7,5,0) and self.cu_device.compute_capability() == (6,1):
        #    print("WARNING: using a Pascal card (compute capability 6.1) with driver .5: forcing arch=sm_52")
        #    self.cu_arch = 'sm_50'
        self.cu_init_kernels()

    def get_context(device):
        """
        Static method to get a context, using the static device context dictionary to avoid creating new contexts,
        which will use up the GPU memory.
        :param device: the pyCUDA device for which a context is desired
        """
        return cu_resources.get_context(device)

    def cu_init_kernels(self):
        """
        Initialize kernels. Virtual function, must be derived.

        :return: nothing
        """

    def cu_fft_set_plan(self, cu_data, batch=True, stream=None):
        """
        Creates FFT plan, or updates it if the shape of the data or the axes have changed.

        .. deprecated:: 2020.2
        Use :func:`cu_fft_get_plan` instead.

        :param cu_data: an array from which the FFT shape will be extracted
        :param batch: if True, perform a 2D batch FFT on an n-dimensional array (n>2). Ignored if data is 2D.
                      The FFT is computed over the last two dimensions.
        :param stream: the cuda stream to be associated with this plan
        :return: nothing
        """
        warnings.warn("CUProcessingUnit.cu_fft_set_plan() is deprecated, use cu_fft_get_plan() instead",
                      DeprecationWarning)
        self.cufft_plan = self.cu_fft_get_plan(cu_data, batch, stream)

    def cu_fft_get_plan(self, cu_data, batch=True, stream=None, dtype_dest=None, dtype_src=None):
        """
        Get a FFT plan according to the given parameters. if the plan already exists, it is re-used

        :param cu_data: an array from which the FFT shape will be extracted. Alternatively
            this can be the shape of the array, but dtype_src and dtype_dest must be given
        :param batch: if True, perform a 2D batch FFT on an n-dimensional array (n>2). Ignored if data is 2D.
                      The FFT is computed over the last two dimensions.
        :param stream: the cuda stream to be associated with this plan
        :param dtype_dest: if None, the destination array dtype is taken from cu_data.
            This can be used for ccomplex<->real transforms
        :param dtype_src: if None, the source array dtype is taken from cu_data.
            This can be used for ccomplex<->real transforms
        :return: the cufft plan
        """
        if isinstance(cu_data, tuple) or isinstance(cu_data, list):
            fft_shape = cu_data
        else:
            fft_shape = cu_data.shape

        if len(fft_shape) == 2:
            batch = False

        if dtype_dest is None:
            dtype_dest = cu_data.dtype

        if dtype_src is None:
            dtype_src = cu_data.dtype

        k = (fft_shape, dtype_src, dtype_dest, batch, stream)

        if k not in self._cufft_plan_v:
            if batch:
                # print("Setting cufft plan: ", cu_data.shape, batch)
                self._cufft_plan_v[k] = cu_fft.Plan(fft_shape[-2:], dtype_src, dtype_dest,
                                                    batch=np.product(cu_data.shape[:-2]), stream=stream)
            else:
                # print("Setting cufft plan: ", cu_data.shape, batch)
                self._cufft_plan_v[k] = cu_fft.Plan(fft_shape, dtype_src, dtype_dest, batch=1, stream=stream)

        return self._cufft_plan_v[k]

    def cu_fft_free_plans(self):
        """
        Delete the cufft plans from memory. Actual deletion may only occur later (gc...)
        :return: nothing
        """
        if cu_fft.cufft.cufftGetVersion() > 10010:
            txt = "WARNING: cuFFT plan destruction inhibited as a workaround for " \
                  "an issue with CUDA>=11.0. See https://github.com/lebedov/scikit-cuda/issues/308"
            warnings.filterwarnings('once', message=txt, append=True)
            warnings.warn(txt, UserWarning)
        else:
            self._cufft_plan_v = {}

    def enable_profiling(self, profiling=True):
        """
        Enable profiling
        :param profiling: True to enable (the default)
        :return:
        """
        if profiling and self.profiling is False:
            cu_drv.start_profiler()
        elif self.profiling and profiling is False:
            cu_drv.stop_profiler()
        self.profiling = profiling

    def finish(self):
        self.cu_ctx.synchronize()


# This is mostly used during tests while avidoing to destroy & create FFT plans
default_processing_unit = CUProcessingUnit()
